#!/usr/bin/env node
import type {Pyright, Gitlab} from "./types"
import path from "node:path"
import { readFileSync, writeFileSync } from "node:fs";
import { parse } from 'ts-command-line-args';

import { convertDiagnostics } from './converter'

interface ICopyFilesArguments {
    src: string;
    output: string;
    base_path: string;
    help?: boolean;
}

// eslint-disable-next-line @typescript-eslint/no-unsafe-assignment, @typescript-eslint/no-unsafe-call
const arguments_ : ICopyFilesArguments = parse<ICopyFilesArguments>(
    {
        src: String,
        output: String,
        base_path: String,
        help: { type: Boolean, optional: true, alias: 'h', description: 'Prints this usage guide' },
    },
    {
        helpArg: 'help',
        headerContentSections: [{ header: 'Overall usage', content: 'Get a json output of pyright and output another json. There is no validation for the input json, so be careful to pass in the correct one. All the paths will be converted from absolute ones to relative ones based on the base_path - which should point to the base directory of the repository' }]
    },
);


// eslint-disable-next-line @typescript-eslint/no-unsafe-assignment
const report : Pyright.GeneralReport= JSON.parse(readFileSync(arguments_.src, "utf8").toString())
const parsedData : Pyright.PyrightJsonDiagnostic[] = report.generalDiagnostics;
const convertedData: Gitlab.GitlabReport[]= convertDiagnostics(parsedData, path.resolve(arguments_.base_path))
writeFileSync(arguments_.output, JSON.stringify(convertedData), {encoding: "utf8"});
