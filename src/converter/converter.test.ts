import type * as Pyright from "../types/pyright"
import type * as Gitlab from "../types/gitlab"
import { convertDiagnosticCategoryToGitlabSeverity, convertPosition, convertRange } from "./converter"

it.each<[Pyright.Position, Gitlab.CodeClimatePosition]>([
    [{line: 4, character:6}, {line: 5, column: 6}],
    [{line: 10, character:20}, {line: 11, column: 20}],
])("convert pyright location to codeclimate location", (pyright:Pyright.Position, codeclimate: Gitlab.CodeClimatePosition)=>{
    expect(convertPosition(pyright)).toEqual(codeclimate)
})


test("convertRange", () => {
    expect(convertRange({start: {character: 2, line:3}, end: {character: 6, line:4}})).toEqual({begin: {column: 2, line:4}, end: {column: 6, line:5}})
})

it.each<[Pyright.SeverityLevel, Gitlab.GitlabSeverityLevel]>([
    ["error", "blocker"],
    ["warning", "major"],
    ["information", "info"],
    ["unreachablecode", "minor"],
    ["unusedcode", "info"],
    ["deprecated", "info"]
])("check conversion", (pyright_severity: Pyright.SeverityLevel, severity:Gitlab.GitlabSeverityLevel)=>{
    expect(convertDiagnosticCategoryToGitlabSeverity(pyright_severity)).toBe(severity)
})