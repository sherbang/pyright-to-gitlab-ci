import path from "node:path"
import type { Gitlab, Pyright } from "../types";

export function convertPosition(position: Pyright.Position) : Gitlab.CodeClimatePosition{
    return {
        line: position.line+1, // pyright count start at 0 while codeclimate start at 1
        column: position.character
    }
}

export function convertRange(range: Pyright.Range): Gitlab.CodeClimateRange{
    return {
        begin: convertPosition(range.start),
        end: convertPosition(range.end)
    }
}


export function convertDiagnosticCategoryToGitlabSeverity(category: Pyright.SeverityLevel): Gitlab.GitlabSeverityLevel {
    switch (category) {
        case "error": {
            return 'blocker';
        }
        case "warning":{
            return "major";
        }
        case "information":{
            return 'info';
        }
        case "unreachablecode":{
            return 'minor';
        }
        case "unusedcode":{
            return 'info';
        }
        case "deprecated":{
            return "info"
        }
    }
}



export function convertDiagnosticToGitlab(diag: Pyright.PyrightJsonDiagnostic, base_path: string): Gitlab.GitlabReport {
    return {
        description: diag.message,
        fingerprint: diag.rule || "Unknown",
        severity: convertDiagnosticCategoryToGitlabSeverity(diag.severity),
        location: {
            path: path.relative(path.resolve(base_path), diag.file),
            positions: diag.range===undefined? {begin: {column:1, line:1}, end:{column:1, line:1}}:convertRange(diag.range)
        },
    };
}

export function convertDiagnostics(diag: Pyright.PyrightJsonDiagnostic[], base_path: string): Gitlab.GitlabReport[] {
    const response: Gitlab.GitlabReport[] = []
    for(const value of diag){
        response.push(convertDiagnosticToGitlab(value, base_path))
    }
    return response
}
