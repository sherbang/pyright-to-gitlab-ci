#!/usr/bin/env node
"use strict";
var __create = Object.create;
var __defProp = Object.defineProperty;
var __getOwnPropDesc = Object.getOwnPropertyDescriptor;
var __getOwnPropNames = Object.getOwnPropertyNames;
var __getProtoOf = Object.getPrototypeOf;
var __hasOwnProp = Object.prototype.hasOwnProperty;
var __commonJS = (cb, mod) => function __require() {
  return mod || (0, cb[__getOwnPropNames(cb)[0]])((mod = { exports: {} }).exports, mod), mod.exports;
};
var __copyProps = (to, from, except, desc) => {
  if (from && typeof from === "object" || typeof from === "function") {
    for (let key of __getOwnPropNames(from))
      if (!__hasOwnProp.call(to, key) && key !== except)
        __defProp(to, key, { get: () => from[key], enumerable: !(desc = __getOwnPropDesc(from, key)) || desc.enumerable });
  }
  return to;
};
var __toESM = (mod, isNodeMode, target) => (target = mod != null ? __create(__getProtoOf(mod)) : {}, __copyProps(
  // If the importer is in node compatibility mode or this is not an ESM
  // file that has been converted to a CommonJS file using a Babel-
  // compatible transform (i.e. "__esModule" has not been set), then set
  // "default" to the CommonJS "module.exports" for node compatibility.
  isNodeMode || !mod || !mod.__esModule ? __defProp(target, "default", { value: mod, enumerable: true }) : target,
  mod
));

// node_modules/lodash.camelcase/index.js
var require_lodash = __commonJS({
  "node_modules/lodash.camelcase/index.js"(exports, module2) {
    var INFINITY = 1 / 0;
    var symbolTag = "[object Symbol]";
    var reAsciiWord = /[^\x00-\x2f\x3a-\x40\x5b-\x60\x7b-\x7f]+/g;
    var reLatin = /[\xc0-\xd6\xd8-\xf6\xf8-\xff\u0100-\u017f]/g;
    var rsAstralRange = "\\ud800-\\udfff";
    var rsComboMarksRange = "\\u0300-\\u036f\\ufe20-\\ufe23";
    var rsComboSymbolsRange = "\\u20d0-\\u20f0";
    var rsDingbatRange = "\\u2700-\\u27bf";
    var rsLowerRange = "a-z\\xdf-\\xf6\\xf8-\\xff";
    var rsMathOpRange = "\\xac\\xb1\\xd7\\xf7";
    var rsNonCharRange = "\\x00-\\x2f\\x3a-\\x40\\x5b-\\x60\\x7b-\\xbf";
    var rsPunctuationRange = "\\u2000-\\u206f";
    var rsSpaceRange = " \\t\\x0b\\f\\xa0\\ufeff\\n\\r\\u2028\\u2029\\u1680\\u180e\\u2000\\u2001\\u2002\\u2003\\u2004\\u2005\\u2006\\u2007\\u2008\\u2009\\u200a\\u202f\\u205f\\u3000";
    var rsUpperRange = "A-Z\\xc0-\\xd6\\xd8-\\xde";
    var rsVarRange = "\\ufe0e\\ufe0f";
    var rsBreakRange = rsMathOpRange + rsNonCharRange + rsPunctuationRange + rsSpaceRange;
    var rsApos = "['\u2019]";
    var rsAstral = "[" + rsAstralRange + "]";
    var rsBreak = "[" + rsBreakRange + "]";
    var rsCombo = "[" + rsComboMarksRange + rsComboSymbolsRange + "]";
    var rsDigits = "\\d+";
    var rsDingbat = "[" + rsDingbatRange + "]";
    var rsLower = "[" + rsLowerRange + "]";
    var rsMisc = "[^" + rsAstralRange + rsBreakRange + rsDigits + rsDingbatRange + rsLowerRange + rsUpperRange + "]";
    var rsFitz = "\\ud83c[\\udffb-\\udfff]";
    var rsModifier = "(?:" + rsCombo + "|" + rsFitz + ")";
    var rsNonAstral = "[^" + rsAstralRange + "]";
    var rsRegional = "(?:\\ud83c[\\udde6-\\uddff]){2}";
    var rsSurrPair = "[\\ud800-\\udbff][\\udc00-\\udfff]";
    var rsUpper = "[" + rsUpperRange + "]";
    var rsZWJ = "\\u200d";
    var rsLowerMisc = "(?:" + rsLower + "|" + rsMisc + ")";
    var rsUpperMisc = "(?:" + rsUpper + "|" + rsMisc + ")";
    var rsOptLowerContr = "(?:" + rsApos + "(?:d|ll|m|re|s|t|ve))?";
    var rsOptUpperContr = "(?:" + rsApos + "(?:D|LL|M|RE|S|T|VE))?";
    var reOptMod = rsModifier + "?";
    var rsOptVar = "[" + rsVarRange + "]?";
    var rsOptJoin = "(?:" + rsZWJ + "(?:" + [rsNonAstral, rsRegional, rsSurrPair].join("|") + ")" + rsOptVar + reOptMod + ")*";
    var rsSeq = rsOptVar + reOptMod + rsOptJoin;
    var rsEmoji = "(?:" + [rsDingbat, rsRegional, rsSurrPair].join("|") + ")" + rsSeq;
    var rsSymbol = "(?:" + [rsNonAstral + rsCombo + "?", rsCombo, rsRegional, rsSurrPair, rsAstral].join("|") + ")";
    var reApos = RegExp(rsApos, "g");
    var reComboMark = RegExp(rsCombo, "g");
    var reUnicode = RegExp(rsFitz + "(?=" + rsFitz + ")|" + rsSymbol + rsSeq, "g");
    var reUnicodeWord = RegExp([
      rsUpper + "?" + rsLower + "+" + rsOptLowerContr + "(?=" + [rsBreak, rsUpper, "$"].join("|") + ")",
      rsUpperMisc + "+" + rsOptUpperContr + "(?=" + [rsBreak, rsUpper + rsLowerMisc, "$"].join("|") + ")",
      rsUpper + "?" + rsLowerMisc + "+" + rsOptLowerContr,
      rsUpper + "+" + rsOptUpperContr,
      rsDigits,
      rsEmoji
    ].join("|"), "g");
    var reHasUnicode = RegExp("[" + rsZWJ + rsAstralRange + rsComboMarksRange + rsComboSymbolsRange + rsVarRange + "]");
    var reHasUnicodeWord = /[a-z][A-Z]|[A-Z]{2,}[a-z]|[0-9][a-zA-Z]|[a-zA-Z][0-9]|[^a-zA-Z0-9 ]/;
    var deburredLetters = {
      // Latin-1 Supplement block.
      "\xC0": "A",
      "\xC1": "A",
      "\xC2": "A",
      "\xC3": "A",
      "\xC4": "A",
      "\xC5": "A",
      "\xE0": "a",
      "\xE1": "a",
      "\xE2": "a",
      "\xE3": "a",
      "\xE4": "a",
      "\xE5": "a",
      "\xC7": "C",
      "\xE7": "c",
      "\xD0": "D",
      "\xF0": "d",
      "\xC8": "E",
      "\xC9": "E",
      "\xCA": "E",
      "\xCB": "E",
      "\xE8": "e",
      "\xE9": "e",
      "\xEA": "e",
      "\xEB": "e",
      "\xCC": "I",
      "\xCD": "I",
      "\xCE": "I",
      "\xCF": "I",
      "\xEC": "i",
      "\xED": "i",
      "\xEE": "i",
      "\xEF": "i",
      "\xD1": "N",
      "\xF1": "n",
      "\xD2": "O",
      "\xD3": "O",
      "\xD4": "O",
      "\xD5": "O",
      "\xD6": "O",
      "\xD8": "O",
      "\xF2": "o",
      "\xF3": "o",
      "\xF4": "o",
      "\xF5": "o",
      "\xF6": "o",
      "\xF8": "o",
      "\xD9": "U",
      "\xDA": "U",
      "\xDB": "U",
      "\xDC": "U",
      "\xF9": "u",
      "\xFA": "u",
      "\xFB": "u",
      "\xFC": "u",
      "\xDD": "Y",
      "\xFD": "y",
      "\xFF": "y",
      "\xC6": "Ae",
      "\xE6": "ae",
      "\xDE": "Th",
      "\xFE": "th",
      "\xDF": "ss",
      // Latin Extended-A block.
      "\u0100": "A",
      "\u0102": "A",
      "\u0104": "A",
      "\u0101": "a",
      "\u0103": "a",
      "\u0105": "a",
      "\u0106": "C",
      "\u0108": "C",
      "\u010A": "C",
      "\u010C": "C",
      "\u0107": "c",
      "\u0109": "c",
      "\u010B": "c",
      "\u010D": "c",
      "\u010E": "D",
      "\u0110": "D",
      "\u010F": "d",
      "\u0111": "d",
      "\u0112": "E",
      "\u0114": "E",
      "\u0116": "E",
      "\u0118": "E",
      "\u011A": "E",
      "\u0113": "e",
      "\u0115": "e",
      "\u0117": "e",
      "\u0119": "e",
      "\u011B": "e",
      "\u011C": "G",
      "\u011E": "G",
      "\u0120": "G",
      "\u0122": "G",
      "\u011D": "g",
      "\u011F": "g",
      "\u0121": "g",
      "\u0123": "g",
      "\u0124": "H",
      "\u0126": "H",
      "\u0125": "h",
      "\u0127": "h",
      "\u0128": "I",
      "\u012A": "I",
      "\u012C": "I",
      "\u012E": "I",
      "\u0130": "I",
      "\u0129": "i",
      "\u012B": "i",
      "\u012D": "i",
      "\u012F": "i",
      "\u0131": "i",
      "\u0134": "J",
      "\u0135": "j",
      "\u0136": "K",
      "\u0137": "k",
      "\u0138": "k",
      "\u0139": "L",
      "\u013B": "L",
      "\u013D": "L",
      "\u013F": "L",
      "\u0141": "L",
      "\u013A": "l",
      "\u013C": "l",
      "\u013E": "l",
      "\u0140": "l",
      "\u0142": "l",
      "\u0143": "N",
      "\u0145": "N",
      "\u0147": "N",
      "\u014A": "N",
      "\u0144": "n",
      "\u0146": "n",
      "\u0148": "n",
      "\u014B": "n",
      "\u014C": "O",
      "\u014E": "O",
      "\u0150": "O",
      "\u014D": "o",
      "\u014F": "o",
      "\u0151": "o",
      "\u0154": "R",
      "\u0156": "R",
      "\u0158": "R",
      "\u0155": "r",
      "\u0157": "r",
      "\u0159": "r",
      "\u015A": "S",
      "\u015C": "S",
      "\u015E": "S",
      "\u0160": "S",
      "\u015B": "s",
      "\u015D": "s",
      "\u015F": "s",
      "\u0161": "s",
      "\u0162": "T",
      "\u0164": "T",
      "\u0166": "T",
      "\u0163": "t",
      "\u0165": "t",
      "\u0167": "t",
      "\u0168": "U",
      "\u016A": "U",
      "\u016C": "U",
      "\u016E": "U",
      "\u0170": "U",
      "\u0172": "U",
      "\u0169": "u",
      "\u016B": "u",
      "\u016D": "u",
      "\u016F": "u",
      "\u0171": "u",
      "\u0173": "u",
      "\u0174": "W",
      "\u0175": "w",
      "\u0176": "Y",
      "\u0177": "y",
      "\u0178": "Y",
      "\u0179": "Z",
      "\u017B": "Z",
      "\u017D": "Z",
      "\u017A": "z",
      "\u017C": "z",
      "\u017E": "z",
      "\u0132": "IJ",
      "\u0133": "ij",
      "\u0152": "Oe",
      "\u0153": "oe",
      "\u0149": "'n",
      "\u017F": "ss"
    };
    var freeGlobal = typeof global == "object" && global && global.Object === Object && global;
    var freeSelf = typeof self == "object" && self && self.Object === Object && self;
    var root = freeGlobal || freeSelf || Function("return this")();
    function arrayReduce(array, iteratee, accumulator, initAccum) {
      var index = -1, length = array ? array.length : 0;
      if (initAccum && length) {
        accumulator = array[++index];
      }
      while (++index < length) {
        accumulator = iteratee(accumulator, array[index], index, array);
      }
      return accumulator;
    }
    function asciiToArray(string) {
      return string.split("");
    }
    function asciiWords(string) {
      return string.match(reAsciiWord) || [];
    }
    function basePropertyOf(object) {
      return function(key) {
        return object == null ? void 0 : object[key];
      };
    }
    var deburrLetter = basePropertyOf(deburredLetters);
    function hasUnicode(string) {
      return reHasUnicode.test(string);
    }
    function hasUnicodeWord(string) {
      return reHasUnicodeWord.test(string);
    }
    function stringToArray(string) {
      return hasUnicode(string) ? unicodeToArray(string) : asciiToArray(string);
    }
    function unicodeToArray(string) {
      return string.match(reUnicode) || [];
    }
    function unicodeWords(string) {
      return string.match(reUnicodeWord) || [];
    }
    var objectProto = Object.prototype;
    var objectToString = objectProto.toString;
    var Symbol2 = root.Symbol;
    var symbolProto = Symbol2 ? Symbol2.prototype : void 0;
    var symbolToString = symbolProto ? symbolProto.toString : void 0;
    function baseSlice(array, start, end) {
      var index = -1, length = array.length;
      if (start < 0) {
        start = -start > length ? 0 : length + start;
      }
      end = end > length ? length : end;
      if (end < 0) {
        end += length;
      }
      length = start > end ? 0 : end - start >>> 0;
      start >>>= 0;
      var result = Array(length);
      while (++index < length) {
        result[index] = array[index + start];
      }
      return result;
    }
    function baseToString(value) {
      if (typeof value == "string") {
        return value;
      }
      if (isSymbol(value)) {
        return symbolToString ? symbolToString.call(value) : "";
      }
      var result = value + "";
      return result == "0" && 1 / value == -INFINITY ? "-0" : result;
    }
    function castSlice(array, start, end) {
      var length = array.length;
      end = end === void 0 ? length : end;
      return !start && end >= length ? array : baseSlice(array, start, end);
    }
    function createCaseFirst(methodName) {
      return function(string) {
        string = toString(string);
        var strSymbols = hasUnicode(string) ? stringToArray(string) : void 0;
        var chr = strSymbols ? strSymbols[0] : string.charAt(0);
        var trailing = strSymbols ? castSlice(strSymbols, 1).join("") : string.slice(1);
        return chr[methodName]() + trailing;
      };
    }
    function createCompounder(callback) {
      return function(string) {
        return arrayReduce(words(deburr(string).replace(reApos, "")), callback, "");
      };
    }
    function isObjectLike(value) {
      return !!value && typeof value == "object";
    }
    function isSymbol(value) {
      return typeof value == "symbol" || isObjectLike(value) && objectToString.call(value) == symbolTag;
    }
    function toString(value) {
      return value == null ? "" : baseToString(value);
    }
    var camelCase = createCompounder(function(result, word, index) {
      word = word.toLowerCase();
      return result + (index ? capitalize(word) : word);
    });
    function capitalize(string) {
      return upperFirst(toString(string).toLowerCase());
    }
    function deburr(string) {
      string = toString(string);
      return string && string.replace(reLatin, deburrLetter).replace(reComboMark, "");
    }
    var upperFirst = createCaseFirst("toUpperCase");
    function words(string, pattern, guard) {
      string = toString(string);
      pattern = guard ? void 0 : pattern;
      if (pattern === void 0) {
        return hasUnicodeWord(string) ? unicodeWords(string) : asciiWords(string);
      }
      return string.match(pattern) || [];
    }
    module2.exports = camelCase;
  }
});

// node_modules/command-line-args/dist/index.js
var require_dist = __commonJS({
  "node_modules/command-line-args/dist/index.js"(exports, module2) {
    "use strict";
    function _interopDefault(ex) {
      return ex && typeof ex === "object" && "default" in ex ? ex["default"] : ex;
    }
    var camelCase = _interopDefault(require_lodash());
    function isObject(input) {
      return typeof input === "object" && input !== null;
    }
    function isArrayLike(input) {
      return isObject(input) && typeof input.length === "number";
    }
    function arrayify(input) {
      if (Array.isArray(input)) {
        return input;
      }
      if (input === void 0) {
        return [];
      }
      if (isArrayLike(input) || input instanceof Set) {
        return Array.from(input);
      }
      return [input];
    }
    function isObject$1(input) {
      return typeof input === "object" && input !== null;
    }
    function isArrayLike$1(input) {
      return isObject$1(input) && typeof input.length === "number";
    }
    function arrayify$1(input) {
      if (Array.isArray(input)) {
        return input;
      } else {
        if (input === void 0) {
          return [];
        } else if (isArrayLike$1(input)) {
          return Array.prototype.slice.call(input);
        } else {
          return [input];
        }
      }
    }
    function findReplace(array, testFn) {
      const found = [];
      const replaceWiths = arrayify$1(arguments);
      replaceWiths.splice(0, 2);
      arrayify$1(array).forEach((value, index) => {
        let expanded = [];
        replaceWiths.forEach((replaceWith) => {
          if (typeof replaceWith === "function") {
            expanded = expanded.concat(replaceWith(value));
          } else {
            expanded.push(replaceWith);
          }
        });
        if (testFn(value)) {
          found.push({
            index,
            replaceWithValue: expanded
          });
        }
      });
      found.reverse().forEach((item) => {
        const spliceArgs = [item.index, 1].concat(item.replaceWithValue);
        array.splice.apply(array, spliceArgs);
      });
      return array;
    }
    var re = {
      short: /^-([^\d-])$/,
      long: /^--(\S+)/,
      combinedShort: /^-[^\d-]{2,}$/,
      optEquals: /^(--\S+?)=(.*)/
    };
    var ArgvArray = class extends Array {
      /**
       * Clears the array has loads the supplied input.
       * @param {string[]} argv - The argv list to load. Defaults to `process.argv`.
       */
      load(argv) {
        this.clear();
        if (argv && argv !== process.argv) {
          argv = arrayify(argv);
        } else {
          argv = process.argv.slice(0);
          const deleteCount = process.execArgv.some(isExecArg) ? 1 : 2;
          argv.splice(0, deleteCount);
        }
        argv.forEach((arg) => this.push(String(arg)));
      }
      /**
       * Clear the array.
       */
      clear() {
        this.length = 0;
      }
      /**
       * expand ``--option=value` style args.
       */
      expandOptionEqualsNotation() {
        if (this.some((arg) => re.optEquals.test(arg))) {
          const expandedArgs = [];
          this.forEach((arg) => {
            const matches = arg.match(re.optEquals);
            if (matches) {
              expandedArgs.push(matches[1], matches[2]);
            } else {
              expandedArgs.push(arg);
            }
          });
          this.clear();
          this.load(expandedArgs);
        }
      }
      /**
       * expand getopt-style combinedShort options.
       */
      expandGetoptNotation() {
        if (this.hasCombinedShortOptions()) {
          findReplace(this, re.combinedShort, expandCombinedShortArg);
        }
      }
      /**
       * Returns true if the array contains combined short options (e.g. `-ab`).
       * @returns {boolean}
       */
      hasCombinedShortOptions() {
        return this.some((arg) => re.combinedShort.test(arg));
      }
      static from(argv) {
        const result = new this();
        result.load(argv);
        return result;
      }
    };
    function expandCombinedShortArg(arg) {
      arg = arg.slice(1);
      return arg.split("").map((letter) => "-" + letter);
    }
    function isOptionEqualsNotation(arg) {
      return re.optEquals.test(arg);
    }
    function isOption(arg) {
      return (re.short.test(arg) || re.long.test(arg)) && !re.optEquals.test(arg);
    }
    function isLongOption(arg) {
      return re.long.test(arg) && !isOptionEqualsNotation(arg);
    }
    function getOptionName(arg) {
      if (re.short.test(arg)) {
        return arg.match(re.short)[1];
      } else if (isLongOption(arg)) {
        return arg.match(re.long)[1];
      } else if (isOptionEqualsNotation(arg)) {
        return arg.match(re.optEquals)[1].replace(/^--/, "");
      } else {
        return null;
      }
    }
    function isValue(arg) {
      return !(isOption(arg) || re.combinedShort.test(arg) || re.optEquals.test(arg));
    }
    function isExecArg(arg) {
      return ["--eval", "-e"].indexOf(arg) > -1 || arg.startsWith("--eval=");
    }
    function isNumber(n) {
      return !isNaN(parseFloat(n)) && isFinite(n);
    }
    function isPlainObject(input) {
      return input !== null && typeof input === "object" && input.constructor === Object;
    }
    function isArrayLike$2(input) {
      return isObject$2(input) && typeof input.length === "number";
    }
    function isObject$2(input) {
      return typeof input === "object" && input !== null;
    }
    function isDefined(input) {
      return typeof input !== "undefined";
    }
    function isString(input) {
      return typeof input === "string";
    }
    function isBoolean(input) {
      return typeof input === "boolean";
    }
    function isFunction(input) {
      return typeof input === "function";
    }
    function isClass(input) {
      if (isFunction(input)) {
        return /^class /.test(Function.prototype.toString.call(input));
      } else {
        return false;
      }
    }
    function isPrimitive(input) {
      if (input === null)
        return true;
      switch (typeof input) {
        case "string":
        case "number":
        case "symbol":
        case "undefined":
        case "boolean":
          return true;
        default:
          return false;
      }
    }
    function isPromise(input) {
      if (input) {
        const isPromise2 = isDefined(Promise) && input instanceof Promise;
        const isThenable = input.then && typeof input.then === "function";
        return !!(isPromise2 || isThenable);
      } else {
        return false;
      }
    }
    function isIterable(input) {
      if (input === null || !isDefined(input)) {
        return false;
      } else {
        return typeof input[Symbol.iterator] === "function" || typeof input[Symbol.asyncIterator] === "function";
      }
    }
    var t = {
      isNumber,
      isString,
      isBoolean,
      isPlainObject,
      isArrayLike: isArrayLike$2,
      isObject: isObject$2,
      isDefined,
      isFunction,
      isClass,
      isPrimitive,
      isPromise,
      isIterable
    };
    var OptionDefinition = class {
      constructor(definition) {
        this.name = definition.name;
        this.type = definition.type || String;
        this.alias = definition.alias;
        this.multiple = definition.multiple;
        this.lazyMultiple = definition.lazyMultiple;
        this.defaultOption = definition.defaultOption;
        this.defaultValue = definition.defaultValue;
        this.group = definition.group;
        for (const prop in definition) {
          if (!this[prop])
            this[prop] = definition[prop];
        }
      }
      isBoolean() {
        return this.type === Boolean || t.isFunction(this.type) && this.type.name === "Boolean";
      }
      isMultiple() {
        return this.multiple || this.lazyMultiple;
      }
      static create(def) {
        const result = new this(def);
        return result;
      }
    };
    var Definitions = class extends Array {
      /**
       * validate option definitions
       * @param {boolean} [caseInsensitive=false] - whether arguments will be parsed in a case insensitive manner
       * @returns {string}
       */
      validate(caseInsensitive) {
        const someHaveNoName = this.some((def) => !def.name);
        if (someHaveNoName) {
          halt(
            "INVALID_DEFINITIONS",
            "Invalid option definitions: the `name` property is required on each definition"
          );
        }
        const someDontHaveFunctionType = this.some((def) => def.type && typeof def.type !== "function");
        if (someDontHaveFunctionType) {
          halt(
            "INVALID_DEFINITIONS",
            "Invalid option definitions: the `type` property must be a setter fuction (default: `Boolean`)"
          );
        }
        let invalidOption;
        const numericAlias = this.some((def) => {
          invalidOption = def;
          return t.isDefined(def.alias) && t.isNumber(def.alias);
        });
        if (numericAlias) {
          halt(
            "INVALID_DEFINITIONS",
            "Invalid option definition: to avoid ambiguity an alias cannot be numeric [--" + invalidOption.name + " alias is -" + invalidOption.alias + "]"
          );
        }
        const multiCharacterAlias = this.some((def) => {
          invalidOption = def;
          return t.isDefined(def.alias) && def.alias.length !== 1;
        });
        if (multiCharacterAlias) {
          halt(
            "INVALID_DEFINITIONS",
            "Invalid option definition: an alias must be a single character"
          );
        }
        const hypenAlias = this.some((def) => {
          invalidOption = def;
          return def.alias === "-";
        });
        if (hypenAlias) {
          halt(
            "INVALID_DEFINITIONS",
            'Invalid option definition: an alias cannot be "-"'
          );
        }
        const duplicateName = hasDuplicates(this.map((def) => caseInsensitive ? def.name.toLowerCase() : def.name));
        if (duplicateName) {
          halt(
            "INVALID_DEFINITIONS",
            "Two or more option definitions have the same name"
          );
        }
        const duplicateAlias = hasDuplicates(this.map((def) => caseInsensitive && t.isDefined(def.alias) ? def.alias.toLowerCase() : def.alias));
        if (duplicateAlias) {
          halt(
            "INVALID_DEFINITIONS",
            "Two or more option definitions have the same alias"
          );
        }
        const duplicateDefaultOption = this.filter((def) => def.defaultOption === true).length > 1;
        if (duplicateDefaultOption) {
          halt(
            "INVALID_DEFINITIONS",
            "Only one option definition can be the defaultOption"
          );
        }
        const defaultBoolean = this.some((def) => {
          invalidOption = def;
          return def.isBoolean() && def.defaultOption;
        });
        if (defaultBoolean) {
          halt(
            "INVALID_DEFINITIONS",
            `A boolean option ["${invalidOption.name}"] can not also be the defaultOption.`
          );
        }
      }
      /**
       * Get definition by option arg (e.g. `--one` or `-o`)
       * @param {string} [arg] the argument name to get the definition for
       * @param {boolean} [caseInsensitive] whether to use case insensitive comparisons when finding the appropriate definition
       * @returns {Definition}
       */
      get(arg, caseInsensitive) {
        if (isOption(arg)) {
          if (re.short.test(arg)) {
            const shortOptionName = getOptionName(arg);
            if (caseInsensitive) {
              const lowercaseShortOptionName = shortOptionName.toLowerCase();
              return this.find((def) => t.isDefined(def.alias) && def.alias.toLowerCase() === lowercaseShortOptionName);
            } else {
              return this.find((def) => def.alias === shortOptionName);
            }
          } else {
            const optionName = getOptionName(arg);
            if (caseInsensitive) {
              const lowercaseOptionName = optionName.toLowerCase();
              return this.find((def) => def.name.toLowerCase() === lowercaseOptionName);
            } else {
              return this.find((def) => def.name === optionName);
            }
          }
        } else {
          return this.find((def) => def.name === arg);
        }
      }
      getDefault() {
        return this.find((def) => def.defaultOption === true);
      }
      isGrouped() {
        return this.some((def) => def.group);
      }
      whereGrouped() {
        return this.filter(containsValidGroup);
      }
      whereNotGrouped() {
        return this.filter((def) => !containsValidGroup(def));
      }
      whereDefaultValueSet() {
        return this.filter((def) => t.isDefined(def.defaultValue));
      }
      static from(definitions, caseInsensitive) {
        if (definitions instanceof this)
          return definitions;
        const result = super.from(arrayify(definitions), (def) => OptionDefinition.create(def));
        result.validate(caseInsensitive);
        return result;
      }
    };
    function halt(name, message) {
      const err = new Error(message);
      err.name = name;
      throw err;
    }
    function containsValidGroup(def) {
      return arrayify(def.group).some((group) => group);
    }
    function hasDuplicates(array) {
      const items = {};
      for (let i = 0; i < array.length; i++) {
        const value = array[i];
        if (items[value]) {
          return true;
        } else {
          if (t.isDefined(value))
            items[value] = true;
        }
      }
    }
    var ArgvParser = class {
      /**
       * @param {OptionDefinitions} - Definitions array
       * @param {object} [options] - Options
       * @param {string[]} [options.argv] - Overrides `process.argv`
       * @param {boolean} [options.stopAtFirstUnknown] -
       * @param {boolean} [options.caseInsensitive] - Arguments will be parsed in a case insensitive manner. Defaults to false.
       */
      constructor(definitions, options) {
        this.options = Object.assign({}, options);
        this.definitions = Definitions.from(definitions, this.options.caseInsensitive);
        this.argv = ArgvArray.from(this.options.argv);
        if (this.argv.hasCombinedShortOptions()) {
          findReplace(this.argv, re.combinedShort.test.bind(re.combinedShort), (arg) => {
            arg = arg.slice(1);
            return arg.split("").map((letter) => ({ origArg: `-${arg}`, arg: "-" + letter }));
          });
        }
      }
      /**
       * Yields one `{ event, name, value, arg, def }` argInfo object for each arg in `process.argv` (or `options.argv`).
       */
      *[Symbol.iterator]() {
        const definitions = this.definitions;
        let def;
        let value;
        let name;
        let event;
        let singularDefaultSet = false;
        let unknownFound = false;
        let origArg;
        for (let arg of this.argv) {
          if (t.isPlainObject(arg)) {
            origArg = arg.origArg;
            arg = arg.arg;
          }
          if (unknownFound && this.options.stopAtFirstUnknown) {
            yield { event: "unknown_value", arg, name: "_unknown", value: void 0 };
            continue;
          }
          if (isOption(arg)) {
            def = definitions.get(arg, this.options.caseInsensitive);
            value = void 0;
            if (def) {
              value = def.isBoolean() ? true : null;
              event = "set";
            } else {
              event = "unknown_option";
            }
          } else if (isOptionEqualsNotation(arg)) {
            const matches = arg.match(re.optEquals);
            def = definitions.get(matches[1], this.options.caseInsensitive);
            if (def) {
              if (def.isBoolean()) {
                yield { event: "unknown_value", arg, name: "_unknown", value, def };
                event = "set";
                value = true;
              } else {
                event = "set";
                value = matches[2];
              }
            } else {
              event = "unknown_option";
            }
          } else if (isValue(arg)) {
            if (def) {
              value = arg;
              event = "set";
            } else {
              def = this.definitions.getDefault();
              if (def && !singularDefaultSet) {
                value = arg;
                event = "set";
              } else {
                event = "unknown_value";
                def = void 0;
              }
            }
          }
          name = def ? def.name : "_unknown";
          const argInfo = { event, arg, name, value, def };
          if (origArg) {
            argInfo.subArg = arg;
            argInfo.arg = origArg;
          }
          yield argInfo;
          if (name === "_unknown")
            unknownFound = true;
          if (def && def.defaultOption && !def.isMultiple() && event === "set")
            singularDefaultSet = true;
          if (def && def.isBoolean())
            def = void 0;
          if (def && !def.multiple && t.isDefined(value) && value !== null) {
            def = void 0;
          }
          value = void 0;
          event = void 0;
          name = void 0;
          origArg = void 0;
        }
      }
    };
    var _value = /* @__PURE__ */ new WeakMap();
    var Option = class {
      constructor(definition) {
        this.definition = new OptionDefinition(definition);
        this.state = null;
        this.resetToDefault();
      }
      get() {
        return _value.get(this);
      }
      set(val) {
        this._set(val, "set");
      }
      _set(val, state) {
        const def = this.definition;
        if (def.isMultiple()) {
          if (val !== null && val !== void 0) {
            const arr = this.get();
            if (this.state === "default")
              arr.length = 0;
            arr.push(def.type(val));
            this.state = state;
          }
        } else {
          if (!def.isMultiple() && this.state === "set") {
            const err = new Error(`Singular option already set [${this.definition.name}=${this.get()}]`);
            err.name = "ALREADY_SET";
            err.value = val;
            err.optionName = def.name;
            throw err;
          } else if (val === null || val === void 0) {
            _value.set(this, val);
          } else {
            _value.set(this, def.type(val));
            this.state = state;
          }
        }
      }
      resetToDefault() {
        if (t.isDefined(this.definition.defaultValue)) {
          if (this.definition.isMultiple()) {
            _value.set(this, arrayify(this.definition.defaultValue).slice());
          } else {
            _value.set(this, this.definition.defaultValue);
          }
        } else {
          if (this.definition.isMultiple()) {
            _value.set(this, []);
          } else {
            _value.set(this, null);
          }
        }
        this.state = "default";
      }
      static create(definition) {
        definition = new OptionDefinition(definition);
        if (definition.isBoolean()) {
          return FlagOption.create(definition);
        } else {
          return new this(definition);
        }
      }
    };
    var FlagOption = class extends Option {
      set(val) {
        super.set(true);
      }
      static create(def) {
        return new this(def);
      }
    };
    var Output = class extends Map {
      constructor(definitions) {
        super();
        this.definitions = Definitions.from(definitions);
        this.set("_unknown", Option.create({ name: "_unknown", multiple: true }));
        for (const def of this.definitions.whereDefaultValueSet()) {
          this.set(def.name, Option.create(def));
        }
      }
      toObject(options) {
        options = options || {};
        const output = {};
        for (const item of this) {
          const name = options.camelCase && item[0] !== "_unknown" ? camelCase(item[0]) : item[0];
          const option = item[1];
          if (name === "_unknown" && !option.get().length)
            continue;
          output[name] = option.get();
        }
        if (options.skipUnknown)
          delete output._unknown;
        return output;
      }
    };
    var GroupedOutput = class extends Output {
      toObject(options) {
        const superOutputNoCamel = super.toObject({ skipUnknown: options.skipUnknown });
        const superOutput = super.toObject(options);
        const unknown = superOutput._unknown;
        delete superOutput._unknown;
        const grouped = {
          _all: superOutput
        };
        if (unknown && unknown.length)
          grouped._unknown = unknown;
        this.definitions.whereGrouped().forEach((def) => {
          const name = options.camelCase ? camelCase(def.name) : def.name;
          const outputValue = superOutputNoCamel[def.name];
          for (const groupName of arrayify(def.group)) {
            grouped[groupName] = grouped[groupName] || {};
            if (t.isDefined(outputValue)) {
              grouped[groupName][name] = outputValue;
            }
          }
        });
        this.definitions.whereNotGrouped().forEach((def) => {
          const name = options.camelCase ? camelCase(def.name) : def.name;
          const outputValue = superOutputNoCamel[def.name];
          if (t.isDefined(outputValue)) {
            if (!grouped._none)
              grouped._none = {};
            grouped._none[name] = outputValue;
          }
        });
        return grouped;
      }
    };
    function commandLineArgs(optionDefinitions, options) {
      options = options || {};
      if (options.stopAtFirstUnknown)
        options.partial = true;
      optionDefinitions = Definitions.from(optionDefinitions, options.caseInsensitive);
      const parser = new ArgvParser(optionDefinitions, {
        argv: options.argv,
        stopAtFirstUnknown: options.stopAtFirstUnknown,
        caseInsensitive: options.caseInsensitive
      });
      const OutputClass = optionDefinitions.isGrouped() ? GroupedOutput : Output;
      const output = new OutputClass(optionDefinitions);
      for (const argInfo of parser) {
        const arg = argInfo.subArg || argInfo.arg;
        if (!options.partial) {
          if (argInfo.event === "unknown_value") {
            const err = new Error(`Unknown value: ${arg}`);
            err.name = "UNKNOWN_VALUE";
            err.value = arg;
            throw err;
          } else if (argInfo.event === "unknown_option") {
            const err = new Error(`Unknown option: ${arg}`);
            err.name = "UNKNOWN_OPTION";
            err.optionName = arg;
            throw err;
          }
        }
        let option;
        if (output.has(argInfo.name)) {
          option = output.get(argInfo.name);
        } else {
          option = Option.create(argInfo.def);
          output.set(argInfo.name, option);
        }
        if (argInfo.name === "_unknown") {
          option.set(arg);
        } else {
          option.set(argInfo.value);
        }
      }
      return output.toObject({ skipUnknown: !options.partial, camelCase: options.camelCase });
    }
    module2.exports = commandLineArgs;
  }
});

// node_modules/command-line-usage/node_modules/array-back/dist/index.js
var require_dist2 = __commonJS({
  "node_modules/command-line-usage/node_modules/array-back/dist/index.js"(exports, module2) {
    (function(global2, factory) {
      typeof exports === "object" && typeof module2 !== "undefined" ? module2.exports = factory() : typeof define === "function" && define.amd ? define(factory) : (global2 = global2 || self, global2.arrayBack = factory());
    })(exports, function() {
      "use strict";
      function isObject(input) {
        return typeof input === "object" && input !== null;
      }
      function isArrayLike(input) {
        return isObject(input) && typeof input.length === "number";
      }
      function arrayify(input) {
        if (Array.isArray(input)) {
          return input;
        }
        if (input === void 0) {
          return [];
        }
        if (isArrayLike(input) || input instanceof Set) {
          return Array.from(input);
        }
        return [input];
      }
      return arrayify;
    });
  }
});

// node_modules/command-line-usage/node_modules/escape-string-regexp/index.js
var require_escape_string_regexp = __commonJS({
  "node_modules/command-line-usage/node_modules/escape-string-regexp/index.js"(exports, module2) {
    "use strict";
    var matchOperatorsRe = /[|\\{}()[\]^$+*?.]/g;
    module2.exports = function(str) {
      if (typeof str !== "string") {
        throw new TypeError("Expected a string");
      }
      return str.replace(matchOperatorsRe, "\\$&");
    };
  }
});

// node_modules/command-line-usage/node_modules/color-name/index.js
var require_color_name = __commonJS({
  "node_modules/command-line-usage/node_modules/color-name/index.js"(exports, module2) {
    "use strict";
    module2.exports = {
      "aliceblue": [240, 248, 255],
      "antiquewhite": [250, 235, 215],
      "aqua": [0, 255, 255],
      "aquamarine": [127, 255, 212],
      "azure": [240, 255, 255],
      "beige": [245, 245, 220],
      "bisque": [255, 228, 196],
      "black": [0, 0, 0],
      "blanchedalmond": [255, 235, 205],
      "blue": [0, 0, 255],
      "blueviolet": [138, 43, 226],
      "brown": [165, 42, 42],
      "burlywood": [222, 184, 135],
      "cadetblue": [95, 158, 160],
      "chartreuse": [127, 255, 0],
      "chocolate": [210, 105, 30],
      "coral": [255, 127, 80],
      "cornflowerblue": [100, 149, 237],
      "cornsilk": [255, 248, 220],
      "crimson": [220, 20, 60],
      "cyan": [0, 255, 255],
      "darkblue": [0, 0, 139],
      "darkcyan": [0, 139, 139],
      "darkgoldenrod": [184, 134, 11],
      "darkgray": [169, 169, 169],
      "darkgreen": [0, 100, 0],
      "darkgrey": [169, 169, 169],
      "darkkhaki": [189, 183, 107],
      "darkmagenta": [139, 0, 139],
      "darkolivegreen": [85, 107, 47],
      "darkorange": [255, 140, 0],
      "darkorchid": [153, 50, 204],
      "darkred": [139, 0, 0],
      "darksalmon": [233, 150, 122],
      "darkseagreen": [143, 188, 143],
      "darkslateblue": [72, 61, 139],
      "darkslategray": [47, 79, 79],
      "darkslategrey": [47, 79, 79],
      "darkturquoise": [0, 206, 209],
      "darkviolet": [148, 0, 211],
      "deeppink": [255, 20, 147],
      "deepskyblue": [0, 191, 255],
      "dimgray": [105, 105, 105],
      "dimgrey": [105, 105, 105],
      "dodgerblue": [30, 144, 255],
      "firebrick": [178, 34, 34],
      "floralwhite": [255, 250, 240],
      "forestgreen": [34, 139, 34],
      "fuchsia": [255, 0, 255],
      "gainsboro": [220, 220, 220],
      "ghostwhite": [248, 248, 255],
      "gold": [255, 215, 0],
      "goldenrod": [218, 165, 32],
      "gray": [128, 128, 128],
      "green": [0, 128, 0],
      "greenyellow": [173, 255, 47],
      "grey": [128, 128, 128],
      "honeydew": [240, 255, 240],
      "hotpink": [255, 105, 180],
      "indianred": [205, 92, 92],
      "indigo": [75, 0, 130],
      "ivory": [255, 255, 240],
      "khaki": [240, 230, 140],
      "lavender": [230, 230, 250],
      "lavenderblush": [255, 240, 245],
      "lawngreen": [124, 252, 0],
      "lemonchiffon": [255, 250, 205],
      "lightblue": [173, 216, 230],
      "lightcoral": [240, 128, 128],
      "lightcyan": [224, 255, 255],
      "lightgoldenrodyellow": [250, 250, 210],
      "lightgray": [211, 211, 211],
      "lightgreen": [144, 238, 144],
      "lightgrey": [211, 211, 211],
      "lightpink": [255, 182, 193],
      "lightsalmon": [255, 160, 122],
      "lightseagreen": [32, 178, 170],
      "lightskyblue": [135, 206, 250],
      "lightslategray": [119, 136, 153],
      "lightslategrey": [119, 136, 153],
      "lightsteelblue": [176, 196, 222],
      "lightyellow": [255, 255, 224],
      "lime": [0, 255, 0],
      "limegreen": [50, 205, 50],
      "linen": [250, 240, 230],
      "magenta": [255, 0, 255],
      "maroon": [128, 0, 0],
      "mediumaquamarine": [102, 205, 170],
      "mediumblue": [0, 0, 205],
      "mediumorchid": [186, 85, 211],
      "mediumpurple": [147, 112, 219],
      "mediumseagreen": [60, 179, 113],
      "mediumslateblue": [123, 104, 238],
      "mediumspringgreen": [0, 250, 154],
      "mediumturquoise": [72, 209, 204],
      "mediumvioletred": [199, 21, 133],
      "midnightblue": [25, 25, 112],
      "mintcream": [245, 255, 250],
      "mistyrose": [255, 228, 225],
      "moccasin": [255, 228, 181],
      "navajowhite": [255, 222, 173],
      "navy": [0, 0, 128],
      "oldlace": [253, 245, 230],
      "olive": [128, 128, 0],
      "olivedrab": [107, 142, 35],
      "orange": [255, 165, 0],
      "orangered": [255, 69, 0],
      "orchid": [218, 112, 214],
      "palegoldenrod": [238, 232, 170],
      "palegreen": [152, 251, 152],
      "paleturquoise": [175, 238, 238],
      "palevioletred": [219, 112, 147],
      "papayawhip": [255, 239, 213],
      "peachpuff": [255, 218, 185],
      "peru": [205, 133, 63],
      "pink": [255, 192, 203],
      "plum": [221, 160, 221],
      "powderblue": [176, 224, 230],
      "purple": [128, 0, 128],
      "rebeccapurple": [102, 51, 153],
      "red": [255, 0, 0],
      "rosybrown": [188, 143, 143],
      "royalblue": [65, 105, 225],
      "saddlebrown": [139, 69, 19],
      "salmon": [250, 128, 114],
      "sandybrown": [244, 164, 96],
      "seagreen": [46, 139, 87],
      "seashell": [255, 245, 238],
      "sienna": [160, 82, 45],
      "silver": [192, 192, 192],
      "skyblue": [135, 206, 235],
      "slateblue": [106, 90, 205],
      "slategray": [112, 128, 144],
      "slategrey": [112, 128, 144],
      "snow": [255, 250, 250],
      "springgreen": [0, 255, 127],
      "steelblue": [70, 130, 180],
      "tan": [210, 180, 140],
      "teal": [0, 128, 128],
      "thistle": [216, 191, 216],
      "tomato": [255, 99, 71],
      "turquoise": [64, 224, 208],
      "violet": [238, 130, 238],
      "wheat": [245, 222, 179],
      "white": [255, 255, 255],
      "whitesmoke": [245, 245, 245],
      "yellow": [255, 255, 0],
      "yellowgreen": [154, 205, 50]
    };
  }
});

// node_modules/command-line-usage/node_modules/color-convert/conversions.js
var require_conversions = __commonJS({
  "node_modules/command-line-usage/node_modules/color-convert/conversions.js"(exports, module2) {
    var cssKeywords = require_color_name();
    var reverseKeywords = {};
    for (key in cssKeywords) {
      if (cssKeywords.hasOwnProperty(key)) {
        reverseKeywords[cssKeywords[key]] = key;
      }
    }
    var key;
    var convert = module2.exports = {
      rgb: { channels: 3, labels: "rgb" },
      hsl: { channels: 3, labels: "hsl" },
      hsv: { channels: 3, labels: "hsv" },
      hwb: { channels: 3, labels: "hwb" },
      cmyk: { channels: 4, labels: "cmyk" },
      xyz: { channels: 3, labels: "xyz" },
      lab: { channels: 3, labels: "lab" },
      lch: { channels: 3, labels: "lch" },
      hex: { channels: 1, labels: ["hex"] },
      keyword: { channels: 1, labels: ["keyword"] },
      ansi16: { channels: 1, labels: ["ansi16"] },
      ansi256: { channels: 1, labels: ["ansi256"] },
      hcg: { channels: 3, labels: ["h", "c", "g"] },
      apple: { channels: 3, labels: ["r16", "g16", "b16"] },
      gray: { channels: 1, labels: ["gray"] }
    };
    for (model in convert) {
      if (convert.hasOwnProperty(model)) {
        if (!("channels" in convert[model])) {
          throw new Error("missing channels property: " + model);
        }
        if (!("labels" in convert[model])) {
          throw new Error("missing channel labels property: " + model);
        }
        if (convert[model].labels.length !== convert[model].channels) {
          throw new Error("channel and label counts mismatch: " + model);
        }
        channels = convert[model].channels;
        labels = convert[model].labels;
        delete convert[model].channels;
        delete convert[model].labels;
        Object.defineProperty(convert[model], "channels", { value: channels });
        Object.defineProperty(convert[model], "labels", { value: labels });
      }
    }
    var channels;
    var labels;
    var model;
    convert.rgb.hsl = function(rgb) {
      var r = rgb[0] / 255;
      var g = rgb[1] / 255;
      var b = rgb[2] / 255;
      var min = Math.min(r, g, b);
      var max = Math.max(r, g, b);
      var delta = max - min;
      var h;
      var s;
      var l;
      if (max === min) {
        h = 0;
      } else if (r === max) {
        h = (g - b) / delta;
      } else if (g === max) {
        h = 2 + (b - r) / delta;
      } else if (b === max) {
        h = 4 + (r - g) / delta;
      }
      h = Math.min(h * 60, 360);
      if (h < 0) {
        h += 360;
      }
      l = (min + max) / 2;
      if (max === min) {
        s = 0;
      } else if (l <= 0.5) {
        s = delta / (max + min);
      } else {
        s = delta / (2 - max - min);
      }
      return [h, s * 100, l * 100];
    };
    convert.rgb.hsv = function(rgb) {
      var rdif;
      var gdif;
      var bdif;
      var h;
      var s;
      var r = rgb[0] / 255;
      var g = rgb[1] / 255;
      var b = rgb[2] / 255;
      var v = Math.max(r, g, b);
      var diff = v - Math.min(r, g, b);
      var diffc = function(c) {
        return (v - c) / 6 / diff + 1 / 2;
      };
      if (diff === 0) {
        h = s = 0;
      } else {
        s = diff / v;
        rdif = diffc(r);
        gdif = diffc(g);
        bdif = diffc(b);
        if (r === v) {
          h = bdif - gdif;
        } else if (g === v) {
          h = 1 / 3 + rdif - bdif;
        } else if (b === v) {
          h = 2 / 3 + gdif - rdif;
        }
        if (h < 0) {
          h += 1;
        } else if (h > 1) {
          h -= 1;
        }
      }
      return [
        h * 360,
        s * 100,
        v * 100
      ];
    };
    convert.rgb.hwb = function(rgb) {
      var r = rgb[0];
      var g = rgb[1];
      var b = rgb[2];
      var h = convert.rgb.hsl(rgb)[0];
      var w = 1 / 255 * Math.min(r, Math.min(g, b));
      b = 1 - 1 / 255 * Math.max(r, Math.max(g, b));
      return [h, w * 100, b * 100];
    };
    convert.rgb.cmyk = function(rgb) {
      var r = rgb[0] / 255;
      var g = rgb[1] / 255;
      var b = rgb[2] / 255;
      var c;
      var m;
      var y;
      var k;
      k = Math.min(1 - r, 1 - g, 1 - b);
      c = (1 - r - k) / (1 - k) || 0;
      m = (1 - g - k) / (1 - k) || 0;
      y = (1 - b - k) / (1 - k) || 0;
      return [c * 100, m * 100, y * 100, k * 100];
    };
    function comparativeDistance(x, y) {
      return Math.pow(x[0] - y[0], 2) + Math.pow(x[1] - y[1], 2) + Math.pow(x[2] - y[2], 2);
    }
    convert.rgb.keyword = function(rgb) {
      var reversed = reverseKeywords[rgb];
      if (reversed) {
        return reversed;
      }
      var currentClosestDistance = Infinity;
      var currentClosestKeyword;
      for (var keyword in cssKeywords) {
        if (cssKeywords.hasOwnProperty(keyword)) {
          var value = cssKeywords[keyword];
          var distance = comparativeDistance(rgb, value);
          if (distance < currentClosestDistance) {
            currentClosestDistance = distance;
            currentClosestKeyword = keyword;
          }
        }
      }
      return currentClosestKeyword;
    };
    convert.keyword.rgb = function(keyword) {
      return cssKeywords[keyword];
    };
    convert.rgb.xyz = function(rgb) {
      var r = rgb[0] / 255;
      var g = rgb[1] / 255;
      var b = rgb[2] / 255;
      r = r > 0.04045 ? Math.pow((r + 0.055) / 1.055, 2.4) : r / 12.92;
      g = g > 0.04045 ? Math.pow((g + 0.055) / 1.055, 2.4) : g / 12.92;
      b = b > 0.04045 ? Math.pow((b + 0.055) / 1.055, 2.4) : b / 12.92;
      var x = r * 0.4124 + g * 0.3576 + b * 0.1805;
      var y = r * 0.2126 + g * 0.7152 + b * 0.0722;
      var z = r * 0.0193 + g * 0.1192 + b * 0.9505;
      return [x * 100, y * 100, z * 100];
    };
    convert.rgb.lab = function(rgb) {
      var xyz = convert.rgb.xyz(rgb);
      var x = xyz[0];
      var y = xyz[1];
      var z = xyz[2];
      var l;
      var a;
      var b;
      x /= 95.047;
      y /= 100;
      z /= 108.883;
      x = x > 8856e-6 ? Math.pow(x, 1 / 3) : 7.787 * x + 16 / 116;
      y = y > 8856e-6 ? Math.pow(y, 1 / 3) : 7.787 * y + 16 / 116;
      z = z > 8856e-6 ? Math.pow(z, 1 / 3) : 7.787 * z + 16 / 116;
      l = 116 * y - 16;
      a = 500 * (x - y);
      b = 200 * (y - z);
      return [l, a, b];
    };
    convert.hsl.rgb = function(hsl) {
      var h = hsl[0] / 360;
      var s = hsl[1] / 100;
      var l = hsl[2] / 100;
      var t1;
      var t2;
      var t3;
      var rgb;
      var val;
      if (s === 0) {
        val = l * 255;
        return [val, val, val];
      }
      if (l < 0.5) {
        t2 = l * (1 + s);
      } else {
        t2 = l + s - l * s;
      }
      t1 = 2 * l - t2;
      rgb = [0, 0, 0];
      for (var i = 0; i < 3; i++) {
        t3 = h + 1 / 3 * -(i - 1);
        if (t3 < 0) {
          t3++;
        }
        if (t3 > 1) {
          t3--;
        }
        if (6 * t3 < 1) {
          val = t1 + (t2 - t1) * 6 * t3;
        } else if (2 * t3 < 1) {
          val = t2;
        } else if (3 * t3 < 2) {
          val = t1 + (t2 - t1) * (2 / 3 - t3) * 6;
        } else {
          val = t1;
        }
        rgb[i] = val * 255;
      }
      return rgb;
    };
    convert.hsl.hsv = function(hsl) {
      var h = hsl[0];
      var s = hsl[1] / 100;
      var l = hsl[2] / 100;
      var smin = s;
      var lmin = Math.max(l, 0.01);
      var sv;
      var v;
      l *= 2;
      s *= l <= 1 ? l : 2 - l;
      smin *= lmin <= 1 ? lmin : 2 - lmin;
      v = (l + s) / 2;
      sv = l === 0 ? 2 * smin / (lmin + smin) : 2 * s / (l + s);
      return [h, sv * 100, v * 100];
    };
    convert.hsv.rgb = function(hsv) {
      var h = hsv[0] / 60;
      var s = hsv[1] / 100;
      var v = hsv[2] / 100;
      var hi = Math.floor(h) % 6;
      var f = h - Math.floor(h);
      var p = 255 * v * (1 - s);
      var q = 255 * v * (1 - s * f);
      var t = 255 * v * (1 - s * (1 - f));
      v *= 255;
      switch (hi) {
        case 0:
          return [v, t, p];
        case 1:
          return [q, v, p];
        case 2:
          return [p, v, t];
        case 3:
          return [p, q, v];
        case 4:
          return [t, p, v];
        case 5:
          return [v, p, q];
      }
    };
    convert.hsv.hsl = function(hsv) {
      var h = hsv[0];
      var s = hsv[1] / 100;
      var v = hsv[2] / 100;
      var vmin = Math.max(v, 0.01);
      var lmin;
      var sl;
      var l;
      l = (2 - s) * v;
      lmin = (2 - s) * vmin;
      sl = s * vmin;
      sl /= lmin <= 1 ? lmin : 2 - lmin;
      sl = sl || 0;
      l /= 2;
      return [h, sl * 100, l * 100];
    };
    convert.hwb.rgb = function(hwb) {
      var h = hwb[0] / 360;
      var wh = hwb[1] / 100;
      var bl = hwb[2] / 100;
      var ratio = wh + bl;
      var i;
      var v;
      var f;
      var n;
      if (ratio > 1) {
        wh /= ratio;
        bl /= ratio;
      }
      i = Math.floor(6 * h);
      v = 1 - bl;
      f = 6 * h - i;
      if ((i & 1) !== 0) {
        f = 1 - f;
      }
      n = wh + f * (v - wh);
      var r;
      var g;
      var b;
      switch (i) {
        default:
        case 6:
        case 0:
          r = v;
          g = n;
          b = wh;
          break;
        case 1:
          r = n;
          g = v;
          b = wh;
          break;
        case 2:
          r = wh;
          g = v;
          b = n;
          break;
        case 3:
          r = wh;
          g = n;
          b = v;
          break;
        case 4:
          r = n;
          g = wh;
          b = v;
          break;
        case 5:
          r = v;
          g = wh;
          b = n;
          break;
      }
      return [r * 255, g * 255, b * 255];
    };
    convert.cmyk.rgb = function(cmyk) {
      var c = cmyk[0] / 100;
      var m = cmyk[1] / 100;
      var y = cmyk[2] / 100;
      var k = cmyk[3] / 100;
      var r;
      var g;
      var b;
      r = 1 - Math.min(1, c * (1 - k) + k);
      g = 1 - Math.min(1, m * (1 - k) + k);
      b = 1 - Math.min(1, y * (1 - k) + k);
      return [r * 255, g * 255, b * 255];
    };
    convert.xyz.rgb = function(xyz) {
      var x = xyz[0] / 100;
      var y = xyz[1] / 100;
      var z = xyz[2] / 100;
      var r;
      var g;
      var b;
      r = x * 3.2406 + y * -1.5372 + z * -0.4986;
      g = x * -0.9689 + y * 1.8758 + z * 0.0415;
      b = x * 0.0557 + y * -0.204 + z * 1.057;
      r = r > 31308e-7 ? 1.055 * Math.pow(r, 1 / 2.4) - 0.055 : r * 12.92;
      g = g > 31308e-7 ? 1.055 * Math.pow(g, 1 / 2.4) - 0.055 : g * 12.92;
      b = b > 31308e-7 ? 1.055 * Math.pow(b, 1 / 2.4) - 0.055 : b * 12.92;
      r = Math.min(Math.max(0, r), 1);
      g = Math.min(Math.max(0, g), 1);
      b = Math.min(Math.max(0, b), 1);
      return [r * 255, g * 255, b * 255];
    };
    convert.xyz.lab = function(xyz) {
      var x = xyz[0];
      var y = xyz[1];
      var z = xyz[2];
      var l;
      var a;
      var b;
      x /= 95.047;
      y /= 100;
      z /= 108.883;
      x = x > 8856e-6 ? Math.pow(x, 1 / 3) : 7.787 * x + 16 / 116;
      y = y > 8856e-6 ? Math.pow(y, 1 / 3) : 7.787 * y + 16 / 116;
      z = z > 8856e-6 ? Math.pow(z, 1 / 3) : 7.787 * z + 16 / 116;
      l = 116 * y - 16;
      a = 500 * (x - y);
      b = 200 * (y - z);
      return [l, a, b];
    };
    convert.lab.xyz = function(lab) {
      var l = lab[0];
      var a = lab[1];
      var b = lab[2];
      var x;
      var y;
      var z;
      y = (l + 16) / 116;
      x = a / 500 + y;
      z = y - b / 200;
      var y2 = Math.pow(y, 3);
      var x2 = Math.pow(x, 3);
      var z2 = Math.pow(z, 3);
      y = y2 > 8856e-6 ? y2 : (y - 16 / 116) / 7.787;
      x = x2 > 8856e-6 ? x2 : (x - 16 / 116) / 7.787;
      z = z2 > 8856e-6 ? z2 : (z - 16 / 116) / 7.787;
      x *= 95.047;
      y *= 100;
      z *= 108.883;
      return [x, y, z];
    };
    convert.lab.lch = function(lab) {
      var l = lab[0];
      var a = lab[1];
      var b = lab[2];
      var hr;
      var h;
      var c;
      hr = Math.atan2(b, a);
      h = hr * 360 / 2 / Math.PI;
      if (h < 0) {
        h += 360;
      }
      c = Math.sqrt(a * a + b * b);
      return [l, c, h];
    };
    convert.lch.lab = function(lch) {
      var l = lch[0];
      var c = lch[1];
      var h = lch[2];
      var a;
      var b;
      var hr;
      hr = h / 360 * 2 * Math.PI;
      a = c * Math.cos(hr);
      b = c * Math.sin(hr);
      return [l, a, b];
    };
    convert.rgb.ansi16 = function(args) {
      var r = args[0];
      var g = args[1];
      var b = args[2];
      var value = 1 in arguments ? arguments[1] : convert.rgb.hsv(args)[2];
      value = Math.round(value / 50);
      if (value === 0) {
        return 30;
      }
      var ansi = 30 + (Math.round(b / 255) << 2 | Math.round(g / 255) << 1 | Math.round(r / 255));
      if (value === 2) {
        ansi += 60;
      }
      return ansi;
    };
    convert.hsv.ansi16 = function(args) {
      return convert.rgb.ansi16(convert.hsv.rgb(args), args[2]);
    };
    convert.rgb.ansi256 = function(args) {
      var r = args[0];
      var g = args[1];
      var b = args[2];
      if (r === g && g === b) {
        if (r < 8) {
          return 16;
        }
        if (r > 248) {
          return 231;
        }
        return Math.round((r - 8) / 247 * 24) + 232;
      }
      var ansi = 16 + 36 * Math.round(r / 255 * 5) + 6 * Math.round(g / 255 * 5) + Math.round(b / 255 * 5);
      return ansi;
    };
    convert.ansi16.rgb = function(args) {
      var color = args % 10;
      if (color === 0 || color === 7) {
        if (args > 50) {
          color += 3.5;
        }
        color = color / 10.5 * 255;
        return [color, color, color];
      }
      var mult = (~~(args > 50) + 1) * 0.5;
      var r = (color & 1) * mult * 255;
      var g = (color >> 1 & 1) * mult * 255;
      var b = (color >> 2 & 1) * mult * 255;
      return [r, g, b];
    };
    convert.ansi256.rgb = function(args) {
      if (args >= 232) {
        var c = (args - 232) * 10 + 8;
        return [c, c, c];
      }
      args -= 16;
      var rem;
      var r = Math.floor(args / 36) / 5 * 255;
      var g = Math.floor((rem = args % 36) / 6) / 5 * 255;
      var b = rem % 6 / 5 * 255;
      return [r, g, b];
    };
    convert.rgb.hex = function(args) {
      var integer = ((Math.round(args[0]) & 255) << 16) + ((Math.round(args[1]) & 255) << 8) + (Math.round(args[2]) & 255);
      var string = integer.toString(16).toUpperCase();
      return "000000".substring(string.length) + string;
    };
    convert.hex.rgb = function(args) {
      var match = args.toString(16).match(/[a-f0-9]{6}|[a-f0-9]{3}/i);
      if (!match) {
        return [0, 0, 0];
      }
      var colorString = match[0];
      if (match[0].length === 3) {
        colorString = colorString.split("").map(function(char) {
          return char + char;
        }).join("");
      }
      var integer = parseInt(colorString, 16);
      var r = integer >> 16 & 255;
      var g = integer >> 8 & 255;
      var b = integer & 255;
      return [r, g, b];
    };
    convert.rgb.hcg = function(rgb) {
      var r = rgb[0] / 255;
      var g = rgb[1] / 255;
      var b = rgb[2] / 255;
      var max = Math.max(Math.max(r, g), b);
      var min = Math.min(Math.min(r, g), b);
      var chroma = max - min;
      var grayscale;
      var hue;
      if (chroma < 1) {
        grayscale = min / (1 - chroma);
      } else {
        grayscale = 0;
      }
      if (chroma <= 0) {
        hue = 0;
      } else if (max === r) {
        hue = (g - b) / chroma % 6;
      } else if (max === g) {
        hue = 2 + (b - r) / chroma;
      } else {
        hue = 4 + (r - g) / chroma + 4;
      }
      hue /= 6;
      hue %= 1;
      return [hue * 360, chroma * 100, grayscale * 100];
    };
    convert.hsl.hcg = function(hsl) {
      var s = hsl[1] / 100;
      var l = hsl[2] / 100;
      var c = 1;
      var f = 0;
      if (l < 0.5) {
        c = 2 * s * l;
      } else {
        c = 2 * s * (1 - l);
      }
      if (c < 1) {
        f = (l - 0.5 * c) / (1 - c);
      }
      return [hsl[0], c * 100, f * 100];
    };
    convert.hsv.hcg = function(hsv) {
      var s = hsv[1] / 100;
      var v = hsv[2] / 100;
      var c = s * v;
      var f = 0;
      if (c < 1) {
        f = (v - c) / (1 - c);
      }
      return [hsv[0], c * 100, f * 100];
    };
    convert.hcg.rgb = function(hcg) {
      var h = hcg[0] / 360;
      var c = hcg[1] / 100;
      var g = hcg[2] / 100;
      if (c === 0) {
        return [g * 255, g * 255, g * 255];
      }
      var pure = [0, 0, 0];
      var hi = h % 1 * 6;
      var v = hi % 1;
      var w = 1 - v;
      var mg = 0;
      switch (Math.floor(hi)) {
        case 0:
          pure[0] = 1;
          pure[1] = v;
          pure[2] = 0;
          break;
        case 1:
          pure[0] = w;
          pure[1] = 1;
          pure[2] = 0;
          break;
        case 2:
          pure[0] = 0;
          pure[1] = 1;
          pure[2] = v;
          break;
        case 3:
          pure[0] = 0;
          pure[1] = w;
          pure[2] = 1;
          break;
        case 4:
          pure[0] = v;
          pure[1] = 0;
          pure[2] = 1;
          break;
        default:
          pure[0] = 1;
          pure[1] = 0;
          pure[2] = w;
      }
      mg = (1 - c) * g;
      return [
        (c * pure[0] + mg) * 255,
        (c * pure[1] + mg) * 255,
        (c * pure[2] + mg) * 255
      ];
    };
    convert.hcg.hsv = function(hcg) {
      var c = hcg[1] / 100;
      var g = hcg[2] / 100;
      var v = c + g * (1 - c);
      var f = 0;
      if (v > 0) {
        f = c / v;
      }
      return [hcg[0], f * 100, v * 100];
    };
    convert.hcg.hsl = function(hcg) {
      var c = hcg[1] / 100;
      var g = hcg[2] / 100;
      var l = g * (1 - c) + 0.5 * c;
      var s = 0;
      if (l > 0 && l < 0.5) {
        s = c / (2 * l);
      } else if (l >= 0.5 && l < 1) {
        s = c / (2 * (1 - l));
      }
      return [hcg[0], s * 100, l * 100];
    };
    convert.hcg.hwb = function(hcg) {
      var c = hcg[1] / 100;
      var g = hcg[2] / 100;
      var v = c + g * (1 - c);
      return [hcg[0], (v - c) * 100, (1 - v) * 100];
    };
    convert.hwb.hcg = function(hwb) {
      var w = hwb[1] / 100;
      var b = hwb[2] / 100;
      var v = 1 - b;
      var c = v - w;
      var g = 0;
      if (c < 1) {
        g = (v - c) / (1 - c);
      }
      return [hwb[0], c * 100, g * 100];
    };
    convert.apple.rgb = function(apple) {
      return [apple[0] / 65535 * 255, apple[1] / 65535 * 255, apple[2] / 65535 * 255];
    };
    convert.rgb.apple = function(rgb) {
      return [rgb[0] / 255 * 65535, rgb[1] / 255 * 65535, rgb[2] / 255 * 65535];
    };
    convert.gray.rgb = function(args) {
      return [args[0] / 100 * 255, args[0] / 100 * 255, args[0] / 100 * 255];
    };
    convert.gray.hsl = convert.gray.hsv = function(args) {
      return [0, 0, args[0]];
    };
    convert.gray.hwb = function(gray) {
      return [0, 100, gray[0]];
    };
    convert.gray.cmyk = function(gray) {
      return [0, 0, 0, gray[0]];
    };
    convert.gray.lab = function(gray) {
      return [gray[0], 0, 0];
    };
    convert.gray.hex = function(gray) {
      var val = Math.round(gray[0] / 100 * 255) & 255;
      var integer = (val << 16) + (val << 8) + val;
      var string = integer.toString(16).toUpperCase();
      return "000000".substring(string.length) + string;
    };
    convert.rgb.gray = function(rgb) {
      var val = (rgb[0] + rgb[1] + rgb[2]) / 3;
      return [val / 255 * 100];
    };
  }
});

// node_modules/command-line-usage/node_modules/color-convert/route.js
var require_route = __commonJS({
  "node_modules/command-line-usage/node_modules/color-convert/route.js"(exports, module2) {
    var conversions = require_conversions();
    function buildGraph() {
      var graph = {};
      var models = Object.keys(conversions);
      for (var len = models.length, i = 0; i < len; i++) {
        graph[models[i]] = {
          // http://jsperf.com/1-vs-infinity
          // micro-opt, but this is simple.
          distance: -1,
          parent: null
        };
      }
      return graph;
    }
    function deriveBFS(fromModel) {
      var graph = buildGraph();
      var queue = [fromModel];
      graph[fromModel].distance = 0;
      while (queue.length) {
        var current = queue.pop();
        var adjacents = Object.keys(conversions[current]);
        for (var len = adjacents.length, i = 0; i < len; i++) {
          var adjacent = adjacents[i];
          var node = graph[adjacent];
          if (node.distance === -1) {
            node.distance = graph[current].distance + 1;
            node.parent = current;
            queue.unshift(adjacent);
          }
        }
      }
      return graph;
    }
    function link(from, to) {
      return function(args) {
        return to(from(args));
      };
    }
    function wrapConversion(toModel, graph) {
      var path3 = [graph[toModel].parent, toModel];
      var fn = conversions[graph[toModel].parent][toModel];
      var cur = graph[toModel].parent;
      while (graph[cur].parent) {
        path3.unshift(graph[cur].parent);
        fn = link(conversions[graph[cur].parent][cur], fn);
        cur = graph[cur].parent;
      }
      fn.conversion = path3;
      return fn;
    }
    module2.exports = function(fromModel) {
      var graph = deriveBFS(fromModel);
      var conversion = {};
      var models = Object.keys(graph);
      for (var len = models.length, i = 0; i < len; i++) {
        var toModel = models[i];
        var node = graph[toModel];
        if (node.parent === null) {
          continue;
        }
        conversion[toModel] = wrapConversion(toModel, graph);
      }
      return conversion;
    };
  }
});

// node_modules/command-line-usage/node_modules/color-convert/index.js
var require_color_convert = __commonJS({
  "node_modules/command-line-usage/node_modules/color-convert/index.js"(exports, module2) {
    var conversions = require_conversions();
    var route = require_route();
    var convert = {};
    var models = Object.keys(conversions);
    function wrapRaw(fn) {
      var wrappedFn = function(args) {
        if (args === void 0 || args === null) {
          return args;
        }
        if (arguments.length > 1) {
          args = Array.prototype.slice.call(arguments);
        }
        return fn(args);
      };
      if ("conversion" in fn) {
        wrappedFn.conversion = fn.conversion;
      }
      return wrappedFn;
    }
    function wrapRounded(fn) {
      var wrappedFn = function(args) {
        if (args === void 0 || args === null) {
          return args;
        }
        if (arguments.length > 1) {
          args = Array.prototype.slice.call(arguments);
        }
        var result = fn(args);
        if (typeof result === "object") {
          for (var len = result.length, i = 0; i < len; i++) {
            result[i] = Math.round(result[i]);
          }
        }
        return result;
      };
      if ("conversion" in fn) {
        wrappedFn.conversion = fn.conversion;
      }
      return wrappedFn;
    }
    models.forEach(function(fromModel) {
      convert[fromModel] = {};
      Object.defineProperty(convert[fromModel], "channels", { value: conversions[fromModel].channels });
      Object.defineProperty(convert[fromModel], "labels", { value: conversions[fromModel].labels });
      var routes = route(fromModel);
      var routeModels = Object.keys(routes);
      routeModels.forEach(function(toModel) {
        var fn = routes[toModel];
        convert[fromModel][toModel] = wrapRounded(fn);
        convert[fromModel][toModel].raw = wrapRaw(fn);
      });
    });
    module2.exports = convert;
  }
});

// node_modules/command-line-usage/node_modules/ansi-styles/index.js
var require_ansi_styles = __commonJS({
  "node_modules/command-line-usage/node_modules/ansi-styles/index.js"(exports, module2) {
    "use strict";
    var colorConvert = require_color_convert();
    var wrapAnsi16 = (fn, offset) => function() {
      const code = fn.apply(colorConvert, arguments);
      return `\x1B[${code + offset}m`;
    };
    var wrapAnsi256 = (fn, offset) => function() {
      const code = fn.apply(colorConvert, arguments);
      return `\x1B[${38 + offset};5;${code}m`;
    };
    var wrapAnsi16m = (fn, offset) => function() {
      const rgb = fn.apply(colorConvert, arguments);
      return `\x1B[${38 + offset};2;${rgb[0]};${rgb[1]};${rgb[2]}m`;
    };
    function assembleStyles() {
      const codes = /* @__PURE__ */ new Map();
      const styles = {
        modifier: {
          reset: [0, 0],
          // 21 isn't widely supported and 22 does the same thing
          bold: [1, 22],
          dim: [2, 22],
          italic: [3, 23],
          underline: [4, 24],
          inverse: [7, 27],
          hidden: [8, 28],
          strikethrough: [9, 29]
        },
        color: {
          black: [30, 39],
          red: [31, 39],
          green: [32, 39],
          yellow: [33, 39],
          blue: [34, 39],
          magenta: [35, 39],
          cyan: [36, 39],
          white: [37, 39],
          gray: [90, 39],
          // Bright color
          redBright: [91, 39],
          greenBright: [92, 39],
          yellowBright: [93, 39],
          blueBright: [94, 39],
          magentaBright: [95, 39],
          cyanBright: [96, 39],
          whiteBright: [97, 39]
        },
        bgColor: {
          bgBlack: [40, 49],
          bgRed: [41, 49],
          bgGreen: [42, 49],
          bgYellow: [43, 49],
          bgBlue: [44, 49],
          bgMagenta: [45, 49],
          bgCyan: [46, 49],
          bgWhite: [47, 49],
          // Bright color
          bgBlackBright: [100, 49],
          bgRedBright: [101, 49],
          bgGreenBright: [102, 49],
          bgYellowBright: [103, 49],
          bgBlueBright: [104, 49],
          bgMagentaBright: [105, 49],
          bgCyanBright: [106, 49],
          bgWhiteBright: [107, 49]
        }
      };
      styles.color.grey = styles.color.gray;
      for (const groupName of Object.keys(styles)) {
        const group = styles[groupName];
        for (const styleName of Object.keys(group)) {
          const style = group[styleName];
          styles[styleName] = {
            open: `\x1B[${style[0]}m`,
            close: `\x1B[${style[1]}m`
          };
          group[styleName] = styles[styleName];
          codes.set(style[0], style[1]);
        }
        Object.defineProperty(styles, groupName, {
          value: group,
          enumerable: false
        });
        Object.defineProperty(styles, "codes", {
          value: codes,
          enumerable: false
        });
      }
      const ansi2ansi = (n) => n;
      const rgb2rgb = (r, g, b) => [r, g, b];
      styles.color.close = "\x1B[39m";
      styles.bgColor.close = "\x1B[49m";
      styles.color.ansi = {
        ansi: wrapAnsi16(ansi2ansi, 0)
      };
      styles.color.ansi256 = {
        ansi256: wrapAnsi256(ansi2ansi, 0)
      };
      styles.color.ansi16m = {
        rgb: wrapAnsi16m(rgb2rgb, 0)
      };
      styles.bgColor.ansi = {
        ansi: wrapAnsi16(ansi2ansi, 10)
      };
      styles.bgColor.ansi256 = {
        ansi256: wrapAnsi256(ansi2ansi, 10)
      };
      styles.bgColor.ansi16m = {
        rgb: wrapAnsi16m(rgb2rgb, 10)
      };
      for (let key of Object.keys(colorConvert)) {
        if (typeof colorConvert[key] !== "object") {
          continue;
        }
        const suite = colorConvert[key];
        if (key === "ansi16") {
          key = "ansi";
        }
        if ("ansi16" in suite) {
          styles.color.ansi[key] = wrapAnsi16(suite.ansi16, 0);
          styles.bgColor.ansi[key] = wrapAnsi16(suite.ansi16, 10);
        }
        if ("ansi256" in suite) {
          styles.color.ansi256[key] = wrapAnsi256(suite.ansi256, 0);
          styles.bgColor.ansi256[key] = wrapAnsi256(suite.ansi256, 10);
        }
        if ("rgb" in suite) {
          styles.color.ansi16m[key] = wrapAnsi16m(suite.rgb, 0);
          styles.bgColor.ansi16m[key] = wrapAnsi16m(suite.rgb, 10);
        }
      }
      return styles;
    }
    Object.defineProperty(module2, "exports", {
      enumerable: true,
      get: assembleStyles
    });
  }
});

// node_modules/command-line-usage/node_modules/has-flag/index.js
var require_has_flag = __commonJS({
  "node_modules/command-line-usage/node_modules/has-flag/index.js"(exports, module2) {
    "use strict";
    module2.exports = (flag, argv) => {
      argv = argv || process.argv;
      const prefix = flag.startsWith("-") ? "" : flag.length === 1 ? "-" : "--";
      const pos = argv.indexOf(prefix + flag);
      const terminatorPos = argv.indexOf("--");
      return pos !== -1 && (terminatorPos === -1 ? true : pos < terminatorPos);
    };
  }
});

// node_modules/command-line-usage/node_modules/supports-color/index.js
var require_supports_color = __commonJS({
  "node_modules/command-line-usage/node_modules/supports-color/index.js"(exports, module2) {
    "use strict";
    var os = require("os");
    var hasFlag = require_has_flag();
    var env = process.env;
    var forceColor;
    if (hasFlag("no-color") || hasFlag("no-colors") || hasFlag("color=false")) {
      forceColor = false;
    } else if (hasFlag("color") || hasFlag("colors") || hasFlag("color=true") || hasFlag("color=always")) {
      forceColor = true;
    }
    if ("FORCE_COLOR" in env) {
      forceColor = env.FORCE_COLOR.length === 0 || parseInt(env.FORCE_COLOR, 10) !== 0;
    }
    function translateLevel(level) {
      if (level === 0) {
        return false;
      }
      return {
        level,
        hasBasic: true,
        has256: level >= 2,
        has16m: level >= 3
      };
    }
    function supportsColor(stream) {
      if (forceColor === false) {
        return 0;
      }
      if (hasFlag("color=16m") || hasFlag("color=full") || hasFlag("color=truecolor")) {
        return 3;
      }
      if (hasFlag("color=256")) {
        return 2;
      }
      if (stream && !stream.isTTY && forceColor !== true) {
        return 0;
      }
      const min = forceColor ? 1 : 0;
      if (process.platform === "win32") {
        const osRelease = os.release().split(".");
        if (Number(process.versions.node.split(".")[0]) >= 8 && Number(osRelease[0]) >= 10 && Number(osRelease[2]) >= 10586) {
          return Number(osRelease[2]) >= 14931 ? 3 : 2;
        }
        return 1;
      }
      if ("CI" in env) {
        if (["TRAVIS", "CIRCLECI", "APPVEYOR", "GITLAB_CI"].some((sign) => sign in env) || env.CI_NAME === "codeship") {
          return 1;
        }
        return min;
      }
      if ("TEAMCITY_VERSION" in env) {
        return /^(9\.(0*[1-9]\d*)\.|\d{2,}\.)/.test(env.TEAMCITY_VERSION) ? 1 : 0;
      }
      if (env.COLORTERM === "truecolor") {
        return 3;
      }
      if ("TERM_PROGRAM" in env) {
        const version = parseInt((env.TERM_PROGRAM_VERSION || "").split(".")[0], 10);
        switch (env.TERM_PROGRAM) {
          case "iTerm.app":
            return version >= 3 ? 3 : 2;
          case "Apple_Terminal":
            return 2;
        }
      }
      if (/-256(color)?$/i.test(env.TERM)) {
        return 2;
      }
      if (/^screen|^xterm|^vt100|^vt220|^rxvt|color|ansi|cygwin|linux/i.test(env.TERM)) {
        return 1;
      }
      if ("COLORTERM" in env) {
        return 1;
      }
      if (env.TERM === "dumb") {
        return min;
      }
      return min;
    }
    function getSupportLevel(stream) {
      const level = supportsColor(stream);
      return translateLevel(level);
    }
    module2.exports = {
      supportsColor: getSupportLevel,
      stdout: getSupportLevel(process.stdout),
      stderr: getSupportLevel(process.stderr)
    };
  }
});

// node_modules/command-line-usage/node_modules/chalk/templates.js
var require_templates = __commonJS({
  "node_modules/command-line-usage/node_modules/chalk/templates.js"(exports, module2) {
    "use strict";
    var TEMPLATE_REGEX = /(?:\\(u[a-f\d]{4}|x[a-f\d]{2}|.))|(?:\{(~)?(\w+(?:\([^)]*\))?(?:\.\w+(?:\([^)]*\))?)*)(?:[ \t]|(?=\r?\n)))|(\})|((?:.|[\r\n\f])+?)/gi;
    var STYLE_REGEX = /(?:^|\.)(\w+)(?:\(([^)]*)\))?/g;
    var STRING_REGEX = /^(['"])((?:\\.|(?!\1)[^\\])*)\1$/;
    var ESCAPE_REGEX = /\\(u[a-f\d]{4}|x[a-f\d]{2}|.)|([^\\])/gi;
    var ESCAPES = /* @__PURE__ */ new Map([
      ["n", "\n"],
      ["r", "\r"],
      ["t", "	"],
      ["b", "\b"],
      ["f", "\f"],
      ["v", "\v"],
      ["0", "\0"],
      ["\\", "\\"],
      ["e", "\x1B"],
      ["a", "\x07"]
    ]);
    function unescape(c) {
      if (c[0] === "u" && c.length === 5 || c[0] === "x" && c.length === 3) {
        return String.fromCharCode(parseInt(c.slice(1), 16));
      }
      return ESCAPES.get(c) || c;
    }
    function parseArguments(name, args) {
      const results = [];
      const chunks = args.trim().split(/\s*,\s*/g);
      let matches;
      for (const chunk of chunks) {
        if (!isNaN(chunk)) {
          results.push(Number(chunk));
        } else if (matches = chunk.match(STRING_REGEX)) {
          results.push(matches[2].replace(ESCAPE_REGEX, (m, escape, chr) => escape ? unescape(escape) : chr));
        } else {
          throw new Error(`Invalid Chalk template style argument: ${chunk} (in style '${name}')`);
        }
      }
      return results;
    }
    function parseStyle(style) {
      STYLE_REGEX.lastIndex = 0;
      const results = [];
      let matches;
      while ((matches = STYLE_REGEX.exec(style)) !== null) {
        const name = matches[1];
        if (matches[2]) {
          const args = parseArguments(name, matches[2]);
          results.push([name].concat(args));
        } else {
          results.push([name]);
        }
      }
      return results;
    }
    function buildStyle(chalk, styles) {
      const enabled = {};
      for (const layer of styles) {
        for (const style of layer.styles) {
          enabled[style[0]] = layer.inverse ? null : style.slice(1);
        }
      }
      let current = chalk;
      for (const styleName of Object.keys(enabled)) {
        if (Array.isArray(enabled[styleName])) {
          if (!(styleName in current)) {
            throw new Error(`Unknown Chalk style: ${styleName}`);
          }
          if (enabled[styleName].length > 0) {
            current = current[styleName].apply(current, enabled[styleName]);
          } else {
            current = current[styleName];
          }
        }
      }
      return current;
    }
    module2.exports = (chalk, tmp) => {
      const styles = [];
      const chunks = [];
      let chunk = [];
      tmp.replace(TEMPLATE_REGEX, (m, escapeChar, inverse, style, close, chr) => {
        if (escapeChar) {
          chunk.push(unescape(escapeChar));
        } else if (style) {
          const str = chunk.join("");
          chunk = [];
          chunks.push(styles.length === 0 ? str : buildStyle(chalk, styles)(str));
          styles.push({ inverse, styles: parseStyle(style) });
        } else if (close) {
          if (styles.length === 0) {
            throw new Error("Found extraneous } in Chalk template literal");
          }
          chunks.push(buildStyle(chalk, styles)(chunk.join("")));
          chunk = [];
          styles.pop();
        } else {
          chunk.push(chr);
        }
      });
      chunks.push(chunk.join(""));
      if (styles.length > 0) {
        const errMsg = `Chalk template literal is missing ${styles.length} closing bracket${styles.length === 1 ? "" : "s"} (\`}\`)`;
        throw new Error(errMsg);
      }
      return chunks.join("");
    };
  }
});

// node_modules/command-line-usage/node_modules/chalk/index.js
var require_chalk = __commonJS({
  "node_modules/command-line-usage/node_modules/chalk/index.js"(exports, module2) {
    "use strict";
    var escapeStringRegexp = require_escape_string_regexp();
    var ansiStyles = require_ansi_styles();
    var stdoutColor = require_supports_color().stdout;
    var template = require_templates();
    var isSimpleWindowsTerm = process.platform === "win32" && !(process.env.TERM || "").toLowerCase().startsWith("xterm");
    var levelMapping = ["ansi", "ansi", "ansi256", "ansi16m"];
    var skipModels = /* @__PURE__ */ new Set(["gray"]);
    var styles = /* @__PURE__ */ Object.create(null);
    function applyOptions(obj, options) {
      options = options || {};
      const scLevel = stdoutColor ? stdoutColor.level : 0;
      obj.level = options.level === void 0 ? scLevel : options.level;
      obj.enabled = "enabled" in options ? options.enabled : obj.level > 0;
    }
    function Chalk(options) {
      if (!this || !(this instanceof Chalk) || this.template) {
        const chalk = {};
        applyOptions(chalk, options);
        chalk.template = function() {
          const args = [].slice.call(arguments);
          return chalkTag.apply(null, [chalk.template].concat(args));
        };
        Object.setPrototypeOf(chalk, Chalk.prototype);
        Object.setPrototypeOf(chalk.template, chalk);
        chalk.template.constructor = Chalk;
        return chalk.template;
      }
      applyOptions(this, options);
    }
    if (isSimpleWindowsTerm) {
      ansiStyles.blue.open = "\x1B[94m";
    }
    for (const key of Object.keys(ansiStyles)) {
      ansiStyles[key].closeRe = new RegExp(escapeStringRegexp(ansiStyles[key].close), "g");
      styles[key] = {
        get() {
          const codes = ansiStyles[key];
          return build.call(this, this._styles ? this._styles.concat(codes) : [codes], this._empty, key);
        }
      };
    }
    styles.visible = {
      get() {
        return build.call(this, this._styles || [], true, "visible");
      }
    };
    ansiStyles.color.closeRe = new RegExp(escapeStringRegexp(ansiStyles.color.close), "g");
    for (const model of Object.keys(ansiStyles.color.ansi)) {
      if (skipModels.has(model)) {
        continue;
      }
      styles[model] = {
        get() {
          const level = this.level;
          return function() {
            const open = ansiStyles.color[levelMapping[level]][model].apply(null, arguments);
            const codes = {
              open,
              close: ansiStyles.color.close,
              closeRe: ansiStyles.color.closeRe
            };
            return build.call(this, this._styles ? this._styles.concat(codes) : [codes], this._empty, model);
          };
        }
      };
    }
    ansiStyles.bgColor.closeRe = new RegExp(escapeStringRegexp(ansiStyles.bgColor.close), "g");
    for (const model of Object.keys(ansiStyles.bgColor.ansi)) {
      if (skipModels.has(model)) {
        continue;
      }
      const bgModel = "bg" + model[0].toUpperCase() + model.slice(1);
      styles[bgModel] = {
        get() {
          const level = this.level;
          return function() {
            const open = ansiStyles.bgColor[levelMapping[level]][model].apply(null, arguments);
            const codes = {
              open,
              close: ansiStyles.bgColor.close,
              closeRe: ansiStyles.bgColor.closeRe
            };
            return build.call(this, this._styles ? this._styles.concat(codes) : [codes], this._empty, model);
          };
        }
      };
    }
    var proto = Object.defineProperties(() => {
    }, styles);
    function build(_styles, _empty, key) {
      const builder = function() {
        return applyStyle.apply(builder, arguments);
      };
      builder._styles = _styles;
      builder._empty = _empty;
      const self2 = this;
      Object.defineProperty(builder, "level", {
        enumerable: true,
        get() {
          return self2.level;
        },
        set(level) {
          self2.level = level;
        }
      });
      Object.defineProperty(builder, "enabled", {
        enumerable: true,
        get() {
          return self2.enabled;
        },
        set(enabled) {
          self2.enabled = enabled;
        }
      });
      builder.hasGrey = this.hasGrey || key === "gray" || key === "grey";
      builder.__proto__ = proto;
      return builder;
    }
    function applyStyle() {
      const args = arguments;
      const argsLen = args.length;
      let str = String(arguments[0]);
      if (argsLen === 0) {
        return "";
      }
      if (argsLen > 1) {
        for (let a = 1; a < argsLen; a++) {
          str += " " + args[a];
        }
      }
      if (!this.enabled || this.level <= 0 || !str) {
        return this._empty ? "" : str;
      }
      const originalDim = ansiStyles.dim.open;
      if (isSimpleWindowsTerm && this.hasGrey) {
        ansiStyles.dim.open = "";
      }
      for (const code of this._styles.slice().reverse()) {
        str = code.open + str.replace(code.closeRe, code.open) + code.close;
        str = str.replace(/\r?\n/g, `${code.close}$&${code.open}`);
      }
      ansiStyles.dim.open = originalDim;
      return str;
    }
    function chalkTag(chalk, strings) {
      if (!Array.isArray(strings)) {
        return [].slice.call(arguments, 1).join(" ");
      }
      const args = [].slice.call(arguments, 2);
      const parts = [strings.raw[0]];
      for (let i = 1; i < strings.length; i++) {
        parts.push(String(args[i - 1]).replace(/[{}\\]/g, "\\$&"));
        parts.push(String(strings.raw[i]));
      }
      return template(chalk, parts.join(""));
    }
    Object.defineProperties(Chalk.prototype, styles);
    module2.exports = Chalk();
    module2.exports.supportsColor = stdoutColor;
    module2.exports.default = module2.exports;
  }
});

// node_modules/command-line-usage/lib/section.js
var require_section = __commonJS({
  "node_modules/command-line-usage/lib/section.js"(exports, module2) {
    var Section = class {
      constructor() {
        this.lines = [];
      }
      add(lines) {
        if (lines) {
          const arrayify = require_dist2();
          arrayify(lines).forEach((line) => this.lines.push(line));
        } else {
          this.lines.push("");
        }
      }
      toString() {
        const os = require("os");
        return this.lines.join(os.EOL);
      }
      header(text) {
        const chalk = require_chalk();
        if (text) {
          this.add(chalk.underline.bold(text));
          this.add();
        }
      }
    };
    module2.exports = Section;
  }
});

// node_modules/deep-extend/lib/deep-extend.js
var require_deep_extend = __commonJS({
  "node_modules/deep-extend/lib/deep-extend.js"(exports, module2) {
    "use strict";
    function isSpecificValue(val) {
      return val instanceof Buffer || val instanceof Date || val instanceof RegExp ? true : false;
    }
    function cloneSpecificValue(val) {
      if (val instanceof Buffer) {
        var x = Buffer.alloc ? Buffer.alloc(val.length) : new Buffer(val.length);
        val.copy(x);
        return x;
      } else if (val instanceof Date) {
        return new Date(val.getTime());
      } else if (val instanceof RegExp) {
        return new RegExp(val);
      } else {
        throw new Error("Unexpected situation");
      }
    }
    function deepCloneArray(arr) {
      var clone = [];
      arr.forEach(function(item, index) {
        if (typeof item === "object" && item !== null) {
          if (Array.isArray(item)) {
            clone[index] = deepCloneArray(item);
          } else if (isSpecificValue(item)) {
            clone[index] = cloneSpecificValue(item);
          } else {
            clone[index] = deepExtend({}, item);
          }
        } else {
          clone[index] = item;
        }
      });
      return clone;
    }
    function safeGetProperty(object, property) {
      return property === "__proto__" ? void 0 : object[property];
    }
    var deepExtend = module2.exports = function() {
      if (arguments.length < 1 || typeof arguments[0] !== "object") {
        return false;
      }
      if (arguments.length < 2) {
        return arguments[0];
      }
      var target = arguments[0];
      var args = Array.prototype.slice.call(arguments, 1);
      var val, src, clone;
      args.forEach(function(obj) {
        if (typeof obj !== "object" || obj === null || Array.isArray(obj)) {
          return;
        }
        Object.keys(obj).forEach(function(key) {
          src = safeGetProperty(target, key);
          val = safeGetProperty(obj, key);
          if (val === target) {
            return;
          } else if (typeof val !== "object" || val === null) {
            target[key] = val;
            return;
          } else if (Array.isArray(val)) {
            target[key] = deepCloneArray(val);
            return;
          } else if (isSpecificValue(val)) {
            target[key] = cloneSpecificValue(val);
            return;
          } else if (typeof src !== "object" || src === null || Array.isArray(src)) {
            target[key] = deepExtend({}, val);
            return;
          } else {
            target[key] = deepExtend(src, val);
            return;
          }
        });
      });
      return target;
    };
  }
});

// node_modules/table-layout/node_modules/array-back/dist/index.js
var require_dist3 = __commonJS({
  "node_modules/table-layout/node_modules/array-back/dist/index.js"(exports, module2) {
    (function(global2, factory) {
      typeof exports === "object" && typeof module2 !== "undefined" ? module2.exports = factory() : typeof define === "function" && define.amd ? define(factory) : (global2 = global2 || self, global2.arrayBack = factory());
    })(exports, function() {
      "use strict";
      function isObject(input) {
        return typeof input === "object" && input !== null;
      }
      function isArrayLike(input) {
        return isObject(input) && typeof input.length === "number";
      }
      function arrayify(input) {
        if (Array.isArray(input)) {
          return input;
        }
        if (input === void 0) {
          return [];
        }
        if (isArrayLike(input) || input instanceof Set) {
          return Array.from(input);
        }
        return [input];
      }
      return arrayify;
    });
  }
});

// node_modules/table-layout/node_modules/typical/dist/index.js
var require_dist4 = __commonJS({
  "node_modules/table-layout/node_modules/typical/dist/index.js"(exports, module2) {
    (function(global2, factory) {
      typeof exports === "object" && typeof module2 !== "undefined" ? factory(exports) : typeof define === "function" && define.amd ? define(["exports"], factory) : (global2 = global2 || self, factory(global2.typical = {}));
    })(exports, function(exports2) {
      "use strict";
      function isNumber(n) {
        return !isNaN(parseFloat(n)) && isFinite(n);
      }
      function isPlainObject(input) {
        return input !== null && typeof input === "object" && input.constructor === Object;
      }
      function isArrayLike(input) {
        return isObject(input) && typeof input.length === "number";
      }
      function isObject(input) {
        return typeof input === "object" && input !== null;
      }
      function isDefined(input) {
        return typeof input !== "undefined";
      }
      function isUndefined(input) {
        return !isDefined(input);
      }
      function isNull(input) {
        return input === null;
      }
      function isDefinedValue(input) {
        return isDefined(input) && !isNull(input) && !Number.isNaN(input);
      }
      function isClass(input) {
        if (typeof input === "function") {
          return /^class /.test(Function.prototype.toString.call(input));
        } else {
          return false;
        }
      }
      function isPrimitive(input) {
        if (input === null)
          return true;
        switch (typeof input) {
          case "string":
          case "number":
          case "symbol":
          case "undefined":
          case "boolean":
            return true;
          default:
            return false;
        }
      }
      function isPromise(input) {
        if (input) {
          const isPromise2 = isDefined(Promise) && input instanceof Promise;
          const isThenable = input.then && typeof input.then === "function";
          return !!(isPromise2 || isThenable);
        } else {
          return false;
        }
      }
      function isIterable(input) {
        if (input === null || !isDefined(input)) {
          return false;
        } else {
          return typeof input[Symbol.iterator] === "function" || typeof input[Symbol.asyncIterator] === "function";
        }
      }
      function isString(input) {
        return typeof input === "string";
      }
      function isFunction(input) {
        return typeof input === "function";
      }
      var index = {
        isNumber,
        isPlainObject,
        isArrayLike,
        isObject,
        isDefined,
        isUndefined,
        isNull,
        isDefinedValue,
        isClass,
        isPrimitive,
        isPromise,
        isIterable,
        isString,
        isFunction
      };
      exports2.default = index;
      exports2.isArrayLike = isArrayLike;
      exports2.isClass = isClass;
      exports2.isDefined = isDefined;
      exports2.isDefinedValue = isDefinedValue;
      exports2.isFunction = isFunction;
      exports2.isIterable = isIterable;
      exports2.isNull = isNull;
      exports2.isNumber = isNumber;
      exports2.isObject = isObject;
      exports2.isPlainObject = isPlainObject;
      exports2.isPrimitive = isPrimitive;
      exports2.isPromise = isPromise;
      exports2.isString = isString;
      exports2.isUndefined = isUndefined;
      Object.defineProperty(exports2, "__esModule", { value: true });
    });
  }
});

// node_modules/table-layout/lib/cell.js
var require_cell = __commonJS({
  "node_modules/table-layout/lib/cell.js"(exports, module2) {
    var t = require_dist4();
    var _value = /* @__PURE__ */ new WeakMap();
    var _column = /* @__PURE__ */ new WeakMap();
    var Cell = class {
      constructor(value, column) {
        this.value = value;
        _column.set(this, column);
      }
      set value(val) {
        _value.set(this, val);
      }
      get value() {
        let cellValue = _value.get(this);
        if (typeof cellValue === "function")
          cellValue = cellValue.call(_column.get(this));
        if (cellValue === void 0) {
          cellValue = "";
        } else {
          cellValue = String(cellValue);
        }
        return cellValue;
      }
    };
    module2.exports = Cell;
  }
});

// node_modules/table-layout/lib/rows.js
var require_rows = __commonJS({
  "node_modules/table-layout/lib/rows.js"(exports, module2) {
    var arrayify = require_dist3();
    var Cell = require_cell();
    var t = require_dist4();
    var Rows = class {
      constructor(rows, columns) {
        this.list = [];
        this.load(rows, columns);
      }
      load(rows, columns) {
        arrayify(rows).forEach((row) => {
          this.list.push(new Map(objectToIterable(row, columns)));
        });
      }
      static removeEmptyColumns(data) {
        const distinctColumnNames = data.reduce((columnNames, row) => {
          Object.keys(row).forEach((key) => {
            if (columnNames.indexOf(key) === -1)
              columnNames.push(key);
          });
          return columnNames;
        }, []);
        const emptyColumns = distinctColumnNames.filter((columnName) => {
          const hasValue = data.some((row) => {
            const value = row[columnName];
            return t.isDefined(value) && typeof value !== "string" || typeof value === "string" && /\S+/.test(value);
          });
          return !hasValue;
        });
        return data.map((row) => {
          emptyColumns.forEach((emptyCol) => delete row[emptyCol]);
          return row;
        });
      }
    };
    function objectToIterable(row, columns) {
      return columns.list.map((column) => {
        return [column, new Cell(row[column.name], column)];
      });
    }
    module2.exports = Rows;
  }
});

// node_modules/table-layout/lib/padding.js
var require_padding = __commonJS({
  "node_modules/table-layout/lib/padding.js"(exports, module2) {
    var Padding = class {
      constructor(padding) {
        this.left = padding.left;
        this.right = padding.right;
      }
      length() {
        return this.left.length + this.right.length;
      }
    };
    module2.exports = Padding;
  }
});

// node_modules/table-layout/lib/column.js
var require_column = __commonJS({
  "node_modules/table-layout/lib/column.js"(exports, module2) {
    var t = require_dist4();
    var Padding = require_padding();
    var _padding = /* @__PURE__ */ new WeakMap();
    var Column = class {
      constructor(column) {
        if (t.isDefined(column.name))
          this.name = column.name;
        if (t.isDefined(column.width))
          this.width = column.width;
        if (t.isDefined(column.maxWidth))
          this.maxWidth = column.maxWidth;
        if (t.isDefined(column.minWidth))
          this.minWidth = column.minWidth;
        if (t.isDefined(column.noWrap))
          this.noWrap = column.noWrap;
        if (t.isDefined(column.break))
          this.break = column.break;
        if (t.isDefined(column.contentWrappable))
          this.contentWrappable = column.contentWrappable;
        if (t.isDefined(column.contentWidth))
          this.contentWidth = column.contentWidth;
        if (t.isDefined(column.minContentWidth))
          this.minContentWidth = column.minContentWidth;
        this.padding = column.padding || { left: " ", right: " " };
        this.generatedWidth = null;
      }
      set padding(padding) {
        _padding.set(this, new Padding(padding));
      }
      get padding() {
        return _padding.get(this);
      }
      /**
       * the width of the content (excluding padding) after being wrapped
       */
      get wrappedContentWidth() {
        return Math.max(this.generatedWidth - this.padding.length(), 0);
      }
      isResizable() {
        return !this.isFixed();
      }
      isFixed() {
        return t.isDefined(this.width) || this.noWrap || !this.contentWrappable;
      }
      generateWidth() {
        this.generatedWidth = this.width || this.contentWidth + this.padding.length();
      }
      generateMinWidth() {
        this.minWidth = this.minContentWidth + this.padding.length();
      }
    };
    module2.exports = Column;
  }
});

// node_modules/wordwrapjs/node_modules/typical/dist/index.js
var require_dist5 = __commonJS({
  "node_modules/wordwrapjs/node_modules/typical/dist/index.js"(exports, module2) {
    (function(global2, factory) {
      typeof exports === "object" && typeof module2 !== "undefined" ? factory(exports) : typeof define === "function" && define.amd ? define(["exports"], factory) : (global2 = global2 || self, factory(global2.typical = {}));
    })(exports, function(exports2) {
      "use strict";
      function isNumber(n) {
        return !isNaN(parseFloat(n)) && isFinite(n);
      }
      function isPlainObject(input) {
        return input !== null && typeof input === "object" && input.constructor === Object;
      }
      function isArrayLike(input) {
        return isObject(input) && typeof input.length === "number";
      }
      function isObject(input) {
        return typeof input === "object" && input !== null;
      }
      function isDefined(input) {
        return typeof input !== "undefined";
      }
      function isUndefined(input) {
        return !isDefined(input);
      }
      function isNull(input) {
        return input === null;
      }
      function isDefinedValue(input) {
        return isDefined(input) && !isNull(input) && !Number.isNaN(input);
      }
      function isClass(input) {
        if (typeof input === "function") {
          return /^class /.test(Function.prototype.toString.call(input));
        } else {
          return false;
        }
      }
      function isPrimitive(input) {
        if (input === null)
          return true;
        switch (typeof input) {
          case "string":
          case "number":
          case "symbol":
          case "undefined":
          case "boolean":
            return true;
          default:
            return false;
        }
      }
      function isPromise(input) {
        if (input) {
          const isPromise2 = isDefined(Promise) && input instanceof Promise;
          const isThenable = input.then && typeof input.then === "function";
          return !!(isPromise2 || isThenable);
        } else {
          return false;
        }
      }
      function isIterable(input) {
        if (input === null || !isDefined(input)) {
          return false;
        } else {
          return typeof input[Symbol.iterator] === "function" || typeof input[Symbol.asyncIterator] === "function";
        }
      }
      function isString(input) {
        return typeof input === "string";
      }
      function isFunction(input) {
        return typeof input === "function";
      }
      var index = {
        isNumber,
        isPlainObject,
        isArrayLike,
        isObject,
        isDefined,
        isUndefined,
        isNull,
        isDefinedValue,
        isClass,
        isPrimitive,
        isPromise,
        isIterable,
        isString,
        isFunction
      };
      exports2.default = index;
      exports2.isArrayLike = isArrayLike;
      exports2.isClass = isClass;
      exports2.isDefined = isDefined;
      exports2.isDefinedValue = isDefinedValue;
      exports2.isFunction = isFunction;
      exports2.isIterable = isIterable;
      exports2.isNull = isNull;
      exports2.isNumber = isNumber;
      exports2.isObject = isObject;
      exports2.isPlainObject = isPlainObject;
      exports2.isPrimitive = isPrimitive;
      exports2.isPromise = isPromise;
      exports2.isString = isString;
      exports2.isUndefined = isUndefined;
      Object.defineProperty(exports2, "__esModule", { value: true });
    });
  }
});

// node_modules/reduce-flatten/index.js
var require_reduce_flatten = __commonJS({
  "node_modules/reduce-flatten/index.js"(exports, module2) {
    module2.exports = flatten;
    function flatten(prev, curr) {
      return prev.concat(curr);
    }
  }
});

// node_modules/wordwrapjs/index.js
var require_wordwrapjs = __commonJS({
  "node_modules/wordwrapjs/index.js"(exports, module2) {
    var os = require("os");
    var t = require_dist5();
    var re = {
      chunk: /[^\s-]+?-\b|\S+|\s+|\r\n?|\n/g,
      ansiEscapeSequence: /\u001b.*?m/g
    };
    var WordWrap = class {
      constructor(text, options) {
        options = options || {};
        if (!t.isDefined(text))
          text = "";
        this._lines = String(text).split(/\r\n|\n/g);
        this.options = options;
        this.options.width = options.width === void 0 ? 30 : options.width;
      }
      lines() {
        const flatten = require_reduce_flatten();
        return this._lines.map(trimLine.bind(this)).map((line) => line.match(re.chunk) || ["~~empty~~"]).map((lineWords) => {
          if (this.options.break) {
            return lineWords.map(breakWord.bind(this));
          } else {
            return lineWords;
          }
        }).map((lineWords) => lineWords.reduce(flatten, [])).map((lineWords) => {
          return lineWords.reduce((lines, word) => {
            let currentLine = lines[lines.length - 1];
            if (replaceAnsi(word).length + replaceAnsi(currentLine).length > this.options.width) {
              lines.push(word);
            } else {
              lines[lines.length - 1] += word;
            }
            return lines;
          }, [""]);
        }).reduce(flatten, []).map(trimLine.bind(this)).filter((line) => line.trim()).map((line) => line.replace("~~empty~~", ""));
      }
      wrap() {
        return this.lines().join(os.EOL);
      }
      toString() {
        return this.wrap();
      }
      /**
       * @param {string} - the input text to wrap
       * @param [options] {object} - optional configuration
       * @param [options.width] {number} - the max column width in characters (defaults to 30).
       * @param [options.break] {boolean} - if true, words exceeding the specified `width` will be forcefully broken
       * @param [options.noTrim] {boolean} - By default, each line output is trimmed. If `noTrim` is set, no line-trimming occurs - all whitespace from the input text is left in.
       * @return {string}
       */
      static wrap(text, options) {
        const block = new this(text, options);
        return block.wrap();
      }
      /**
       * Wraps the input text, returning an array of strings (lines).
       * @param {string} - input text
       * @param {object} - Accepts same options as constructor.
       */
      static lines(text, options) {
        const block = new this(text, options);
        return block.lines();
      }
      /**
       * Returns true if the input text would be wrapped if passed into `.wrap()`.
       * @param {string} - input text
       * @return {boolean}
       */
      static isWrappable(text) {
        if (t.isDefined(text)) {
          text = String(text);
          var matches = text.match(re.chunk);
          return matches ? matches.length > 1 : false;
        }
      }
      /**
       * Splits the input text into an array of words and whitespace.
       * @param {string} - input text
       * @returns {string[]}
       */
      static getChunks(text) {
        return text.match(re.chunk) || [];
      }
    };
    function trimLine(line) {
      return this.options.noTrim ? line : line.trim();
    }
    function replaceAnsi(string) {
      return string.replace(re.ansiEscapeSequence, "");
    }
    function breakWord(word) {
      if (replaceAnsi(word).length > this.options.width) {
        const letters = word.split("");
        let piece;
        const pieces = [];
        while ((piece = letters.splice(0, this.options.width)).length) {
          pieces.push(piece.join(""));
        }
        return pieces;
      } else {
        return word;
      }
    }
    module2.exports = WordWrap;
  }
});

// node_modules/table-layout/lib/ansi.js
var require_ansi = __commonJS({
  "node_modules/table-layout/lib/ansi.js"(exports) {
    var ansiEscapeSequence = /\u001b.*?m/g;
    exports.remove = remove;
    exports.has = has;
    function remove(input) {
      return input.replace(ansiEscapeSequence, "");
    }
    function has(input) {
      return ansiEscapeSequence.test(input);
    }
  }
});

// node_modules/table-layout/lib/columns.js
var require_columns = __commonJS({
  "node_modules/table-layout/lib/columns.js"(exports, module2) {
    var t = require_dist4();
    var arrayify = require_dist3();
    var Column = require_column();
    var wrap = require_wordwrapjs();
    var Cell = require_cell();
    var ansi = require_ansi();
    var _maxWidth = /* @__PURE__ */ new WeakMap();
    var Columns = class {
      constructor(columns) {
        this.list = [];
        arrayify(columns).forEach(this.add.bind(this));
      }
      /**
       * sum of all generatedWidth fields
       * @return {number}
       */
      totalWidth() {
        return this.list.length ? this.list.map((col) => col.generatedWidth).reduce((a, b) => a + b) : 0;
      }
      totalFixedWidth() {
        return this.getFixed().map((col) => col.generatedWidth).reduce((a, b) => a + b, 0);
      }
      get(columnName) {
        return this.list.find((column) => column.name === columnName);
      }
      getResizable() {
        return this.list.filter((column) => column.isResizable());
      }
      getFixed() {
        return this.list.filter((column) => column.isFixed());
      }
      add(column) {
        const col = column instanceof Column ? column : new Column(column);
        this.list.push(col);
        return col;
      }
      set maxWidth(val) {
        _maxWidth.set(this, val);
      }
      /**
       * sets `generatedWidth` for each column
       * @chainable
       */
      autoSize() {
        const maxWidth = _maxWidth.get(this);
        this.list.forEach((column) => {
          column.generateWidth();
          column.generateMinWidth();
        });
        this.list.forEach((column) => {
          if (t.isDefined(column.maxWidth) && column.generatedWidth > column.maxWidth) {
            column.generatedWidth = column.maxWidth;
          }
          if (t.isDefined(column.minWidth) && column.generatedWidth < column.minWidth) {
            column.generatedWidth = column.minWidth;
          }
        });
        const width = {
          total: this.totalWidth(),
          view: maxWidth,
          diff: this.totalWidth() - maxWidth,
          totalFixed: this.totalFixedWidth(),
          totalResizable: Math.max(maxWidth - this.totalFixedWidth(), 0)
        };
        if (width.diff > 0) {
          let resizableColumns = this.getResizable();
          resizableColumns.forEach((column) => {
            column.generatedWidth = Math.floor(width.totalResizable / resizableColumns.length);
          });
          const grownColumns = this.list.filter((column) => column.generatedWidth > column.contentWidth);
          const shrunkenColumns = this.list.filter((column) => column.generatedWidth < column.contentWidth);
          let salvagedSpace = 0;
          grownColumns.forEach((column) => {
            const currentGeneratedWidth = column.generatedWidth;
            column.generateWidth();
            salvagedSpace += currentGeneratedWidth - column.generatedWidth;
          });
          shrunkenColumns.forEach((column) => {
            column.generatedWidth += Math.floor(salvagedSpace / shrunkenColumns.length);
          });
        }
        return this;
      }
      /**
       * Factory method returning all distinct columns from input
       * @param  {object[]} - input recordset
       * @return {module:columns}
       */
      static getColumns(rows) {
        var columns = new Columns();
        arrayify(rows).forEach((row) => {
          for (let columnName in row) {
            let column = columns.get(columnName);
            if (!column) {
              column = columns.add({ name: columnName, contentWidth: 0, minContentWidth: 0 });
            }
            let cell = new Cell(row[columnName], column);
            let cellValue = cell.value;
            if (ansi.has(cellValue)) {
              cellValue = ansi.remove(cellValue);
            }
            if (cellValue.length > column.contentWidth)
              column.contentWidth = cellValue.length;
            let longestWord = getLongestWord(cellValue);
            if (longestWord > column.minContentWidth) {
              column.minContentWidth = longestWord;
            }
            if (!column.contentWrappable)
              column.contentWrappable = wrap.isWrappable(cellValue);
          }
        });
        return columns;
      }
    };
    function getLongestWord(line) {
      const words = wrap.getChunks(line);
      return words.reduce((max, word) => {
        return Math.max(word.length, max);
      }, 0);
    }
    module2.exports = Columns;
  }
});

// node_modules/table-layout/index.js
var require_table_layout = __commonJS({
  "node_modules/table-layout/index.js"(exports, module2) {
    var os = require("os");
    var Table = class {
      /**
       * @param {object[]} - input data
       * @param [options] {object} - optional settings
       * @param [options.maxWidth] {number} - maximum width of layout
       * @param [options.noWrap] {boolean} - disable wrapping on all columns
       * @param [options.noTrim] {boolean} - disable line-trimming
       * @param [options.break] {boolean} - enable word-breaking on all columns
       * @param [options.columns] {module:table-layout~columnOption} - array of column-specific options
       * @param [options.ignoreEmptyColumns] {boolean} - if set, empty columns or columns containing only whitespace are not rendered.
       * @param [options.padding] {object} - Padding values to set on each column. Per-column overrides can be set in the `options.columns` array.
       * @param [options.padding.left] {string} - Defaults to a single space.
       * @param [options.padding.right] {string} - Defaults to a single space.
       * @alias module:table-layout
       */
      constructor(data, options) {
        let ttyWidth = process && (process.stdout.columns || process.stderr.columns) || 0;
        if (ttyWidth && os.platform() === "win32")
          ttyWidth--;
        let defaults = {
          padding: {
            left: " ",
            right: " "
          },
          maxWidth: ttyWidth || 80,
          columns: []
        };
        const extend = require_deep_extend();
        this.options = extend(defaults, options);
        this.load(data);
      }
      load(data) {
        const Rows = require_rows();
        const Columns = require_columns();
        let options = this.options;
        if (options.ignoreEmptyColumns) {
          data = Rows.removeEmptyColumns(data);
        }
        this.columns = Columns.getColumns(data);
        this.rows = new Rows(data, this.columns);
        this.columns.maxWidth = options.maxWidth;
        this.columns.list.forEach((column) => {
          if (options.padding)
            column.padding = options.padding;
          if (options.noWrap)
            column.noWrap = options.noWrap;
          if (options.break) {
            column.break = options.break;
            column.contentWrappable = true;
          }
        });
        options.columns.forEach((optionColumn) => {
          let column = this.columns.get(optionColumn.name);
          if (column) {
            if (optionColumn.padding) {
              column.padding.left = optionColumn.padding.left;
              column.padding.right = optionColumn.padding.right;
            }
            if (optionColumn.width)
              column.width = optionColumn.width;
            if (optionColumn.maxWidth)
              column.maxWidth = optionColumn.maxWidth;
            if (optionColumn.minWidth)
              column.minWidth = optionColumn.minWidth;
            if (optionColumn.noWrap)
              column.noWrap = optionColumn.noWrap;
            if (optionColumn.break) {
              column.break = optionColumn.break;
              column.contentWrappable = true;
            }
          }
        });
        this.columns.autoSize();
        return this;
      }
      getWrapped() {
        const wrap = require_wordwrapjs();
        this.columns.autoSize();
        return this.rows.list.map((row) => {
          let line = [];
          row.forEach((cell, column) => {
            if (column.noWrap) {
              line.push(cell.value.split(/\r\n?|\n/));
            } else {
              line.push(wrap.lines(cell.value, {
                width: column.wrappedContentWidth,
                break: column.break,
                noTrim: this.options.noTrim
              }));
            }
          });
          return line;
        });
      }
      getLines() {
        var wrappedLines = this.getWrapped();
        var lines = [];
        wrappedLines.forEach((wrapped) => {
          let mostLines = getLongestArray(wrapped);
          for (let i = 0; i < mostLines; i++) {
            let line = [];
            wrapped.forEach((cell) => {
              line.push(cell[i] || "");
            });
            lines.push(line);
          }
        });
        return lines;
      }
      /**
       * Identical to `.toString()` with the exception that the result will be an array of lines, rather than a single, multi-line string.
       * @returns {string[]}
       */
      renderLines() {
        var lines = this.getLines();
        return lines.map((line) => {
          return line.reduce((prev, cell, index) => {
            let column = this.columns.list[index];
            return prev + padCell(cell, column.padding, column.generatedWidth);
          }, "");
        });
      }
      /**
       * Returns the input data as a text table.
       * @returns {string}
       */
      toString() {
        return this.renderLines().join(os.EOL) + os.EOL;
      }
    };
    function getLongestArray(arrays) {
      var lengths = arrays.map((array) => array.length);
      return Math.max.apply(null, lengths);
    }
    function padCell(cellValue, padding, width) {
      const ansi = require_ansi();
      var ansiLength = cellValue.length - ansi.remove(cellValue).length;
      cellValue = cellValue || "";
      return (padding.left || "") + cellValue.padEnd(width - padding.length() + ansiLength) + (padding.right || "");
    }
    module2.exports = Table;
  }
});

// node_modules/command-line-usage/lib/chalk-format.js
var require_chalk_format = __commonJS({
  "node_modules/command-line-usage/lib/chalk-format.js"(exports, module2) {
    function chalkFormat(str) {
      if (str) {
        str = str.replace(/`/g, "\\`");
        const chalk = require_chalk();
        return chalk(Object.assign([], { raw: [str] }));
      } else {
        return "";
      }
    }
    module2.exports = chalkFormat;
  }
});

// node_modules/command-line-usage/node_modules/typical/dist/index.js
var require_dist6 = __commonJS({
  "node_modules/command-line-usage/node_modules/typical/dist/index.js"(exports, module2) {
    (function(global2, factory) {
      typeof exports === "object" && typeof module2 !== "undefined" ? factory(exports) : typeof define === "function" && define.amd ? define(["exports"], factory) : (global2 = global2 || self, factory(global2.typical = {}));
    })(exports, function(exports2) {
      "use strict";
      function isNumber(n) {
        return !isNaN(parseFloat(n)) && isFinite(n);
      }
      function isPlainObject(input) {
        return input !== null && typeof input === "object" && input.constructor === Object;
      }
      function isArrayLike(input) {
        return isObject(input) && typeof input.length === "number";
      }
      function isObject(input) {
        return typeof input === "object" && input !== null;
      }
      function isDefined(input) {
        return typeof input !== "undefined";
      }
      function isUndefined(input) {
        return !isDefined(input);
      }
      function isNull(input) {
        return input === null;
      }
      function isDefinedValue(input) {
        return isDefined(input) && !isNull(input) && !Number.isNaN(input);
      }
      function isClass(input) {
        if (typeof input === "function") {
          return /^class /.test(Function.prototype.toString.call(input));
        } else {
          return false;
        }
      }
      function isPrimitive(input) {
        if (input === null)
          return true;
        switch (typeof input) {
          case "string":
          case "number":
          case "symbol":
          case "undefined":
          case "boolean":
            return true;
          default:
            return false;
        }
      }
      function isPromise(input) {
        if (input) {
          const isPromise2 = isDefined(Promise) && input instanceof Promise;
          const isThenable = input.then && typeof input.then === "function";
          return !!(isPromise2 || isThenable);
        } else {
          return false;
        }
      }
      function isIterable(input) {
        if (input === null || !isDefined(input)) {
          return false;
        } else {
          return typeof input[Symbol.iterator] === "function" || typeof input[Symbol.asyncIterator] === "function";
        }
      }
      function isString(input) {
        return typeof input === "string";
      }
      function isFunction(input) {
        return typeof input === "function";
      }
      var index = {
        isNumber,
        isPlainObject,
        isArrayLike,
        isObject,
        isDefined,
        isUndefined,
        isNull,
        isDefinedValue,
        isClass,
        isPrimitive,
        isPromise,
        isIterable,
        isString,
        isFunction
      };
      exports2.default = index;
      exports2.isArrayLike = isArrayLike;
      exports2.isClass = isClass;
      exports2.isDefined = isDefined;
      exports2.isDefinedValue = isDefinedValue;
      exports2.isFunction = isFunction;
      exports2.isIterable = isIterable;
      exports2.isNull = isNull;
      exports2.isNumber = isNumber;
      exports2.isObject = isObject;
      exports2.isPlainObject = isPlainObject;
      exports2.isPrimitive = isPrimitive;
      exports2.isPromise = isPromise;
      exports2.isString = isString;
      exports2.isUndefined = isUndefined;
      Object.defineProperty(exports2, "__esModule", { value: true });
    });
  }
});

// node_modules/command-line-usage/lib/section/option-list.js
var require_option_list = __commonJS({
  "node_modules/command-line-usage/lib/section/option-list.js"(exports, module2) {
    var Section = require_section();
    var Table = require_table_layout();
    var chalk = require_chalk_format();
    var t = require_dist6();
    var arrayify = require_dist2();
    var OptionList = class extends Section {
      constructor(data) {
        super();
        let definitions = arrayify(data.optionList);
        const hide = arrayify(data.hide);
        const groups = arrayify(data.group);
        if (hide.length) {
          definitions = definitions.filter((definition) => {
            return hide.indexOf(definition.name) === -1;
          });
        }
        if (data.header)
          this.header(data.header);
        if (groups.length) {
          definitions = definitions.filter((def) => {
            const noGroupMatch = groups.indexOf("_none") > -1 && !t.isDefined(def.group);
            const groupMatch = intersect(arrayify(def.group), groups);
            if (noGroupMatch || groupMatch)
              return def;
          });
        }
        const rows = definitions.map((def) => {
          return {
            option: getOptionNames(def, data.reverseNameOrder),
            description: chalk(def.description)
          };
        });
        const tableOptions = data.tableOptions || {
          padding: { left: "  ", right: " " },
          columns: [
            { name: "option", noWrap: true },
            { name: "description", maxWidth: 80 }
          ]
        };
        const table = new Table(rows, tableOptions);
        this.add(table.renderLines());
        this.add();
      }
    };
    function getOptionNames(definition, reverseNameOrder) {
      let type = definition.type ? definition.type.name.toLowerCase() : "string";
      const multiple = definition.multiple || definition.lazyMultiple ? "[]" : "";
      if (type) {
        type = type === "boolean" ? "" : `{underline ${type}${multiple}}`;
      }
      type = chalk(definition.typeLabel || type);
      let result = "";
      if (definition.alias) {
        if (definition.name) {
          if (reverseNameOrder) {
            result = chalk(`{bold --${definition.name}}, {bold -${definition.alias}} ${type}`);
          } else {
            result = chalk(`{bold -${definition.alias}}, {bold --${definition.name}} ${type}`);
          }
        } else {
          if (reverseNameOrder) {
            result = chalk(`{bold -${definition.alias}} ${type}`);
          } else {
            result = chalk(`{bold -${definition.alias}} ${type}`);
          }
        }
      } else {
        result = chalk(`{bold --${definition.name}} ${type}`);
      }
      return result;
    }
    function intersect(arr1, arr2) {
      return arr1.some(function(item1) {
        return arr2.some(function(item2) {
          return item1 === item2;
        });
      });
    }
    module2.exports = OptionList;
  }
});

// node_modules/command-line-usage/lib/section/content.js
var require_content = __commonJS({
  "node_modules/command-line-usage/lib/section/content.js"(exports, module2) {
    var Section = require_section();
    var t = require_dist6();
    var Table = require_table_layout();
    var chalkFormat = require_chalk_format();
    var ContentSection = class extends Section {
      constructor(section) {
        super();
        this.header(section.header);
        if (section.content) {
          if (section.raw) {
            const arrayify = require_dist2();
            const content = arrayify(section.content).map((line) => chalkFormat(line));
            this.add(content);
          } else {
            this.add(getContentLines(section.content));
          }
          this.add();
        }
      }
    };
    function getContentLines(content) {
      const defaultPadding = { left: "  ", right: " " };
      if (content) {
        if (t.isString(content)) {
          const table = new Table({ column: chalkFormat(content) }, {
            padding: defaultPadding,
            maxWidth: 80
          });
          return table.renderLines();
        } else if (Array.isArray(content) && content.every(t.isString)) {
          const rows = content.map((string) => ({ column: chalkFormat(string) }));
          const table = new Table(rows, {
            padding: defaultPadding,
            maxWidth: 80
          });
          return table.renderLines();
        } else if (Array.isArray(content) && content.every(t.isPlainObject)) {
          const table = new Table(content.map((row) => ansiFormatRow(row)), {
            padding: defaultPadding
          });
          return table.renderLines();
        } else if (t.isPlainObject(content)) {
          if (!content.options || !content.data) {
            throw new Error('must have an "options" or "data" property\n' + JSON.stringify(content));
          }
          const options = Object.assign(
            { padding: defaultPadding },
            content.options
          );
          if (options.columns) {
            options.columns = options.columns.map((column) => {
              if (column.nowrap) {
                column.noWrap = column.nowrap;
                delete column.nowrap;
              }
              return column;
            });
          }
          const table = new Table(
            content.data.map((row) => ansiFormatRow(row)),
            options
          );
          return table.renderLines();
        } else {
          const message = `invalid input - 'content' must be a string, array of strings, or array of plain objects:

${JSON.stringify(content)}`;
          throw new Error(message);
        }
      }
    }
    function ansiFormatRow(row) {
      for (const key in row) {
        row[key] = chalkFormat(row[key]);
      }
      return row;
    }
    module2.exports = ContentSection;
  }
});

// node_modules/command-line-usage/index.js
var require_command_line_usage = __commonJS({
  "node_modules/command-line-usage/index.js"(exports, module2) {
    function commandLineUsage(sections) {
      const arrayify = require_dist2();
      sections = arrayify(sections);
      if (sections.length) {
        const OptionList = require_option_list();
        const ContentSection = require_content();
        const output = sections.map((section) => {
          if (section.optionList) {
            return new OptionList(section);
          } else {
            return new ContentSection(section);
          }
        });
        return "\n" + output.join("\n");
      } else {
        return "";
      }
    }
    module2.exports = commandLineUsage;
  }
});

// node_modules/ts-command-line-args/dist/helpers/options.helper.js
var require_options_helper = __commonJS({
  "node_modules/ts-command-line-args/dist/helpers/options.helper.js"(exports) {
    "use strict";
    var __assign = exports && exports.__assign || function() {
      __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
          s = arguments[i];
          for (var p in s)
            if (Object.prototype.hasOwnProperty.call(s, p))
              t[p] = s[p];
        }
        return t;
      };
      return __assign.apply(this, arguments);
    };
    Object.defineProperty(exports, "__esModule", { value: true });
    exports.isBoolean = exports.mapDefinitionDetails = exports.addOptions = exports.generateTableFooter = exports.getOptionFooterSection = exports.getOptionSections = void 0;
    function getOptionSections(options) {
      return options.optionSections || [
        { header: options.optionsHeaderText || "Options", headerLevel: options.optionsHeaderLevel || 2 }
      ];
    }
    exports.getOptionSections = getOptionSections;
    function getOptionFooterSection(optionList, options) {
      var optionsFooter = generateTableFooter(optionList, options);
      if (optionsFooter != null) {
        console.log("Adding footer: " + optionsFooter);
        return [{ content: optionsFooter }];
      }
      return [];
    }
    exports.getOptionFooterSection = getOptionFooterSection;
    function generateTableFooter(optionList, options) {
      if (options.addOptionalDefaultExplanatoryFooter != true || options.displayOptionalAndDefault != true) {
        return void 0;
      }
      var optionalProps = optionList.some(function(option) {
        return option.optional === true;
      });
      var defaultProps = optionList.some(function(option) {
        return option.defaultOption === true;
      });
      if (optionalProps || defaultProps) {
        var footerValues = [
          optionalProps != null ? "(O) = optional" : void 0,
          defaultProps != null ? "(D) = default option" : null
        ];
        return footerValues.filter(function(v) {
          return v != null;
        }).join(", ");
      }
      return void 0;
    }
    exports.generateTableFooter = generateTableFooter;
    function addOptions(content, optionList, options) {
      optionList = optionList.map(function(option) {
        return mapDefinitionDetails(option, options);
      });
      return __assign(__assign({}, content), { optionList });
    }
    exports.addOptions = addOptions;
    function mapDefinitionDetails(definition, options) {
      definition = mapOptionTypeLabel(definition, options);
      definition = mapOptionDescription(definition, options);
      return definition;
    }
    exports.mapDefinitionDetails = mapDefinitionDetails;
    function mapOptionDescription(definition, options) {
      if (options.prependParamOptionsToDescription !== true || isBoolean(definition)) {
        return definition;
      }
      definition.description = definition.description || "";
      if (definition.defaultOption) {
        definition.description = "Default Option. " + definition.description;
      }
      if (definition.optional === true) {
        definition.description = "Optional. " + definition.description;
      }
      if (definition.defaultValue != null) {
        definition.description = "Defaults to " + JSON.stringify(definition.defaultValue) + ". " + definition.description;
      }
      return definition;
    }
    function mapOptionTypeLabel(definition, options) {
      if (options.displayOptionalAndDefault !== true || isBoolean(definition)) {
        return definition;
      }
      definition.typeLabel = definition.typeLabel || getTypeLabel(definition);
      if (definition.defaultOption) {
        definition.typeLabel = definition.typeLabel + " (D)";
      }
      if (definition.optional === true) {
        definition.typeLabel = definition.typeLabel + " (O)";
      }
      return definition;
    }
    function getTypeLabel(definition) {
      var typeLabel = definition.type ? definition.type.name.toLowerCase() : "string";
      var multiple = definition.multiple || definition.lazyMultiple ? "[]" : "";
      if (typeLabel) {
        typeLabel = typeLabel === "boolean" ? "" : "{underline " + typeLabel + multiple + "}";
      }
      return typeLabel;
    }
    function isBoolean(option) {
      return option.type.name === "Boolean";
    }
    exports.isBoolean = isBoolean;
  }
});

// node_modules/ts-command-line-args/dist/helpers/command-line.helper.js
var require_command_line_helper = __commonJS({
  "node_modules/ts-command-line-args/dist/helpers/command-line.helper.js"(exports) {
    "use strict";
    var __assign = exports && exports.__assign || function() {
      __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
          s = arguments[i];
          for (var p in s)
            if (Object.prototype.hasOwnProperty.call(s, p))
              t[p] = s[p];
        }
        return t;
      };
      return __assign.apply(this, arguments);
    };
    var __spreadArray = exports && exports.__spreadArray || function(to, from) {
      for (var i = 0, il = from.length, j = to.length; i < il; i++, j++)
        to[j] = from[i];
      return to;
    };
    Object.defineProperty(exports, "__esModule", { value: true });
    exports.getBooleanValues = exports.removeBooleanValues = exports.mergeConfig = exports.normaliseConfig = exports.createCommandLineConfig = void 0;
    var options_helper_1 = require_options_helper();
    function createCommandLineConfig(config) {
      return Object.keys(config).map(function(key) {
        var argConfig = config[key];
        var definition = typeof argConfig === "object" ? argConfig : { type: argConfig };
        return __assign({ name: key }, definition);
      });
    }
    exports.createCommandLineConfig = createCommandLineConfig;
    function normaliseConfig(config) {
      Object.keys(config).forEach(function(key) {
        var argConfig = config[key];
        config[key] = typeof argConfig === "object" ? argConfig : { type: argConfig };
      });
      return config;
    }
    exports.normaliseConfig = normaliseConfig;
    function mergeConfig(parsedConfig, parsedConfigWithoutDefaults, fileContent, options, jsonPath) {
      var configPath = jsonPath ? parsedConfig[jsonPath] : void 0;
      var configFromFile = resolveConfigFromFile(fileContent, configPath);
      if (configFromFile == null) {
        throw new Error("Could not resolve config object from specified file and path");
      }
      return __assign(__assign(__assign({}, parsedConfig), applyTypeConversion(configFromFile, options)), parsedConfigWithoutDefaults);
    }
    exports.mergeConfig = mergeConfig;
    function resolveConfigFromFile(configfromFile, configPath) {
      if (configPath == null || configPath == "") {
        return configfromFile;
      }
      var paths = configPath.split(".");
      var key = paths.shift();
      if (key == null) {
        return configfromFile;
      }
      var config = configfromFile[key];
      return resolveConfigFromFile(config, paths.join("."));
    }
    function applyTypeConversion(configfromFile, options) {
      var transformedParams = {};
      Object.keys(configfromFile).forEach(function(prop) {
        var key = prop;
        var argumentOptions = options[key];
        if (argumentOptions == null) {
          return;
        }
        var fileValue = configfromFile[key];
        if (argumentOptions.multiple || argumentOptions.lazyMultiple) {
          var fileArrayValue = Array.isArray(fileValue) ? fileValue : [fileValue];
          transformedParams[key] = fileArrayValue.map(function(arrayValue) {
            return convertType(arrayValue, argumentOptions);
          });
        } else {
          transformedParams[key] = convertType(fileValue, argumentOptions);
        }
      });
      return transformedParams;
    }
    function convertType(value, propOptions) {
      if (propOptions.type.name === "Boolean") {
        switch (value) {
          case "true":
            return propOptions.type(true);
          case "false":
            return propOptions.type(false);
        }
      }
      return propOptions.type(value);
    }
    var argNameRegExp = /^-{1,2}(\w+)(=(\w+))?$/;
    var booleanValue = ["1", "0", "true", "false"];
    function removeBooleanValues(args, config) {
      function removeBooleanArgs(argsAndLastValue, arg) {
        var _a = getParamConfig(arg, config), argOptions = _a.argOptions, argValue = _a.argValue;
        var lastOption = argsAndLastValue.lastOption;
        if (lastOption != null && options_helper_1.isBoolean(lastOption) && booleanValue.some(function(boolValue) {
          return boolValue === arg;
        })) {
          var args_1 = argsAndLastValue.args.concat();
          args_1.pop();
          return { args: args_1 };
        } else if (argOptions != null && options_helper_1.isBoolean(argOptions) && argValue != null) {
          return { args: argsAndLastValue.args };
        } else {
          return { args: __spreadArray(__spreadArray([], argsAndLastValue.args), [arg]), lastOption: argOptions };
        }
      }
      return args.reduce(removeBooleanArgs, { args: [] }).args;
    }
    exports.removeBooleanValues = removeBooleanValues;
    function getBooleanValues(args, config) {
      function getBooleanValues2(argsAndLastOption, arg) {
        var _a = getParamConfig(arg, config), argOptions = _a.argOptions, argName = _a.argName, argValue = _a.argValue;
        var lastOption = argsAndLastOption.lastOption;
        if (argOptions != null && options_helper_1.isBoolean(argOptions) && argValue != null && argName != null) {
          argsAndLastOption.partial[argName] = convertType(argValue, argOptions);
        } else if (argsAndLastOption.lastName != null && lastOption != null && options_helper_1.isBoolean(lastOption) && booleanValue.some(function(boolValue) {
          return boolValue === arg;
        })) {
          argsAndLastOption.partial[argsAndLastOption.lastName] = convertType(arg, lastOption);
        }
        return { partial: argsAndLastOption.partial, lastName: argName, lastOption: argOptions };
      }
      return args.reduce(getBooleanValues2, { partial: {} }).partial;
    }
    exports.getBooleanValues = getBooleanValues;
    function getParamConfig(arg, config) {
      var regExpResult = argNameRegExp.exec(arg);
      if (regExpResult == null) {
        return {};
      }
      var nameOrAlias = regExpResult[1];
      for (var argName in config) {
        var argConfig = config[argName];
        if (argName === nameOrAlias || argConfig.alias === nameOrAlias) {
          return { argOptions: argConfig, argName, argValue: regExpResult[3] };
        }
      }
      return {};
    }
  }
});

// node_modules/ts-command-line-args/dist/write-markdown.constants.js
var require_write_markdown_constants = __commonJS({
  "node_modules/ts-command-line-args/dist/write-markdown.constants.js"(exports) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    exports.usageGuideInfo = exports.parseOptions = exports.argumentConfig = exports.footerReplaceBelowMarker = exports.configImportNameDefault = exports.copyCodeAboveDefault = exports.copyCodeBelowDefault = exports.insertCodeAboveDefault = exports.insertCodeBelowDefault = exports.replaceAboveDefault = exports.replaceBelowDefault = void 0;
    exports.replaceBelowDefault = "[//]: ####ts-command-line-args_write-markdown_replaceBelow";
    exports.replaceAboveDefault = "[//]: ####ts-command-line-args_write-markdown_replaceAbove";
    exports.insertCodeBelowDefault = "[//]: # (ts-command-line-args_write-markdown_insertCodeBelow";
    exports.insertCodeAboveDefault = "[//]: # (ts-command-line-args_write-markdown_insertCodeAbove)";
    exports.copyCodeBelowDefault = "// ts-command-line-args_write-markdown_copyCodeBelow";
    exports.copyCodeAboveDefault = "// ts-command-line-args_write-markdown_copyCodeAbove";
    exports.configImportNameDefault = "usageGuideInfo";
    exports.footerReplaceBelowMarker = "[//]: ####ts-command-line-args_generated-by-footer";
    exports.argumentConfig = {
      markdownPath: {
        type: String,
        alias: "m",
        defaultOption: true,
        description: "The file to write to. Without replacement markers the whole file content will be replaced. Path can be absolute or relative."
      },
      replaceBelow: {
        type: String,
        defaultValue: exports.replaceBelowDefault,
        description: "A marker in the file to replace text below.",
        optional: true
      },
      replaceAbove: {
        type: String,
        defaultValue: exports.replaceAboveDefault,
        description: "A marker in the file to replace text above.",
        optional: true
      },
      insertCodeBelow: {
        type: String,
        defaultValue: exports.insertCodeBelowDefault,
        description: `A marker in the file to insert code below. File path to insert must be added at the end of the line and optionally codeComment flag: 'insertToken file="path/toFile.md" codeComment="ts"'`,
        optional: true
      },
      insertCodeAbove: {
        type: String,
        defaultValue: exports.insertCodeAboveDefault,
        description: "A marker in the file to insert code above.",
        optional: true
      },
      copyCodeBelow: {
        type: String,
        defaultValue: exports.copyCodeBelowDefault,
        description: "A marker in the file being inserted to say only copy code below this line",
        optional: true
      },
      copyCodeAbove: {
        type: String,
        defaultValue: exports.copyCodeAboveDefault,
        description: "A marker in the file being inserted to say only copy code above this line",
        optional: true
      },
      jsFile: {
        type: String,
        optional: true,
        alias: "j",
        description: "jsFile to 'require' that has an export with the 'UsageGuideConfig' export. Multiple files can be specified.",
        multiple: true
      },
      configImportName: {
        type: String,
        alias: "c",
        defaultValue: [exports.configImportNameDefault],
        description: "Export name of the 'UsageGuideConfig' object. Defaults to '" + exports.configImportNameDefault + "'. Multiple exports can be specified.",
        multiple: true
      },
      verify: {
        type: Boolean,
        alias: "v",
        description: "Verify the markdown file. Does not update the file but returns a non zero exit code if the markdown file is not correct. Useful for a pre-publish script."
      },
      configFile: {
        type: String,
        alias: "f",
        optional: true,
        description: "Optional config file to load config from. package.json can be used if jsonPath specified as well"
      },
      jsonPath: {
        type: String,
        alias: "p",
        optional: true,
        description: "Used in conjunction with 'configFile'. The path within the config file to load the config from. For example: 'configs.writeMarkdown'"
      },
      verifyMessage: {
        type: String,
        optional: true,
        description: "Optional message that is printed when markdown verification fails. Use '\\{fileName\\}' to refer to the file being processed."
      },
      removeDoubleBlankLines: {
        type: Boolean,
        description: "When replacing content removes any more than a single blank line"
      },
      skipFooter: {
        type: Boolean,
        description: "Does not add the 'Markdown Generated by...' footer to the end of the markdown"
      },
      help: { type: Boolean, alias: "h", description: "Show this usage guide." }
    };
    exports.parseOptions = {
      helpArg: "help",
      loadFromFileArg: "configFile",
      loadFromFileJsonPathArg: "jsonPath",
      baseCommand: "write-markdown",
      optionsHeaderLevel: 3,
      defaultSectionHeaderLevel: 3,
      optionsHeaderText: "write-markdown cli options",
      headerContentSections: [
        {
          header: "Markdown Generation",
          headerLevel: 2,
          content: "A markdown version of the usage guide can be generated and inserted into an existing markdown document.\nMarkers in the document describe where the content should be inserted, existing content betweeen the markers is overwritten."
        },
        {
          content: "{highlight write-markdown -m README.MD -j usageGuideConstants.js}"
        }
      ],
      footerContentSections: [
        {
          header: "Default Replacement Markers",
          content: "replaceBelow defaults to:\n{code '" + exports.replaceBelowDefault + "'}\nreplaceAbove defaults to:\n{code '" + exports.replaceAboveDefault + "'}\ninsertCodeBelow defaults to:\n{code '" + exports.insertCodeBelowDefault + "'}\ninsertCodeAbove defaults to:\n{code '" + exports.insertCodeAboveDefault + "'}\ncopyCodeBelow defaults to:\n{code '" + exports.copyCodeBelowDefault + "'}\ncopyCodeAbove defaults to:\n{code '" + exports.copyCodeAboveDefault + "'}"
        }
      ]
    };
    exports.usageGuideInfo = {
      arguments: exports.argumentConfig,
      parseOptions: exports.parseOptions
    };
  }
});

// node_modules/ts-command-line-args/dist/helpers/line-ending.helper.js
var require_line_ending_helper = __commonJS({
  "node_modules/ts-command-line-args/dist/helpers/line-ending.helper.js"(exports) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    exports.filterDoubleBlankLines = exports.splitContent = exports.findEscapeSequence = exports.getEscapeSequence = exports.lineEndings = void 0;
    var os_1 = require("os");
    exports.lineEndings = ["LF", "CRLF", "CR", "LFCR"];
    var multiCharRegExp = /(\r\n)|(\n\r)/g;
    var singleCharRegExp = /(\r)|(\n)/g;
    function getEscapeSequence(lineEnding) {
      switch (lineEnding) {
        case "CR":
          return "\r";
        case "CRLF":
          return "\r\n";
        case "LF":
          return "\n";
        case "LFCR":
          return "\n\r";
        default:
          return handleNever(lineEnding);
      }
    }
    exports.getEscapeSequence = getEscapeSequence;
    function handleNever(lineEnding) {
      throw new Error("Unknown line ending: '" + lineEnding + "'. Line Ending must be one of " + exports.lineEndings.join(", "));
    }
    function findEscapeSequence(content) {
      var multiCharMatch = multiCharRegExp.exec(content);
      if (multiCharMatch != null) {
        return multiCharMatch[0];
      }
      var singleCharMatch = singleCharRegExp.exec(content);
      if (singleCharMatch != null) {
        return singleCharMatch[0];
      }
      return os_1.EOL;
    }
    exports.findEscapeSequence = findEscapeSequence;
    function splitContent(content) {
      content = content.replace(multiCharRegExp, "\n");
      content = content.replace(singleCharRegExp, "\n");
      return content.split("\n");
    }
    exports.splitContent = splitContent;
    var nonWhitespaceRegExp = /[^ \t]/;
    function filterDoubleBlankLines(line, index, lines) {
      var previousLine = index > 0 ? lines[index - 1] : void 0;
      return nonWhitespaceRegExp.test(line) || previousLine == null || nonWhitespaceRegExp.test(previousLine);
    }
    exports.filterDoubleBlankLines = filterDoubleBlankLines;
  }
});

// node_modules/ts-command-line-args/dist/helpers/add-content.helper.js
var require_add_content_helper = __commonJS({
  "node_modules/ts-command-line-args/dist/helpers/add-content.helper.js"(exports) {
    "use strict";
    var __spreadArray = exports && exports.__spreadArray || function(to, from) {
      for (var i = 0, il = from.length, j = to.length; i < il; i++, j++)
        to[j] = from[i];
      return to;
    };
    Object.defineProperty(exports, "__esModule", { value: true });
    exports.addCommandLineArgsFooter = exports.addContent = void 0;
    var write_markdown_constants_1 = require_write_markdown_constants();
    var line_ending_helper_1 = require_line_ending_helper();
    function addContent(inputString, content, options) {
      var replaceBelow = options === null || options === void 0 ? void 0 : options.replaceBelow;
      var replaceAbove = options === null || options === void 0 ? void 0 : options.replaceAbove;
      content = Array.isArray(content) ? content : [content];
      var lineBreak = line_ending_helper_1.findEscapeSequence(inputString);
      var lines = line_ending_helper_1.splitContent(inputString);
      var replaceBelowLine = replaceBelow != null ? lines.filter(function(line) {
        return line.indexOf(replaceBelow) === 0;
      })[0] : void 0;
      var replaceBelowIndex = replaceBelowLine != null ? lines.indexOf(replaceBelowLine) : -1;
      var replaceAboveLine = replaceAbove != null ? lines.filter(function(line) {
        return line.indexOf(replaceAbove) === 0;
      })[0] : void 0;
      var replaceAboveIndex = replaceAboveLine != null ? lines.indexOf(replaceAboveLine) : -1;
      if (replaceAboveIndex > -1 && replaceBelowIndex > -1 && replaceAboveIndex < replaceBelowIndex) {
        throw new Error("The replaceAbove marker '" + options.replaceAbove + "' was found before the replaceBelow marker '" + options.replaceBelow + "'. The replaceBelow marked must be before the replaceAbove.");
      }
      var linesBefore = lines.slice(0, replaceBelowIndex + 1);
      var linesAfter = replaceAboveIndex >= 0 ? lines.slice(replaceAboveIndex) : [];
      var contentLines = content.reduce(function(lines2, currentContent) {
        return __spreadArray(__spreadArray([], lines2), line_ending_helper_1.splitContent(currentContent));
      }, new Array());
      var allLines = __spreadArray(__spreadArray(__spreadArray([], linesBefore), contentLines), linesAfter);
      if (options.removeDoubleBlankLines) {
        allLines = allLines.filter(function(line, index, lines2) {
          return line_ending_helper_1.filterDoubleBlankLines(line, index, lines2);
        });
      }
      return allLines.join(lineBreak);
    }
    exports.addContent = addContent;
    function addCommandLineArgsFooter(fileContent) {
      if (fileContent.indexOf(write_markdown_constants_1.footerReplaceBelowMarker) < 0) {
        fileContent = fileContent + "  \n\n" + write_markdown_constants_1.footerReplaceBelowMarker;
      }
      var footerContent = "Markdown Generated by [ts-command-line-args](https://www.npmjs.com/package/ts-command-line-args)";
      return addContent(fileContent, footerContent, {
        replaceBelow: write_markdown_constants_1.footerReplaceBelowMarker,
        removeDoubleBlankLines: false
      });
    }
    exports.addCommandLineArgsFooter = addCommandLineArgsFooter;
  }
});

// node_modules/ts-command-line-args/dist/helpers/string.helper.js
var require_string_helper = __commonJS({
  "node_modules/ts-command-line-args/dist/helpers/string.helper.js"(exports) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    exports.removeAdditionalFormatting = exports.convertChalkStringToMarkdown = void 0;
    var chalkStringStyleRegExp = /(?<!\\){([^}]+?)[ \n\r](.+?[^\\])}/gms;
    var newLineRegExp = /\n/g;
    var highlightModifier = "highlight";
    var codeModifier = "code";
    function convertChalkStringToMarkdown(input) {
      return input.replace(chalkStringStyleRegExp, replaceChalkFormatting).replace(newLineRegExp, "  \n").replace(/\\{/g, "{").replace(/\\}/g, "}");
    }
    exports.convertChalkStringToMarkdown = convertChalkStringToMarkdown;
    function replaceChalkFormatting(_substring) {
      var matches = [];
      for (var _i = 1; _i < arguments.length; _i++) {
        matches[_i - 1] = arguments[_i];
      }
      var modifier = "";
      if (matches[0].indexOf(highlightModifier) >= 0) {
        modifier = "`";
      } else if (matches[0].indexOf(codeModifier) >= 0) {
        var codeOptions = matches[0].split(".");
        modifier = "\n```";
        if (codeOptions[1] != null) {
          return "" + modifier + codeOptions[1] + "\n" + matches[1] + modifier + "\n";
        } else {
          return modifier + "\n" + matches[1] + modifier + "\n";
        }
      } else {
        if (matches[0].indexOf("bold") >= 0) {
          modifier += "**";
        }
        if (matches[0].indexOf("italic") >= 0) {
          modifier += "*";
        }
      }
      return "" + modifier + matches[1] + modifier;
    }
    function removeAdditionalFormatting(input) {
      return input.replace(chalkStringStyleRegExp, removeNonChalkFormatting);
    }
    exports.removeAdditionalFormatting = removeAdditionalFormatting;
    function removeNonChalkFormatting(substring) {
      var matches = [];
      for (var _i = 1; _i < arguments.length; _i++) {
        matches[_i - 1] = arguments[_i];
      }
      var nonChalkFormats = [highlightModifier, codeModifier];
      if (nonChalkFormats.some(function(format) {
        return matches[0].indexOf(format) >= 0;
      })) {
        return matches[1];
      }
      return substring;
    }
  }
});

// node_modules/ts-command-line-args/dist/helpers/markdown.helper.js
var require_markdown_helper = __commonJS({
  "node_modules/ts-command-line-args/dist/helpers/markdown.helper.js"(exports) {
    "use strict";
    var __spreadArray = exports && exports.__spreadArray || function(to, from) {
      for (var i = 0, il = from.length, j = to.length; i < il; i++, j++)
        to[j] = from[i];
      return to;
    };
    Object.defineProperty(exports, "__esModule", { value: true });
    exports.loadArgConfig = exports.generateUsageGuides = exports.getType = exports.createOptionRow = exports.createHeading = exports.createOptionsSection = exports.createOptionsSections = exports.createSectionTable = exports.createSectionContent = exports.createSection = exports.createUsageGuide = void 0;
    var path_1 = require("path");
    var command_line_helper_1 = require_command_line_helper();
    var options_helper_1 = require_options_helper();
    var string_helper_1 = require_string_helper();
    function createUsageGuide(config) {
      var options = config.parseOptions || {};
      var headerSections = options.headerContentSections || [];
      var footerSections = options.footerContentSections || [];
      return __spreadArray(__spreadArray(__spreadArray([], headerSections.filter(filterMarkdownSections).map(function(section) {
        return createSection(section, config);
      })), createOptionsSections(config.arguments, options)), footerSections.filter(filterMarkdownSections).map(function(section) {
        return createSection(section, config);
      })).join("\n");
    }
    exports.createUsageGuide = createUsageGuide;
    function filterMarkdownSections(section) {
      return section.includeIn == null || section.includeIn === "both" || section.includeIn === "markdown";
    }
    function createSection(section, config) {
      var _a;
      return "\n" + createHeading(section, ((_a = config.parseOptions) === null || _a === void 0 ? void 0 : _a.defaultSectionHeaderLevel) || 1) + "\n" + createSectionContent(section) + "\n";
    }
    exports.createSection = createSection;
    function createSectionContent(section) {
      if (typeof section.content === "string") {
        return string_helper_1.convertChalkStringToMarkdown(section.content);
      }
      if (Array.isArray(section.content)) {
        if (section.content.every(function(content) {
          return typeof content === "string";
        })) {
          return section.content.map(string_helper_1.convertChalkStringToMarkdown).join("\n");
        } else if (section.content.every(function(content) {
          return typeof content === "object";
        })) {
          return createSectionTable(section.content);
        }
      }
      return "";
    }
    exports.createSectionContent = createSectionContent;
    function createSectionTable(rows) {
      if (rows.length === 0) {
        return "";
      }
      var cellKeys = Object.keys(rows[0]);
      return "\n|" + cellKeys.map(function(key) {
        return " " + key + " ";
      }).join("|") + "|\n|" + cellKeys.map(function() {
        return "-";
      }).join("|") + "|\n" + rows.map(function(row) {
        return "| " + cellKeys.map(function(key) {
          return string_helper_1.convertChalkStringToMarkdown(row[key]);
        }).join(" | ") + " |";
      }).join("\n");
    }
    exports.createSectionTable = createSectionTable;
    function createOptionsSections(cliArguments, options) {
      var normalisedConfig = command_line_helper_1.normaliseConfig(cliArguments);
      var optionList = command_line_helper_1.createCommandLineConfig(normalisedConfig);
      if (optionList.length === 0) {
        return [];
      }
      return options_helper_1.getOptionSections(options).map(function(section) {
        return createOptionsSection(optionList, section, options);
      });
    }
    exports.createOptionsSections = createOptionsSections;
    function createOptionsSection(optionList, content, options) {
      optionList = optionList.filter(function(option) {
        return filterOptions(option, content.group);
      });
      var anyAlias = optionList.some(function(option) {
        return option.alias != null;
      });
      var anyDescription = optionList.some(function(option) {
        return option.description != null;
      });
      var footer = options_helper_1.generateTableFooter(optionList, options);
      return "\n" + createHeading(content, 2) + "\n| Argument |" + (anyAlias ? " Alias |" : "") + " Type |" + (anyDescription ? " Description |" : "") + "\n|-|" + (anyAlias ? "-|" : "") + "-|" + (anyDescription ? "-|" : "") + "\n" + optionList.map(function(option) {
        return options_helper_1.mapDefinitionDetails(option, options);
      }).map(function(option) {
        return createOptionRow(option, anyAlias, anyDescription);
      }).join("\n") + "\n" + (footer != null ? footer + "\n" : "");
    }
    exports.createOptionsSection = createOptionsSection;
    function filterOptions(option, groups) {
      return groups == null || typeof groups === "string" && (groups === option.group || groups === "_none" && option.group == null) || Array.isArray(groups) && (groups.some(function(group) {
        return group === option.group;
      }) || groups.some(function(group) {
        return group === "_none";
      }) && option.group == null);
    }
    function createHeading(section, defaultLevel) {
      if (section.header == null) {
        return "";
      }
      var headingLevel = Array.from({ length: section.headerLevel || defaultLevel }).map(function() {
        return "#";
      }).join("");
      return headingLevel + " " + section.header + "\n";
    }
    exports.createHeading = createHeading;
    function createOptionRow(option, includeAlias, includeDescription) {
      if (includeAlias === void 0) {
        includeAlias = true;
      }
      if (includeDescription === void 0) {
        includeDescription = true;
      }
      var alias = includeAlias ? " " + (option.alias == null ? "" : "**" + option.alias + "** ") + "|" : "";
      var description = includeDescription ? " " + (option.description == null ? "" : string_helper_1.convertChalkStringToMarkdown(option.description) + " ") + "|" : "";
      return "| **" + option.name + "** |" + alias + " " + getType(option) + "|" + description;
    }
    exports.createOptionRow = createOptionRow;
    function getType(option) {
      if (option.typeLabel) {
        return string_helper_1.convertChalkStringToMarkdown(option.typeLabel) + " ";
      }
      var type = option.type ? option.type.name.toLowerCase() : "string";
      var multiple = option.multiple || option.lazyMultiple ? "[]" : "";
      return "" + type + multiple + " ";
    }
    exports.getType = getType;
    function generateUsageGuides(args) {
      if (args.jsFile == null) {
        console.log("No jsFile defined for usage guide generation. See 'write-markdown -h' for details on generating usage guides.");
        return void 0;
      }
      function mapJsImports(imports, jsFile) {
        return __spreadArray(__spreadArray([], imports), args.configImportName.map(function(importName) {
          return { jsFile, importName };
        }));
      }
      return args.jsFile.reduce(mapJsImports, new Array()).map(function(_a) {
        var jsFile = _a.jsFile, importName = _a.importName;
        return loadArgConfig(jsFile, importName);
      }).filter(isDefined).map(createUsageGuide);
    }
    exports.generateUsageGuides = generateUsageGuides;
    function loadArgConfig(jsFile, importName) {
      var jsPath = path_1.join(process.cwd(), jsFile);
      var jsExports = require(jsPath);
      var argConfig = jsExports[importName];
      if (argConfig == null) {
        console.warn("Could not import ArgumentConfig named '" + importName + "' from jsFile '" + jsFile + "'");
        return void 0;
      }
      return argConfig;
    }
    exports.loadArgConfig = loadArgConfig;
    function isDefined(value) {
      return value != null;
    }
  }
});

// node_modules/ts-command-line-args/dist/helpers/visitor.js
var require_visitor = __commonJS({
  "node_modules/ts-command-line-args/dist/helpers/visitor.js"(exports) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    exports.visit = void 0;
    function visit(value, callback) {
      if (Array.isArray(value)) {
        value.forEach(function(_, index) {
          return visitKey(index, value, callback);
        });
      } else {
        Object.keys(value).forEach(function(key) {
          return visitKey(key, value, callback);
        });
      }
      return value;
    }
    exports.visit = visit;
    function visitKey(key, parent, callback) {
      var keyValue = parent[key];
      parent[key] = callback(keyValue, key, parent);
      if (typeof keyValue === "object") {
        visit(keyValue, callback);
      }
    }
  }
});

// node_modules/color-name/index.js
var require_color_name2 = __commonJS({
  "node_modules/color-name/index.js"(exports, module2) {
    "use strict";
    module2.exports = {
      "aliceblue": [240, 248, 255],
      "antiquewhite": [250, 235, 215],
      "aqua": [0, 255, 255],
      "aquamarine": [127, 255, 212],
      "azure": [240, 255, 255],
      "beige": [245, 245, 220],
      "bisque": [255, 228, 196],
      "black": [0, 0, 0],
      "blanchedalmond": [255, 235, 205],
      "blue": [0, 0, 255],
      "blueviolet": [138, 43, 226],
      "brown": [165, 42, 42],
      "burlywood": [222, 184, 135],
      "cadetblue": [95, 158, 160],
      "chartreuse": [127, 255, 0],
      "chocolate": [210, 105, 30],
      "coral": [255, 127, 80],
      "cornflowerblue": [100, 149, 237],
      "cornsilk": [255, 248, 220],
      "crimson": [220, 20, 60],
      "cyan": [0, 255, 255],
      "darkblue": [0, 0, 139],
      "darkcyan": [0, 139, 139],
      "darkgoldenrod": [184, 134, 11],
      "darkgray": [169, 169, 169],
      "darkgreen": [0, 100, 0],
      "darkgrey": [169, 169, 169],
      "darkkhaki": [189, 183, 107],
      "darkmagenta": [139, 0, 139],
      "darkolivegreen": [85, 107, 47],
      "darkorange": [255, 140, 0],
      "darkorchid": [153, 50, 204],
      "darkred": [139, 0, 0],
      "darksalmon": [233, 150, 122],
      "darkseagreen": [143, 188, 143],
      "darkslateblue": [72, 61, 139],
      "darkslategray": [47, 79, 79],
      "darkslategrey": [47, 79, 79],
      "darkturquoise": [0, 206, 209],
      "darkviolet": [148, 0, 211],
      "deeppink": [255, 20, 147],
      "deepskyblue": [0, 191, 255],
      "dimgray": [105, 105, 105],
      "dimgrey": [105, 105, 105],
      "dodgerblue": [30, 144, 255],
      "firebrick": [178, 34, 34],
      "floralwhite": [255, 250, 240],
      "forestgreen": [34, 139, 34],
      "fuchsia": [255, 0, 255],
      "gainsboro": [220, 220, 220],
      "ghostwhite": [248, 248, 255],
      "gold": [255, 215, 0],
      "goldenrod": [218, 165, 32],
      "gray": [128, 128, 128],
      "green": [0, 128, 0],
      "greenyellow": [173, 255, 47],
      "grey": [128, 128, 128],
      "honeydew": [240, 255, 240],
      "hotpink": [255, 105, 180],
      "indianred": [205, 92, 92],
      "indigo": [75, 0, 130],
      "ivory": [255, 255, 240],
      "khaki": [240, 230, 140],
      "lavender": [230, 230, 250],
      "lavenderblush": [255, 240, 245],
      "lawngreen": [124, 252, 0],
      "lemonchiffon": [255, 250, 205],
      "lightblue": [173, 216, 230],
      "lightcoral": [240, 128, 128],
      "lightcyan": [224, 255, 255],
      "lightgoldenrodyellow": [250, 250, 210],
      "lightgray": [211, 211, 211],
      "lightgreen": [144, 238, 144],
      "lightgrey": [211, 211, 211],
      "lightpink": [255, 182, 193],
      "lightsalmon": [255, 160, 122],
      "lightseagreen": [32, 178, 170],
      "lightskyblue": [135, 206, 250],
      "lightslategray": [119, 136, 153],
      "lightslategrey": [119, 136, 153],
      "lightsteelblue": [176, 196, 222],
      "lightyellow": [255, 255, 224],
      "lime": [0, 255, 0],
      "limegreen": [50, 205, 50],
      "linen": [250, 240, 230],
      "magenta": [255, 0, 255],
      "maroon": [128, 0, 0],
      "mediumaquamarine": [102, 205, 170],
      "mediumblue": [0, 0, 205],
      "mediumorchid": [186, 85, 211],
      "mediumpurple": [147, 112, 219],
      "mediumseagreen": [60, 179, 113],
      "mediumslateblue": [123, 104, 238],
      "mediumspringgreen": [0, 250, 154],
      "mediumturquoise": [72, 209, 204],
      "mediumvioletred": [199, 21, 133],
      "midnightblue": [25, 25, 112],
      "mintcream": [245, 255, 250],
      "mistyrose": [255, 228, 225],
      "moccasin": [255, 228, 181],
      "navajowhite": [255, 222, 173],
      "navy": [0, 0, 128],
      "oldlace": [253, 245, 230],
      "olive": [128, 128, 0],
      "olivedrab": [107, 142, 35],
      "orange": [255, 165, 0],
      "orangered": [255, 69, 0],
      "orchid": [218, 112, 214],
      "palegoldenrod": [238, 232, 170],
      "palegreen": [152, 251, 152],
      "paleturquoise": [175, 238, 238],
      "palevioletred": [219, 112, 147],
      "papayawhip": [255, 239, 213],
      "peachpuff": [255, 218, 185],
      "peru": [205, 133, 63],
      "pink": [255, 192, 203],
      "plum": [221, 160, 221],
      "powderblue": [176, 224, 230],
      "purple": [128, 0, 128],
      "rebeccapurple": [102, 51, 153],
      "red": [255, 0, 0],
      "rosybrown": [188, 143, 143],
      "royalblue": [65, 105, 225],
      "saddlebrown": [139, 69, 19],
      "salmon": [250, 128, 114],
      "sandybrown": [244, 164, 96],
      "seagreen": [46, 139, 87],
      "seashell": [255, 245, 238],
      "sienna": [160, 82, 45],
      "silver": [192, 192, 192],
      "skyblue": [135, 206, 235],
      "slateblue": [106, 90, 205],
      "slategray": [112, 128, 144],
      "slategrey": [112, 128, 144],
      "snow": [255, 250, 250],
      "springgreen": [0, 255, 127],
      "steelblue": [70, 130, 180],
      "tan": [210, 180, 140],
      "teal": [0, 128, 128],
      "thistle": [216, 191, 216],
      "tomato": [255, 99, 71],
      "turquoise": [64, 224, 208],
      "violet": [238, 130, 238],
      "wheat": [245, 222, 179],
      "white": [255, 255, 255],
      "whitesmoke": [245, 245, 245],
      "yellow": [255, 255, 0],
      "yellowgreen": [154, 205, 50]
    };
  }
});

// node_modules/color-convert/conversions.js
var require_conversions2 = __commonJS({
  "node_modules/color-convert/conversions.js"(exports, module2) {
    var cssKeywords = require_color_name2();
    var reverseKeywords = {};
    for (const key of Object.keys(cssKeywords)) {
      reverseKeywords[cssKeywords[key]] = key;
    }
    var convert = {
      rgb: { channels: 3, labels: "rgb" },
      hsl: { channels: 3, labels: "hsl" },
      hsv: { channels: 3, labels: "hsv" },
      hwb: { channels: 3, labels: "hwb" },
      cmyk: { channels: 4, labels: "cmyk" },
      xyz: { channels: 3, labels: "xyz" },
      lab: { channels: 3, labels: "lab" },
      lch: { channels: 3, labels: "lch" },
      hex: { channels: 1, labels: ["hex"] },
      keyword: { channels: 1, labels: ["keyword"] },
      ansi16: { channels: 1, labels: ["ansi16"] },
      ansi256: { channels: 1, labels: ["ansi256"] },
      hcg: { channels: 3, labels: ["h", "c", "g"] },
      apple: { channels: 3, labels: ["r16", "g16", "b16"] },
      gray: { channels: 1, labels: ["gray"] }
    };
    module2.exports = convert;
    for (const model of Object.keys(convert)) {
      if (!("channels" in convert[model])) {
        throw new Error("missing channels property: " + model);
      }
      if (!("labels" in convert[model])) {
        throw new Error("missing channel labels property: " + model);
      }
      if (convert[model].labels.length !== convert[model].channels) {
        throw new Error("channel and label counts mismatch: " + model);
      }
      const { channels, labels } = convert[model];
      delete convert[model].channels;
      delete convert[model].labels;
      Object.defineProperty(convert[model], "channels", { value: channels });
      Object.defineProperty(convert[model], "labels", { value: labels });
    }
    convert.rgb.hsl = function(rgb) {
      const r = rgb[0] / 255;
      const g = rgb[1] / 255;
      const b = rgb[2] / 255;
      const min = Math.min(r, g, b);
      const max = Math.max(r, g, b);
      const delta = max - min;
      let h;
      let s;
      if (max === min) {
        h = 0;
      } else if (r === max) {
        h = (g - b) / delta;
      } else if (g === max) {
        h = 2 + (b - r) / delta;
      } else if (b === max) {
        h = 4 + (r - g) / delta;
      }
      h = Math.min(h * 60, 360);
      if (h < 0) {
        h += 360;
      }
      const l = (min + max) / 2;
      if (max === min) {
        s = 0;
      } else if (l <= 0.5) {
        s = delta / (max + min);
      } else {
        s = delta / (2 - max - min);
      }
      return [h, s * 100, l * 100];
    };
    convert.rgb.hsv = function(rgb) {
      let rdif;
      let gdif;
      let bdif;
      let h;
      let s;
      const r = rgb[0] / 255;
      const g = rgb[1] / 255;
      const b = rgb[2] / 255;
      const v = Math.max(r, g, b);
      const diff = v - Math.min(r, g, b);
      const diffc = function(c) {
        return (v - c) / 6 / diff + 1 / 2;
      };
      if (diff === 0) {
        h = 0;
        s = 0;
      } else {
        s = diff / v;
        rdif = diffc(r);
        gdif = diffc(g);
        bdif = diffc(b);
        if (r === v) {
          h = bdif - gdif;
        } else if (g === v) {
          h = 1 / 3 + rdif - bdif;
        } else if (b === v) {
          h = 2 / 3 + gdif - rdif;
        }
        if (h < 0) {
          h += 1;
        } else if (h > 1) {
          h -= 1;
        }
      }
      return [
        h * 360,
        s * 100,
        v * 100
      ];
    };
    convert.rgb.hwb = function(rgb) {
      const r = rgb[0];
      const g = rgb[1];
      let b = rgb[2];
      const h = convert.rgb.hsl(rgb)[0];
      const w = 1 / 255 * Math.min(r, Math.min(g, b));
      b = 1 - 1 / 255 * Math.max(r, Math.max(g, b));
      return [h, w * 100, b * 100];
    };
    convert.rgb.cmyk = function(rgb) {
      const r = rgb[0] / 255;
      const g = rgb[1] / 255;
      const b = rgb[2] / 255;
      const k = Math.min(1 - r, 1 - g, 1 - b);
      const c = (1 - r - k) / (1 - k) || 0;
      const m = (1 - g - k) / (1 - k) || 0;
      const y = (1 - b - k) / (1 - k) || 0;
      return [c * 100, m * 100, y * 100, k * 100];
    };
    function comparativeDistance(x, y) {
      return (x[0] - y[0]) ** 2 + (x[1] - y[1]) ** 2 + (x[2] - y[2]) ** 2;
    }
    convert.rgb.keyword = function(rgb) {
      const reversed = reverseKeywords[rgb];
      if (reversed) {
        return reversed;
      }
      let currentClosestDistance = Infinity;
      let currentClosestKeyword;
      for (const keyword of Object.keys(cssKeywords)) {
        const value = cssKeywords[keyword];
        const distance = comparativeDistance(rgb, value);
        if (distance < currentClosestDistance) {
          currentClosestDistance = distance;
          currentClosestKeyword = keyword;
        }
      }
      return currentClosestKeyword;
    };
    convert.keyword.rgb = function(keyword) {
      return cssKeywords[keyword];
    };
    convert.rgb.xyz = function(rgb) {
      let r = rgb[0] / 255;
      let g = rgb[1] / 255;
      let b = rgb[2] / 255;
      r = r > 0.04045 ? ((r + 0.055) / 1.055) ** 2.4 : r / 12.92;
      g = g > 0.04045 ? ((g + 0.055) / 1.055) ** 2.4 : g / 12.92;
      b = b > 0.04045 ? ((b + 0.055) / 1.055) ** 2.4 : b / 12.92;
      const x = r * 0.4124 + g * 0.3576 + b * 0.1805;
      const y = r * 0.2126 + g * 0.7152 + b * 0.0722;
      const z = r * 0.0193 + g * 0.1192 + b * 0.9505;
      return [x * 100, y * 100, z * 100];
    };
    convert.rgb.lab = function(rgb) {
      const xyz = convert.rgb.xyz(rgb);
      let x = xyz[0];
      let y = xyz[1];
      let z = xyz[2];
      x /= 95.047;
      y /= 100;
      z /= 108.883;
      x = x > 8856e-6 ? x ** (1 / 3) : 7.787 * x + 16 / 116;
      y = y > 8856e-6 ? y ** (1 / 3) : 7.787 * y + 16 / 116;
      z = z > 8856e-6 ? z ** (1 / 3) : 7.787 * z + 16 / 116;
      const l = 116 * y - 16;
      const a = 500 * (x - y);
      const b = 200 * (y - z);
      return [l, a, b];
    };
    convert.hsl.rgb = function(hsl) {
      const h = hsl[0] / 360;
      const s = hsl[1] / 100;
      const l = hsl[2] / 100;
      let t2;
      let t3;
      let val;
      if (s === 0) {
        val = l * 255;
        return [val, val, val];
      }
      if (l < 0.5) {
        t2 = l * (1 + s);
      } else {
        t2 = l + s - l * s;
      }
      const t1 = 2 * l - t2;
      const rgb = [0, 0, 0];
      for (let i = 0; i < 3; i++) {
        t3 = h + 1 / 3 * -(i - 1);
        if (t3 < 0) {
          t3++;
        }
        if (t3 > 1) {
          t3--;
        }
        if (6 * t3 < 1) {
          val = t1 + (t2 - t1) * 6 * t3;
        } else if (2 * t3 < 1) {
          val = t2;
        } else if (3 * t3 < 2) {
          val = t1 + (t2 - t1) * (2 / 3 - t3) * 6;
        } else {
          val = t1;
        }
        rgb[i] = val * 255;
      }
      return rgb;
    };
    convert.hsl.hsv = function(hsl) {
      const h = hsl[0];
      let s = hsl[1] / 100;
      let l = hsl[2] / 100;
      let smin = s;
      const lmin = Math.max(l, 0.01);
      l *= 2;
      s *= l <= 1 ? l : 2 - l;
      smin *= lmin <= 1 ? lmin : 2 - lmin;
      const v = (l + s) / 2;
      const sv = l === 0 ? 2 * smin / (lmin + smin) : 2 * s / (l + s);
      return [h, sv * 100, v * 100];
    };
    convert.hsv.rgb = function(hsv) {
      const h = hsv[0] / 60;
      const s = hsv[1] / 100;
      let v = hsv[2] / 100;
      const hi = Math.floor(h) % 6;
      const f = h - Math.floor(h);
      const p = 255 * v * (1 - s);
      const q = 255 * v * (1 - s * f);
      const t = 255 * v * (1 - s * (1 - f));
      v *= 255;
      switch (hi) {
        case 0:
          return [v, t, p];
        case 1:
          return [q, v, p];
        case 2:
          return [p, v, t];
        case 3:
          return [p, q, v];
        case 4:
          return [t, p, v];
        case 5:
          return [v, p, q];
      }
    };
    convert.hsv.hsl = function(hsv) {
      const h = hsv[0];
      const s = hsv[1] / 100;
      const v = hsv[2] / 100;
      const vmin = Math.max(v, 0.01);
      let sl;
      let l;
      l = (2 - s) * v;
      const lmin = (2 - s) * vmin;
      sl = s * vmin;
      sl /= lmin <= 1 ? lmin : 2 - lmin;
      sl = sl || 0;
      l /= 2;
      return [h, sl * 100, l * 100];
    };
    convert.hwb.rgb = function(hwb) {
      const h = hwb[0] / 360;
      let wh = hwb[1] / 100;
      let bl = hwb[2] / 100;
      const ratio = wh + bl;
      let f;
      if (ratio > 1) {
        wh /= ratio;
        bl /= ratio;
      }
      const i = Math.floor(6 * h);
      const v = 1 - bl;
      f = 6 * h - i;
      if ((i & 1) !== 0) {
        f = 1 - f;
      }
      const n = wh + f * (v - wh);
      let r;
      let g;
      let b;
      switch (i) {
        default:
        case 6:
        case 0:
          r = v;
          g = n;
          b = wh;
          break;
        case 1:
          r = n;
          g = v;
          b = wh;
          break;
        case 2:
          r = wh;
          g = v;
          b = n;
          break;
        case 3:
          r = wh;
          g = n;
          b = v;
          break;
        case 4:
          r = n;
          g = wh;
          b = v;
          break;
        case 5:
          r = v;
          g = wh;
          b = n;
          break;
      }
      return [r * 255, g * 255, b * 255];
    };
    convert.cmyk.rgb = function(cmyk) {
      const c = cmyk[0] / 100;
      const m = cmyk[1] / 100;
      const y = cmyk[2] / 100;
      const k = cmyk[3] / 100;
      const r = 1 - Math.min(1, c * (1 - k) + k);
      const g = 1 - Math.min(1, m * (1 - k) + k);
      const b = 1 - Math.min(1, y * (1 - k) + k);
      return [r * 255, g * 255, b * 255];
    };
    convert.xyz.rgb = function(xyz) {
      const x = xyz[0] / 100;
      const y = xyz[1] / 100;
      const z = xyz[2] / 100;
      let r;
      let g;
      let b;
      r = x * 3.2406 + y * -1.5372 + z * -0.4986;
      g = x * -0.9689 + y * 1.8758 + z * 0.0415;
      b = x * 0.0557 + y * -0.204 + z * 1.057;
      r = r > 31308e-7 ? 1.055 * r ** (1 / 2.4) - 0.055 : r * 12.92;
      g = g > 31308e-7 ? 1.055 * g ** (1 / 2.4) - 0.055 : g * 12.92;
      b = b > 31308e-7 ? 1.055 * b ** (1 / 2.4) - 0.055 : b * 12.92;
      r = Math.min(Math.max(0, r), 1);
      g = Math.min(Math.max(0, g), 1);
      b = Math.min(Math.max(0, b), 1);
      return [r * 255, g * 255, b * 255];
    };
    convert.xyz.lab = function(xyz) {
      let x = xyz[0];
      let y = xyz[1];
      let z = xyz[2];
      x /= 95.047;
      y /= 100;
      z /= 108.883;
      x = x > 8856e-6 ? x ** (1 / 3) : 7.787 * x + 16 / 116;
      y = y > 8856e-6 ? y ** (1 / 3) : 7.787 * y + 16 / 116;
      z = z > 8856e-6 ? z ** (1 / 3) : 7.787 * z + 16 / 116;
      const l = 116 * y - 16;
      const a = 500 * (x - y);
      const b = 200 * (y - z);
      return [l, a, b];
    };
    convert.lab.xyz = function(lab) {
      const l = lab[0];
      const a = lab[1];
      const b = lab[2];
      let x;
      let y;
      let z;
      y = (l + 16) / 116;
      x = a / 500 + y;
      z = y - b / 200;
      const y2 = y ** 3;
      const x2 = x ** 3;
      const z2 = z ** 3;
      y = y2 > 8856e-6 ? y2 : (y - 16 / 116) / 7.787;
      x = x2 > 8856e-6 ? x2 : (x - 16 / 116) / 7.787;
      z = z2 > 8856e-6 ? z2 : (z - 16 / 116) / 7.787;
      x *= 95.047;
      y *= 100;
      z *= 108.883;
      return [x, y, z];
    };
    convert.lab.lch = function(lab) {
      const l = lab[0];
      const a = lab[1];
      const b = lab[2];
      let h;
      const hr = Math.atan2(b, a);
      h = hr * 360 / 2 / Math.PI;
      if (h < 0) {
        h += 360;
      }
      const c = Math.sqrt(a * a + b * b);
      return [l, c, h];
    };
    convert.lch.lab = function(lch) {
      const l = lch[0];
      const c = lch[1];
      const h = lch[2];
      const hr = h / 360 * 2 * Math.PI;
      const a = c * Math.cos(hr);
      const b = c * Math.sin(hr);
      return [l, a, b];
    };
    convert.rgb.ansi16 = function(args, saturation = null) {
      const [r, g, b] = args;
      let value = saturation === null ? convert.rgb.hsv(args)[2] : saturation;
      value = Math.round(value / 50);
      if (value === 0) {
        return 30;
      }
      let ansi = 30 + (Math.round(b / 255) << 2 | Math.round(g / 255) << 1 | Math.round(r / 255));
      if (value === 2) {
        ansi += 60;
      }
      return ansi;
    };
    convert.hsv.ansi16 = function(args) {
      return convert.rgb.ansi16(convert.hsv.rgb(args), args[2]);
    };
    convert.rgb.ansi256 = function(args) {
      const r = args[0];
      const g = args[1];
      const b = args[2];
      if (r === g && g === b) {
        if (r < 8) {
          return 16;
        }
        if (r > 248) {
          return 231;
        }
        return Math.round((r - 8) / 247 * 24) + 232;
      }
      const ansi = 16 + 36 * Math.round(r / 255 * 5) + 6 * Math.round(g / 255 * 5) + Math.round(b / 255 * 5);
      return ansi;
    };
    convert.ansi16.rgb = function(args) {
      let color = args % 10;
      if (color === 0 || color === 7) {
        if (args > 50) {
          color += 3.5;
        }
        color = color / 10.5 * 255;
        return [color, color, color];
      }
      const mult = (~~(args > 50) + 1) * 0.5;
      const r = (color & 1) * mult * 255;
      const g = (color >> 1 & 1) * mult * 255;
      const b = (color >> 2 & 1) * mult * 255;
      return [r, g, b];
    };
    convert.ansi256.rgb = function(args) {
      if (args >= 232) {
        const c = (args - 232) * 10 + 8;
        return [c, c, c];
      }
      args -= 16;
      let rem;
      const r = Math.floor(args / 36) / 5 * 255;
      const g = Math.floor((rem = args % 36) / 6) / 5 * 255;
      const b = rem % 6 / 5 * 255;
      return [r, g, b];
    };
    convert.rgb.hex = function(args) {
      const integer = ((Math.round(args[0]) & 255) << 16) + ((Math.round(args[1]) & 255) << 8) + (Math.round(args[2]) & 255);
      const string = integer.toString(16).toUpperCase();
      return "000000".substring(string.length) + string;
    };
    convert.hex.rgb = function(args) {
      const match = args.toString(16).match(/[a-f0-9]{6}|[a-f0-9]{3}/i);
      if (!match) {
        return [0, 0, 0];
      }
      let colorString = match[0];
      if (match[0].length === 3) {
        colorString = colorString.split("").map((char) => {
          return char + char;
        }).join("");
      }
      const integer = parseInt(colorString, 16);
      const r = integer >> 16 & 255;
      const g = integer >> 8 & 255;
      const b = integer & 255;
      return [r, g, b];
    };
    convert.rgb.hcg = function(rgb) {
      const r = rgb[0] / 255;
      const g = rgb[1] / 255;
      const b = rgb[2] / 255;
      const max = Math.max(Math.max(r, g), b);
      const min = Math.min(Math.min(r, g), b);
      const chroma = max - min;
      let grayscale;
      let hue;
      if (chroma < 1) {
        grayscale = min / (1 - chroma);
      } else {
        grayscale = 0;
      }
      if (chroma <= 0) {
        hue = 0;
      } else if (max === r) {
        hue = (g - b) / chroma % 6;
      } else if (max === g) {
        hue = 2 + (b - r) / chroma;
      } else {
        hue = 4 + (r - g) / chroma;
      }
      hue /= 6;
      hue %= 1;
      return [hue * 360, chroma * 100, grayscale * 100];
    };
    convert.hsl.hcg = function(hsl) {
      const s = hsl[1] / 100;
      const l = hsl[2] / 100;
      const c = l < 0.5 ? 2 * s * l : 2 * s * (1 - l);
      let f = 0;
      if (c < 1) {
        f = (l - 0.5 * c) / (1 - c);
      }
      return [hsl[0], c * 100, f * 100];
    };
    convert.hsv.hcg = function(hsv) {
      const s = hsv[1] / 100;
      const v = hsv[2] / 100;
      const c = s * v;
      let f = 0;
      if (c < 1) {
        f = (v - c) / (1 - c);
      }
      return [hsv[0], c * 100, f * 100];
    };
    convert.hcg.rgb = function(hcg) {
      const h = hcg[0] / 360;
      const c = hcg[1] / 100;
      const g = hcg[2] / 100;
      if (c === 0) {
        return [g * 255, g * 255, g * 255];
      }
      const pure = [0, 0, 0];
      const hi = h % 1 * 6;
      const v = hi % 1;
      const w = 1 - v;
      let mg = 0;
      switch (Math.floor(hi)) {
        case 0:
          pure[0] = 1;
          pure[1] = v;
          pure[2] = 0;
          break;
        case 1:
          pure[0] = w;
          pure[1] = 1;
          pure[2] = 0;
          break;
        case 2:
          pure[0] = 0;
          pure[1] = 1;
          pure[2] = v;
          break;
        case 3:
          pure[0] = 0;
          pure[1] = w;
          pure[2] = 1;
          break;
        case 4:
          pure[0] = v;
          pure[1] = 0;
          pure[2] = 1;
          break;
        default:
          pure[0] = 1;
          pure[1] = 0;
          pure[2] = w;
      }
      mg = (1 - c) * g;
      return [
        (c * pure[0] + mg) * 255,
        (c * pure[1] + mg) * 255,
        (c * pure[2] + mg) * 255
      ];
    };
    convert.hcg.hsv = function(hcg) {
      const c = hcg[1] / 100;
      const g = hcg[2] / 100;
      const v = c + g * (1 - c);
      let f = 0;
      if (v > 0) {
        f = c / v;
      }
      return [hcg[0], f * 100, v * 100];
    };
    convert.hcg.hsl = function(hcg) {
      const c = hcg[1] / 100;
      const g = hcg[2] / 100;
      const l = g * (1 - c) + 0.5 * c;
      let s = 0;
      if (l > 0 && l < 0.5) {
        s = c / (2 * l);
      } else if (l >= 0.5 && l < 1) {
        s = c / (2 * (1 - l));
      }
      return [hcg[0], s * 100, l * 100];
    };
    convert.hcg.hwb = function(hcg) {
      const c = hcg[1] / 100;
      const g = hcg[2] / 100;
      const v = c + g * (1 - c);
      return [hcg[0], (v - c) * 100, (1 - v) * 100];
    };
    convert.hwb.hcg = function(hwb) {
      const w = hwb[1] / 100;
      const b = hwb[2] / 100;
      const v = 1 - b;
      const c = v - w;
      let g = 0;
      if (c < 1) {
        g = (v - c) / (1 - c);
      }
      return [hwb[0], c * 100, g * 100];
    };
    convert.apple.rgb = function(apple) {
      return [apple[0] / 65535 * 255, apple[1] / 65535 * 255, apple[2] / 65535 * 255];
    };
    convert.rgb.apple = function(rgb) {
      return [rgb[0] / 255 * 65535, rgb[1] / 255 * 65535, rgb[2] / 255 * 65535];
    };
    convert.gray.rgb = function(args) {
      return [args[0] / 100 * 255, args[0] / 100 * 255, args[0] / 100 * 255];
    };
    convert.gray.hsl = function(args) {
      return [0, 0, args[0]];
    };
    convert.gray.hsv = convert.gray.hsl;
    convert.gray.hwb = function(gray) {
      return [0, 100, gray[0]];
    };
    convert.gray.cmyk = function(gray) {
      return [0, 0, 0, gray[0]];
    };
    convert.gray.lab = function(gray) {
      return [gray[0], 0, 0];
    };
    convert.gray.hex = function(gray) {
      const val = Math.round(gray[0] / 100 * 255) & 255;
      const integer = (val << 16) + (val << 8) + val;
      const string = integer.toString(16).toUpperCase();
      return "000000".substring(string.length) + string;
    };
    convert.rgb.gray = function(rgb) {
      const val = (rgb[0] + rgb[1] + rgb[2]) / 3;
      return [val / 255 * 100];
    };
  }
});

// node_modules/color-convert/route.js
var require_route2 = __commonJS({
  "node_modules/color-convert/route.js"(exports, module2) {
    var conversions = require_conversions2();
    function buildGraph() {
      const graph = {};
      const models = Object.keys(conversions);
      for (let len = models.length, i = 0; i < len; i++) {
        graph[models[i]] = {
          // http://jsperf.com/1-vs-infinity
          // micro-opt, but this is simple.
          distance: -1,
          parent: null
        };
      }
      return graph;
    }
    function deriveBFS(fromModel) {
      const graph = buildGraph();
      const queue = [fromModel];
      graph[fromModel].distance = 0;
      while (queue.length) {
        const current = queue.pop();
        const adjacents = Object.keys(conversions[current]);
        for (let len = adjacents.length, i = 0; i < len; i++) {
          const adjacent = adjacents[i];
          const node = graph[adjacent];
          if (node.distance === -1) {
            node.distance = graph[current].distance + 1;
            node.parent = current;
            queue.unshift(adjacent);
          }
        }
      }
      return graph;
    }
    function link(from, to) {
      return function(args) {
        return to(from(args));
      };
    }
    function wrapConversion(toModel, graph) {
      const path3 = [graph[toModel].parent, toModel];
      let fn = conversions[graph[toModel].parent][toModel];
      let cur = graph[toModel].parent;
      while (graph[cur].parent) {
        path3.unshift(graph[cur].parent);
        fn = link(conversions[graph[cur].parent][cur], fn);
        cur = graph[cur].parent;
      }
      fn.conversion = path3;
      return fn;
    }
    module2.exports = function(fromModel) {
      const graph = deriveBFS(fromModel);
      const conversion = {};
      const models = Object.keys(graph);
      for (let len = models.length, i = 0; i < len; i++) {
        const toModel = models[i];
        const node = graph[toModel];
        if (node.parent === null) {
          continue;
        }
        conversion[toModel] = wrapConversion(toModel, graph);
      }
      return conversion;
    };
  }
});

// node_modules/color-convert/index.js
var require_color_convert2 = __commonJS({
  "node_modules/color-convert/index.js"(exports, module2) {
    var conversions = require_conversions2();
    var route = require_route2();
    var convert = {};
    var models = Object.keys(conversions);
    function wrapRaw(fn) {
      const wrappedFn = function(...args) {
        const arg0 = args[0];
        if (arg0 === void 0 || arg0 === null) {
          return arg0;
        }
        if (arg0.length > 1) {
          args = arg0;
        }
        return fn(args);
      };
      if ("conversion" in fn) {
        wrappedFn.conversion = fn.conversion;
      }
      return wrappedFn;
    }
    function wrapRounded(fn) {
      const wrappedFn = function(...args) {
        const arg0 = args[0];
        if (arg0 === void 0 || arg0 === null) {
          return arg0;
        }
        if (arg0.length > 1) {
          args = arg0;
        }
        const result = fn(args);
        if (typeof result === "object") {
          for (let len = result.length, i = 0; i < len; i++) {
            result[i] = Math.round(result[i]);
          }
        }
        return result;
      };
      if ("conversion" in fn) {
        wrappedFn.conversion = fn.conversion;
      }
      return wrappedFn;
    }
    models.forEach((fromModel) => {
      convert[fromModel] = {};
      Object.defineProperty(convert[fromModel], "channels", { value: conversions[fromModel].channels });
      Object.defineProperty(convert[fromModel], "labels", { value: conversions[fromModel].labels });
      const routes = route(fromModel);
      const routeModels = Object.keys(routes);
      routeModels.forEach((toModel) => {
        const fn = routes[toModel];
        convert[fromModel][toModel] = wrapRounded(fn);
        convert[fromModel][toModel].raw = wrapRaw(fn);
      });
    });
    module2.exports = convert;
  }
});

// node_modules/ansi-styles/index.js
var require_ansi_styles2 = __commonJS({
  "node_modules/ansi-styles/index.js"(exports, module2) {
    "use strict";
    var wrapAnsi16 = (fn, offset) => (...args) => {
      const code = fn(...args);
      return `\x1B[${code + offset}m`;
    };
    var wrapAnsi256 = (fn, offset) => (...args) => {
      const code = fn(...args);
      return `\x1B[${38 + offset};5;${code}m`;
    };
    var wrapAnsi16m = (fn, offset) => (...args) => {
      const rgb = fn(...args);
      return `\x1B[${38 + offset};2;${rgb[0]};${rgb[1]};${rgb[2]}m`;
    };
    var ansi2ansi = (n) => n;
    var rgb2rgb = (r, g, b) => [r, g, b];
    var setLazyProperty = (object, property, get) => {
      Object.defineProperty(object, property, {
        get: () => {
          const value = get();
          Object.defineProperty(object, property, {
            value,
            enumerable: true,
            configurable: true
          });
          return value;
        },
        enumerable: true,
        configurable: true
      });
    };
    var colorConvert;
    var makeDynamicStyles = (wrap, targetSpace, identity, isBackground) => {
      if (colorConvert === void 0) {
        colorConvert = require_color_convert2();
      }
      const offset = isBackground ? 10 : 0;
      const styles = {};
      for (const [sourceSpace, suite] of Object.entries(colorConvert)) {
        const name = sourceSpace === "ansi16" ? "ansi" : sourceSpace;
        if (sourceSpace === targetSpace) {
          styles[name] = wrap(identity, offset);
        } else if (typeof suite === "object") {
          styles[name] = wrap(suite[targetSpace], offset);
        }
      }
      return styles;
    };
    function assembleStyles() {
      const codes = /* @__PURE__ */ new Map();
      const styles = {
        modifier: {
          reset: [0, 0],
          // 21 isn't widely supported and 22 does the same thing
          bold: [1, 22],
          dim: [2, 22],
          italic: [3, 23],
          underline: [4, 24],
          inverse: [7, 27],
          hidden: [8, 28],
          strikethrough: [9, 29]
        },
        color: {
          black: [30, 39],
          red: [31, 39],
          green: [32, 39],
          yellow: [33, 39],
          blue: [34, 39],
          magenta: [35, 39],
          cyan: [36, 39],
          white: [37, 39],
          // Bright color
          blackBright: [90, 39],
          redBright: [91, 39],
          greenBright: [92, 39],
          yellowBright: [93, 39],
          blueBright: [94, 39],
          magentaBright: [95, 39],
          cyanBright: [96, 39],
          whiteBright: [97, 39]
        },
        bgColor: {
          bgBlack: [40, 49],
          bgRed: [41, 49],
          bgGreen: [42, 49],
          bgYellow: [43, 49],
          bgBlue: [44, 49],
          bgMagenta: [45, 49],
          bgCyan: [46, 49],
          bgWhite: [47, 49],
          // Bright color
          bgBlackBright: [100, 49],
          bgRedBright: [101, 49],
          bgGreenBright: [102, 49],
          bgYellowBright: [103, 49],
          bgBlueBright: [104, 49],
          bgMagentaBright: [105, 49],
          bgCyanBright: [106, 49],
          bgWhiteBright: [107, 49]
        }
      };
      styles.color.gray = styles.color.blackBright;
      styles.bgColor.bgGray = styles.bgColor.bgBlackBright;
      styles.color.grey = styles.color.blackBright;
      styles.bgColor.bgGrey = styles.bgColor.bgBlackBright;
      for (const [groupName, group] of Object.entries(styles)) {
        for (const [styleName, style] of Object.entries(group)) {
          styles[styleName] = {
            open: `\x1B[${style[0]}m`,
            close: `\x1B[${style[1]}m`
          };
          group[styleName] = styles[styleName];
          codes.set(style[0], style[1]);
        }
        Object.defineProperty(styles, groupName, {
          value: group,
          enumerable: false
        });
      }
      Object.defineProperty(styles, "codes", {
        value: codes,
        enumerable: false
      });
      styles.color.close = "\x1B[39m";
      styles.bgColor.close = "\x1B[49m";
      setLazyProperty(styles.color, "ansi", () => makeDynamicStyles(wrapAnsi16, "ansi16", ansi2ansi, false));
      setLazyProperty(styles.color, "ansi256", () => makeDynamicStyles(wrapAnsi256, "ansi256", ansi2ansi, false));
      setLazyProperty(styles.color, "ansi16m", () => makeDynamicStyles(wrapAnsi16m, "rgb", rgb2rgb, false));
      setLazyProperty(styles.bgColor, "ansi", () => makeDynamicStyles(wrapAnsi16, "ansi16", ansi2ansi, true));
      setLazyProperty(styles.bgColor, "ansi256", () => makeDynamicStyles(wrapAnsi256, "ansi256", ansi2ansi, true));
      setLazyProperty(styles.bgColor, "ansi16m", () => makeDynamicStyles(wrapAnsi16m, "rgb", rgb2rgb, true));
      return styles;
    }
    Object.defineProperty(module2, "exports", {
      enumerable: true,
      get: assembleStyles
    });
  }
});

// node_modules/has-flag/index.js
var require_has_flag2 = __commonJS({
  "node_modules/has-flag/index.js"(exports, module2) {
    "use strict";
    module2.exports = (flag, argv = process.argv) => {
      const prefix = flag.startsWith("-") ? "" : flag.length === 1 ? "-" : "--";
      const position = argv.indexOf(prefix + flag);
      const terminatorPosition = argv.indexOf("--");
      return position !== -1 && (terminatorPosition === -1 || position < terminatorPosition);
    };
  }
});

// node_modules/supports-color/index.js
var require_supports_color2 = __commonJS({
  "node_modules/supports-color/index.js"(exports, module2) {
    "use strict";
    var os = require("os");
    var tty = require("tty");
    var hasFlag = require_has_flag2();
    var { env } = process;
    var forceColor;
    if (hasFlag("no-color") || hasFlag("no-colors") || hasFlag("color=false") || hasFlag("color=never")) {
      forceColor = 0;
    } else if (hasFlag("color") || hasFlag("colors") || hasFlag("color=true") || hasFlag("color=always")) {
      forceColor = 1;
    }
    if ("FORCE_COLOR" in env) {
      if (env.FORCE_COLOR === "true") {
        forceColor = 1;
      } else if (env.FORCE_COLOR === "false") {
        forceColor = 0;
      } else {
        forceColor = env.FORCE_COLOR.length === 0 ? 1 : Math.min(parseInt(env.FORCE_COLOR, 10), 3);
      }
    }
    function translateLevel(level) {
      if (level === 0) {
        return false;
      }
      return {
        level,
        hasBasic: true,
        has256: level >= 2,
        has16m: level >= 3
      };
    }
    function supportsColor(haveStream, streamIsTTY) {
      if (forceColor === 0) {
        return 0;
      }
      if (hasFlag("color=16m") || hasFlag("color=full") || hasFlag("color=truecolor")) {
        return 3;
      }
      if (hasFlag("color=256")) {
        return 2;
      }
      if (haveStream && !streamIsTTY && forceColor === void 0) {
        return 0;
      }
      const min = forceColor || 0;
      if (env.TERM === "dumb") {
        return min;
      }
      if (process.platform === "win32") {
        const osRelease = os.release().split(".");
        if (Number(osRelease[0]) >= 10 && Number(osRelease[2]) >= 10586) {
          return Number(osRelease[2]) >= 14931 ? 3 : 2;
        }
        return 1;
      }
      if ("CI" in env) {
        if (["TRAVIS", "CIRCLECI", "APPVEYOR", "GITLAB_CI", "GITHUB_ACTIONS", "BUILDKITE"].some((sign) => sign in env) || env.CI_NAME === "codeship") {
          return 1;
        }
        return min;
      }
      if ("TEAMCITY_VERSION" in env) {
        return /^(9\.(0*[1-9]\d*)\.|\d{2,}\.)/.test(env.TEAMCITY_VERSION) ? 1 : 0;
      }
      if (env.COLORTERM === "truecolor") {
        return 3;
      }
      if ("TERM_PROGRAM" in env) {
        const version = parseInt((env.TERM_PROGRAM_VERSION || "").split(".")[0], 10);
        switch (env.TERM_PROGRAM) {
          case "iTerm.app":
            return version >= 3 ? 3 : 2;
          case "Apple_Terminal":
            return 2;
        }
      }
      if (/-256(color)?$/i.test(env.TERM)) {
        return 2;
      }
      if (/^screen|^xterm|^vt100|^vt220|^rxvt|color|ansi|cygwin|linux/i.test(env.TERM)) {
        return 1;
      }
      if ("COLORTERM" in env) {
        return 1;
      }
      return min;
    }
    function getSupportLevel(stream) {
      const level = supportsColor(stream, stream && stream.isTTY);
      return translateLevel(level);
    }
    module2.exports = {
      supportsColor: getSupportLevel,
      stdout: translateLevel(supportsColor(true, tty.isatty(1))),
      stderr: translateLevel(supportsColor(true, tty.isatty(2)))
    };
  }
});

// node_modules/chalk/source/util.js
var require_util = __commonJS({
  "node_modules/chalk/source/util.js"(exports, module2) {
    "use strict";
    var stringReplaceAll = (string, substring, replacer) => {
      let index = string.indexOf(substring);
      if (index === -1) {
        return string;
      }
      const substringLength = substring.length;
      let endIndex = 0;
      let returnValue = "";
      do {
        returnValue += string.substr(endIndex, index - endIndex) + substring + replacer;
        endIndex = index + substringLength;
        index = string.indexOf(substring, endIndex);
      } while (index !== -1);
      returnValue += string.substr(endIndex);
      return returnValue;
    };
    var stringEncaseCRLFWithFirstIndex = (string, prefix, postfix, index) => {
      let endIndex = 0;
      let returnValue = "";
      do {
        const gotCR = string[index - 1] === "\r";
        returnValue += string.substr(endIndex, (gotCR ? index - 1 : index) - endIndex) + prefix + (gotCR ? "\r\n" : "\n") + postfix;
        endIndex = index + 1;
        index = string.indexOf("\n", endIndex);
      } while (index !== -1);
      returnValue += string.substr(endIndex);
      return returnValue;
    };
    module2.exports = {
      stringReplaceAll,
      stringEncaseCRLFWithFirstIndex
    };
  }
});

// node_modules/chalk/source/templates.js
var require_templates2 = __commonJS({
  "node_modules/chalk/source/templates.js"(exports, module2) {
    "use strict";
    var TEMPLATE_REGEX = /(?:\\(u(?:[a-f\d]{4}|\{[a-f\d]{1,6}\})|x[a-f\d]{2}|.))|(?:\{(~)?(\w+(?:\([^)]*\))?(?:\.\w+(?:\([^)]*\))?)*)(?:[ \t]|(?=\r?\n)))|(\})|((?:.|[\r\n\f])+?)/gi;
    var STYLE_REGEX = /(?:^|\.)(\w+)(?:\(([^)]*)\))?/g;
    var STRING_REGEX = /^(['"])((?:\\.|(?!\1)[^\\])*)\1$/;
    var ESCAPE_REGEX = /\\(u(?:[a-f\d]{4}|{[a-f\d]{1,6}})|x[a-f\d]{2}|.)|([^\\])/gi;
    var ESCAPES = /* @__PURE__ */ new Map([
      ["n", "\n"],
      ["r", "\r"],
      ["t", "	"],
      ["b", "\b"],
      ["f", "\f"],
      ["v", "\v"],
      ["0", "\0"],
      ["\\", "\\"],
      ["e", "\x1B"],
      ["a", "\x07"]
    ]);
    function unescape(c) {
      const u = c[0] === "u";
      const bracket = c[1] === "{";
      if (u && !bracket && c.length === 5 || c[0] === "x" && c.length === 3) {
        return String.fromCharCode(parseInt(c.slice(1), 16));
      }
      if (u && bracket) {
        return String.fromCodePoint(parseInt(c.slice(2, -1), 16));
      }
      return ESCAPES.get(c) || c;
    }
    function parseArguments(name, arguments_2) {
      const results = [];
      const chunks = arguments_2.trim().split(/\s*,\s*/g);
      let matches;
      for (const chunk of chunks) {
        const number = Number(chunk);
        if (!Number.isNaN(number)) {
          results.push(number);
        } else if (matches = chunk.match(STRING_REGEX)) {
          results.push(matches[2].replace(ESCAPE_REGEX, (m, escape, character) => escape ? unescape(escape) : character));
        } else {
          throw new Error(`Invalid Chalk template style argument: ${chunk} (in style '${name}')`);
        }
      }
      return results;
    }
    function parseStyle(style) {
      STYLE_REGEX.lastIndex = 0;
      const results = [];
      let matches;
      while ((matches = STYLE_REGEX.exec(style)) !== null) {
        const name = matches[1];
        if (matches[2]) {
          const args = parseArguments(name, matches[2]);
          results.push([name].concat(args));
        } else {
          results.push([name]);
        }
      }
      return results;
    }
    function buildStyle(chalk, styles) {
      const enabled = {};
      for (const layer of styles) {
        for (const style of layer.styles) {
          enabled[style[0]] = layer.inverse ? null : style.slice(1);
        }
      }
      let current = chalk;
      for (const [styleName, styles2] of Object.entries(enabled)) {
        if (!Array.isArray(styles2)) {
          continue;
        }
        if (!(styleName in current)) {
          throw new Error(`Unknown Chalk style: ${styleName}`);
        }
        current = styles2.length > 0 ? current[styleName](...styles2) : current[styleName];
      }
      return current;
    }
    module2.exports = (chalk, temporary) => {
      const styles = [];
      const chunks = [];
      let chunk = [];
      temporary.replace(TEMPLATE_REGEX, (m, escapeCharacter, inverse, style, close, character) => {
        if (escapeCharacter) {
          chunk.push(unescape(escapeCharacter));
        } else if (style) {
          const string = chunk.join("");
          chunk = [];
          chunks.push(styles.length === 0 ? string : buildStyle(chalk, styles)(string));
          styles.push({ inverse, styles: parseStyle(style) });
        } else if (close) {
          if (styles.length === 0) {
            throw new Error("Found extraneous } in Chalk template literal");
          }
          chunks.push(buildStyle(chalk, styles)(chunk.join("")));
          chunk = [];
          styles.pop();
        } else {
          chunk.push(character);
        }
      });
      chunks.push(chunk.join(""));
      if (styles.length > 0) {
        const errMessage = `Chalk template literal is missing ${styles.length} closing bracket${styles.length === 1 ? "" : "s"} (\`}\`)`;
        throw new Error(errMessage);
      }
      return chunks.join("");
    };
  }
});

// node_modules/chalk/source/index.js
var require_source = __commonJS({
  "node_modules/chalk/source/index.js"(exports, module2) {
    "use strict";
    var ansiStyles = require_ansi_styles2();
    var { stdout: stdoutColor, stderr: stderrColor } = require_supports_color2();
    var {
      stringReplaceAll,
      stringEncaseCRLFWithFirstIndex
    } = require_util();
    var { isArray } = Array;
    var levelMapping = [
      "ansi",
      "ansi",
      "ansi256",
      "ansi16m"
    ];
    var styles = /* @__PURE__ */ Object.create(null);
    var applyOptions = (object, options = {}) => {
      if (options.level && !(Number.isInteger(options.level) && options.level >= 0 && options.level <= 3)) {
        throw new Error("The `level` option should be an integer from 0 to 3");
      }
      const colorLevel = stdoutColor ? stdoutColor.level : 0;
      object.level = options.level === void 0 ? colorLevel : options.level;
    };
    var ChalkClass = class {
      constructor(options) {
        return chalkFactory(options);
      }
    };
    var chalkFactory = (options) => {
      const chalk2 = {};
      applyOptions(chalk2, options);
      chalk2.template = (...arguments_2) => chalkTag(chalk2.template, ...arguments_2);
      Object.setPrototypeOf(chalk2, Chalk.prototype);
      Object.setPrototypeOf(chalk2.template, chalk2);
      chalk2.template.constructor = () => {
        throw new Error("`chalk.constructor()` is deprecated. Use `new chalk.Instance()` instead.");
      };
      chalk2.template.Instance = ChalkClass;
      return chalk2.template;
    };
    function Chalk(options) {
      return chalkFactory(options);
    }
    for (const [styleName, style] of Object.entries(ansiStyles)) {
      styles[styleName] = {
        get() {
          const builder = createBuilder(this, createStyler(style.open, style.close, this._styler), this._isEmpty);
          Object.defineProperty(this, styleName, { value: builder });
          return builder;
        }
      };
    }
    styles.visible = {
      get() {
        const builder = createBuilder(this, this._styler, true);
        Object.defineProperty(this, "visible", { value: builder });
        return builder;
      }
    };
    var usedModels = ["rgb", "hex", "keyword", "hsl", "hsv", "hwb", "ansi", "ansi256"];
    for (const model of usedModels) {
      styles[model] = {
        get() {
          const { level } = this;
          return function(...arguments_2) {
            const styler = createStyler(ansiStyles.color[levelMapping[level]][model](...arguments_2), ansiStyles.color.close, this._styler);
            return createBuilder(this, styler, this._isEmpty);
          };
        }
      };
    }
    for (const model of usedModels) {
      const bgModel = "bg" + model[0].toUpperCase() + model.slice(1);
      styles[bgModel] = {
        get() {
          const { level } = this;
          return function(...arguments_2) {
            const styler = createStyler(ansiStyles.bgColor[levelMapping[level]][model](...arguments_2), ansiStyles.bgColor.close, this._styler);
            return createBuilder(this, styler, this._isEmpty);
          };
        }
      };
    }
    var proto = Object.defineProperties(() => {
    }, {
      ...styles,
      level: {
        enumerable: true,
        get() {
          return this._generator.level;
        },
        set(level) {
          this._generator.level = level;
        }
      }
    });
    var createStyler = (open, close, parent) => {
      let openAll;
      let closeAll;
      if (parent === void 0) {
        openAll = open;
        closeAll = close;
      } else {
        openAll = parent.openAll + open;
        closeAll = close + parent.closeAll;
      }
      return {
        open,
        close,
        openAll,
        closeAll,
        parent
      };
    };
    var createBuilder = (self2, _styler, _isEmpty) => {
      const builder = (...arguments_2) => {
        if (isArray(arguments_2[0]) && isArray(arguments_2[0].raw)) {
          return applyStyle(builder, chalkTag(builder, ...arguments_2));
        }
        return applyStyle(builder, arguments_2.length === 1 ? "" + arguments_2[0] : arguments_2.join(" "));
      };
      Object.setPrototypeOf(builder, proto);
      builder._generator = self2;
      builder._styler = _styler;
      builder._isEmpty = _isEmpty;
      return builder;
    };
    var applyStyle = (self2, string) => {
      if (self2.level <= 0 || !string) {
        return self2._isEmpty ? "" : string;
      }
      let styler = self2._styler;
      if (styler === void 0) {
        return string;
      }
      const { openAll, closeAll } = styler;
      if (string.indexOf("\x1B") !== -1) {
        while (styler !== void 0) {
          string = stringReplaceAll(string, styler.close, styler.open);
          styler = styler.parent;
        }
      }
      const lfIndex = string.indexOf("\n");
      if (lfIndex !== -1) {
        string = stringEncaseCRLFWithFirstIndex(string, closeAll, openAll, lfIndex);
      }
      return openAll + string + closeAll;
    };
    var template;
    var chalkTag = (chalk2, ...strings) => {
      const [firstString] = strings;
      if (!isArray(firstString) || !isArray(firstString.raw)) {
        return strings.join(" ");
      }
      const arguments_2 = strings.slice(1);
      const parts = [firstString.raw[0]];
      for (let i = 1; i < firstString.length; i++) {
        parts.push(
          String(arguments_2[i - 1]).replace(/[{}\\]/g, "\\$&"),
          String(firstString.raw[i])
        );
      }
      if (template === void 0) {
        template = require_templates2();
      }
      return template(chalk2, parts.join(""));
    };
    Object.defineProperties(Chalk.prototype, styles);
    var chalk = Chalk();
    chalk.supportsColor = stdoutColor;
    chalk.stderr = Chalk({ level: stderrColor ? stderrColor.level : 0 });
    chalk.stderr.supportsColor = stderrColor;
    module2.exports = chalk;
  }
});

// node_modules/ts-command-line-args/dist/helpers/insert-code.helper.js
var require_insert_code_helper = __commonJS({
  "node_modules/ts-command-line-args/dist/helpers/insert-code.helper.js"(exports) {
    "use strict";
    var __assign = exports && exports.__assign || function() {
      __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
          s = arguments[i];
          for (var p in s)
            if (Object.prototype.hasOwnProperty.call(s, p))
              t[p] = s[p];
        }
        return t;
      };
      return __assign.apply(this, arguments);
    };
    var __awaiter = exports && exports.__awaiter || function(thisArg, _arguments, P, generator) {
      function adopt(value) {
        return value instanceof P ? value : new P(function(resolve) {
          resolve(value);
        });
      }
      return new (P || (P = Promise))(function(resolve, reject) {
        function fulfilled(value) {
          try {
            step(generator.next(value));
          } catch (e) {
            reject(e);
          }
        }
        function rejected(value) {
          try {
            step(generator["throw"](value));
          } catch (e) {
            reject(e);
          }
        }
        function step(result) {
          result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected);
        }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
      });
    };
    var __generator = exports && exports.__generator || function(thisArg, body) {
      var _ = { label: 0, sent: function() {
        if (t[0] & 1)
          throw t[1];
        return t[1];
      }, trys: [], ops: [] }, f, y, t, g;
      return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() {
        return this;
      }), g;
      function verb(n) {
        return function(v) {
          return step([n, v]);
        };
      }
      function step(op) {
        if (f)
          throw new TypeError("Generator is already executing.");
        while (_)
          try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done)
              return t;
            if (y = 0, t)
              op = [op[0] & 2, t.value];
            switch (op[0]) {
              case 0:
              case 1:
                t = op;
                break;
              case 4:
                _.label++;
                return { value: op[1], done: false };
              case 5:
                _.label++;
                y = op[1];
                op = [0];
                continue;
              case 7:
                op = _.ops.pop();
                _.trys.pop();
                continue;
              default:
                if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) {
                  _ = 0;
                  continue;
                }
                if (op[0] === 3 && (!t || op[1] > t[0] && op[1] < t[3])) {
                  _.label = op[1];
                  break;
                }
                if (op[0] === 6 && _.label < t[1]) {
                  _.label = t[1];
                  t = op;
                  break;
                }
                if (t && _.label < t[2]) {
                  _.label = t[2];
                  _.ops.push(op);
                  break;
                }
                if (t[2])
                  _.ops.pop();
                _.trys.pop();
                continue;
            }
            op = body.call(thisArg, _);
          } catch (e) {
            op = [6, e];
            y = 0;
          } finally {
            f = t = 0;
          }
        if (op[0] & 5)
          throw op[1];
        return { value: op[0] ? op[1] : void 0, done: true };
      }
    };
    var __spreadArray = exports && exports.__spreadArray || function(to, from) {
      for (var i = 0, il = from.length, j = to.length; i < il; i++, j++)
        to[j] = from[i];
      return to;
    };
    var __importDefault = exports && exports.__importDefault || function(mod) {
      return mod && mod.__esModule ? mod : { "default": mod };
    };
    Object.defineProperty(exports, "__esModule", { value: true });
    exports.insertCode = void 0;
    var line_ending_helper_1 = require_line_ending_helper();
    var path_1 = require("path");
    var util_1 = require("util");
    var fs_1 = require("fs");
    var chalk_1 = __importDefault(require_source());
    var asyncReadFile = util_1.promisify(fs_1.readFile);
    var asyncWriteFile = util_1.promisify(fs_1.writeFile);
    function insertCode(input, partialOptions) {
      return __awaiter(this, void 0, void 0, function() {
        var options, fileDetails, filePath, content, lineBreak, lines, modifiedContent;
        var _a;
        return __generator(this, function(_b) {
          switch (_b.label) {
            case 0:
              options = __assign({ removeDoubleBlankLines: false }, partialOptions);
              if (!(typeof input === "string"))
                return [3, 2];
              filePath = path_1.resolve(input);
              console.log("Loading existing file from '" + chalk_1.default.blue(filePath) + "'");
              _a = { filePath };
              return [4, asyncReadFile(filePath)];
            case 1:
              fileDetails = (_a.fileContent = _b.sent().toString(), _a);
              return [3, 3];
            case 2:
              fileDetails = input;
              _b.label = 3;
            case 3:
              content = fileDetails.fileContent;
              lineBreak = line_ending_helper_1.findEscapeSequence(content);
              lines = line_ending_helper_1.splitContent(content);
              return [4, insertCodeImpl(fileDetails.filePath, lines, options, 0)];
            case 4:
              lines = _b.sent();
              if (options.removeDoubleBlankLines) {
                lines = lines.filter(function(line, index, lines2) {
                  return line_ending_helper_1.filterDoubleBlankLines(line, index, lines2);
                });
              }
              modifiedContent = lines.join(lineBreak);
              if (!(typeof input === "string"))
                return [3, 6];
              console.log("Saving modified content to '" + chalk_1.default.blue(fileDetails.filePath) + "'");
              return [4, asyncWriteFile(fileDetails.filePath, modifiedContent)];
            case 5:
              _b.sent();
              _b.label = 6;
            case 6:
              return [2, modifiedContent];
          }
        });
      });
    }
    exports.insertCode = insertCode;
    function insertCodeImpl(filePath, lines, options, startLine) {
      return __awaiter(this, void 0, void 0, function() {
        var insertCodeBelow, insertCodeAbove, insertCodeBelowResult, insertCodeAboveResult, linesFromFile, linesBefore, linesAfter;
        return __generator(this, function(_a) {
          switch (_a.label) {
            case 0:
              insertCodeBelow = options === null || options === void 0 ? void 0 : options.insertCodeBelow;
              insertCodeAbove = options === null || options === void 0 ? void 0 : options.insertCodeAbove;
              if (insertCodeBelow == null) {
                return [2, Promise.resolve(lines)];
              }
              insertCodeBelowResult = insertCodeBelow != null ? findIndex(lines, function(line) {
                return line.indexOf(insertCodeBelow) === 0;
              }, startLine) : void 0;
              if (insertCodeBelowResult == null) {
                return [2, Promise.resolve(lines)];
              }
              insertCodeAboveResult = insertCodeAbove != null ? findIndex(lines, function(line) {
                return line.indexOf(insertCodeAbove) === 0;
              }, insertCodeBelowResult.lineIndex) : void 0;
              return [4, loadLines(filePath, options, insertCodeBelowResult)];
            case 1:
              linesFromFile = _a.sent();
              linesBefore = lines.slice(0, insertCodeBelowResult.lineIndex + 1);
              linesAfter = insertCodeAboveResult != null ? lines.slice(insertCodeAboveResult.lineIndex) : [];
              lines = __spreadArray(__spreadArray(__spreadArray([], linesBefore), linesFromFile), linesAfter);
              return [2, insertCodeAboveResult == null ? lines : insertCodeImpl(filePath, lines, options, insertCodeAboveResult.lineIndex)];
          }
        });
      });
    }
    var fileRegExp = /file="([^"]+)"/;
    var codeCommentRegExp = /codeComment(="([^"]+)")?/;
    var snippetRegExp = /snippetName="([^"]+)"/;
    function loadLines(targetFilePath, options, result) {
      var _a;
      return __awaiter(this, void 0, void 0, function() {
        var partialPathResult, codeCommentResult, snippetResult, partialPath, filePath, fileBuffer, contentLines, copyBelowMarker, copyAboveMarker, copyBelowIndex, copyAboveIndex;
        return __generator(this, function(_b) {
          switch (_b.label) {
            case 0:
              partialPathResult = fileRegExp.exec(result.line);
              if (partialPathResult == null) {
                throw new Error("insert code token (" + options.insertCodeBelow + ') found in file but file path not specified (file="relativePath/from/markdown/toFile.whatever")');
              }
              codeCommentResult = codeCommentRegExp.exec(result.line);
              snippetResult = snippetRegExp.exec(result.line);
              partialPath = partialPathResult[1];
              filePath = path_1.isAbsolute(partialPath) ? partialPath : path_1.join(path_1.dirname(targetFilePath), partialPathResult[1]);
              console.log("Inserting code from '" + chalk_1.default.blue(filePath) + "' into '" + chalk_1.default.blue(targetFilePath) + "'");
              return [4, asyncReadFile(filePath)];
            case 1:
              fileBuffer = _b.sent();
              contentLines = line_ending_helper_1.splitContent(fileBuffer.toString());
              copyBelowMarker = options.copyCodeBelow;
              copyAboveMarker = options.copyCodeAbove;
              copyBelowIndex = copyBelowMarker != null ? contentLines.findIndex(findLine(copyBelowMarker, snippetResult === null || snippetResult === void 0 ? void 0 : snippetResult[1])) : -1;
              copyAboveIndex = copyAboveMarker != null ? contentLines.findIndex(function(line, index) {
                return line.indexOf(copyAboveMarker) === 0 && index > copyBelowIndex;
              }) : -1;
              if (snippetResult != null && copyBelowIndex < 0) {
                throw new Error("The copyCodeBelow marker '" + options.copyCodeBelow + "' was not found with the requested snippet: '" + snippetResult[1] + "'");
              }
              contentLines = contentLines.slice(copyBelowIndex + 1, copyAboveIndex > 0 ? copyAboveIndex : void 0);
              if (codeCommentResult != null) {
                contentLines = __spreadArray(__spreadArray(["```" + ((_a = codeCommentResult[2]) !== null && _a !== void 0 ? _a : "")], contentLines), ["```"]);
              }
              return [2, contentLines];
          }
        });
      });
    }
    function findLine(copyBelowMarker, snippetName) {
      return function(line) {
        return line.indexOf(copyBelowMarker) === 0 && (snippetName == null || line.indexOf(snippetName) > 0);
      };
    }
    function findIndex(lines, predicate, startLine) {
      for (var lineIndex = startLine; lineIndex < lines.length; lineIndex++) {
        var line = lines[lineIndex];
        if (predicate(line)) {
          return { lineIndex, line };
        }
      }
      return void 0;
    }
  }
});

// node_modules/ts-command-line-args/dist/helpers/index.js
var require_helpers = __commonJS({
  "node_modules/ts-command-line-args/dist/helpers/index.js"(exports) {
    "use strict";
    var __createBinding = exports && exports.__createBinding || (Object.create ? function(o, m, k, k2) {
      if (k2 === void 0)
        k2 = k;
      Object.defineProperty(o, k2, { enumerable: true, get: function() {
        return m[k];
      } });
    } : function(o, m, k, k2) {
      if (k2 === void 0)
        k2 = k;
      o[k2] = m[k];
    });
    var __exportStar = exports && exports.__exportStar || function(m, exports2) {
      for (var p in m)
        if (p !== "default" && !Object.prototype.hasOwnProperty.call(exports2, p))
          __createBinding(exports2, m, p);
    };
    Object.defineProperty(exports, "__esModule", { value: true });
    __exportStar(require_command_line_helper(), exports);
    __exportStar(require_add_content_helper(), exports);
    __exportStar(require_markdown_helper(), exports);
    __exportStar(require_visitor(), exports);
    __exportStar(require_line_ending_helper(), exports);
    __exportStar(require_options_helper(), exports);
    __exportStar(require_string_helper(), exports);
    __exportStar(require_insert_code_helper(), exports);
  }
});

// node_modules/ts-command-line-args/dist/parse.js
var require_parse = __commonJS({
  "node_modules/ts-command-line-args/dist/parse.js"(exports) {
    "use strict";
    var __assign = exports && exports.__assign || function() {
      __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
          s = arguments[i];
          for (var p in s)
            if (Object.prototype.hasOwnProperty.call(s, p))
              t[p] = s[p];
        }
        return t;
      };
      return __assign.apply(this, arguments);
    };
    var __rest = exports && exports.__rest || function(s, e) {
      var t = {};
      for (var p in s)
        if (Object.prototype.hasOwnProperty.call(s, p) && e.indexOf(p) < 0)
          t[p] = s[p];
      if (s != null && typeof Object.getOwnPropertySymbols === "function")
        for (var i = 0, p = Object.getOwnPropertySymbols(s); i < p.length; i++) {
          if (e.indexOf(p[i]) < 0 && Object.prototype.propertyIsEnumerable.call(s, p[i]))
            t[p[i]] = s[p[i]];
        }
      return t;
    };
    var __spreadArray = exports && exports.__spreadArray || function(to, from) {
      for (var i = 0, il = from.length, j = to.length; i < il; i++, j++)
        to[j] = from[i];
      return to;
    };
    var __importDefault = exports && exports.__importDefault || function(mod) {
      return mod && mod.__esModule ? mod : { "default": mod };
    };
    Object.defineProperty(exports, "__esModule", { value: true });
    exports.parse = void 0;
    var command_line_args_1 = __importDefault(require_dist());
    var command_line_usage_1 = __importDefault(require_command_line_usage());
    var helpers_1 = require_helpers();
    var options_helper_1 = require_options_helper();
    var string_helper_1 = require_string_helper();
    var fs_1 = require("fs");
    var path_1 = require("path");
    function parse2(config, options, exitProcess, addCommandLineResults) {
      if (options === void 0) {
        options = {};
      }
      if (exitProcess === void 0) {
        exitProcess = true;
      }
      options = options || {};
      var argsWithBooleanValues = options.argv || process.argv.slice(2);
      var logger = options.logger || console;
      var normalisedConfig = helpers_1.normaliseConfig(config);
      options.argv = helpers_1.removeBooleanValues(argsWithBooleanValues, normalisedConfig);
      var optionList = helpers_1.createCommandLineConfig(normalisedConfig);
      var parsedArgs = command_line_args_1.default(optionList, options);
      if (parsedArgs["_all"] != null) {
        var unknown = parsedArgs["_unknown"];
        parsedArgs = parsedArgs["_all"];
        if (unknown) {
          parsedArgs["_unknown"] = unknown;
        }
      }
      var booleanValues = helpers_1.getBooleanValues(argsWithBooleanValues, normalisedConfig);
      parsedArgs = __assign(__assign({}, parsedArgs), booleanValues);
      if (options.loadFromFileArg != null && parsedArgs[options.loadFromFileArg] != null) {
        var configFromFile = JSON.parse(fs_1.readFileSync(path_1.resolve(parsedArgs[options.loadFromFileArg])).toString());
        var parsedArgsWithoutDefaults = command_line_args_1.default(
          // eslint-disable-next-line @typescript-eslint/no-unused-vars
          optionList.map(function(_a) {
            var defaultValue = _a.defaultValue, option = __rest(_a, ["defaultValue"]);
            return __assign({}, option);
          }),
          options
        );
        parsedArgs = helpers_1.mergeConfig(parsedArgs, __assign(__assign({}, parsedArgsWithoutDefaults), booleanValues), configFromFile, normalisedConfig, options.loadFromFileJsonPathArg);
      }
      var missingArgs = listMissingArgs(optionList, parsedArgs);
      if (options.helpArg != null && parsedArgs[options.helpArg]) {
        printHelpGuide(options, optionList, logger);
        if (exitProcess) {
          return process.exit(resolveExitCode(options, "usageGuide", parsedArgs, missingArgs));
        }
      } else if (missingArgs.length > 0) {
        if (options.showHelpWhenArgsMissing) {
          var missingArgsHeader = typeof options.helpWhenArgMissingHeader === "function" ? options.helpWhenArgMissingHeader(missingArgs) : options.helpWhenArgMissingHeader;
          var additionalHeaderSections = missingArgsHeader != null ? [missingArgsHeader] : [];
          printHelpGuide(options, optionList, logger, additionalHeaderSections);
        } else if (options.hideMissingArgMessages !== true) {
          printMissingArgErrors(missingArgs, logger, options.baseCommand);
          printUsageGuideMessage(__assign(__assign({}, options), { logger }), options.helpArg != null ? optionList.filter(function(option) {
            return option.name === options.helpArg;
          })[0] : void 0);
        }
      }
      var _commandLineResults = {
        missingArgs,
        printHelp: function() {
          return printHelpGuide(options, optionList, logger);
        }
      };
      if (missingArgs.length > 0 && exitProcess) {
        process.exit(resolveExitCode(options, "missingArgs", parsedArgs, missingArgs));
      } else {
        if (addCommandLineResults) {
          parsedArgs = __assign(__assign({}, parsedArgs), { _commandLineResults });
        }
        return parsedArgs;
      }
    }
    exports.parse = parse2;
    function resolveExitCode(options, reason, passedArgs, missingArgs) {
      switch (typeof options.processExitCode) {
        case "number":
          return options.processExitCode;
        case "function":
          return options.processExitCode(reason, passedArgs, missingArgs);
        default:
          return 0;
      }
    }
    function printHelpGuide(options, optionList, logger, additionalHeaderSections) {
      var _a, _b;
      if (additionalHeaderSections === void 0) {
        additionalHeaderSections = [];
      }
      var sections = __spreadArray(__spreadArray(__spreadArray(__spreadArray(__spreadArray([], additionalHeaderSections), ((_a = options.headerContentSections) === null || _a === void 0 ? void 0 : _a.filter(filterCliSections)) || []), options_helper_1.getOptionSections(options).map(function(option) {
        return options_helper_1.addOptions(option, optionList, options);
      })), options_helper_1.getOptionFooterSection(optionList, options)), ((_b = options.footerContentSections) === null || _b === void 0 ? void 0 : _b.filter(filterCliSections)) || []);
      helpers_1.visit(sections, function(value) {
        switch (typeof value) {
          case "string":
            return string_helper_1.removeAdditionalFormatting(value);
          default:
            return value;
        }
      });
      var usageGuide = command_line_usage_1.default(sections);
      logger.log(usageGuide);
    }
    function filterCliSections(section) {
      return section.includeIn == null || section.includeIn === "both" || section.includeIn === "cli";
    }
    function printMissingArgErrors(missingArgs, logger, baseCommand) {
      baseCommand = baseCommand ? baseCommand + " " : "";
      missingArgs.forEach(function(config) {
        var aliasMessage = config.alias != null ? " or '" + baseCommand + "-" + config.alias + " passedValue'" : "";
        var runCommand = baseCommand !== "" ? "running '" + baseCommand + "--" + config.name + "=passedValue'" + aliasMessage : "passing '--" + config.name + "=passedValue'" + aliasMessage + " in command line arguments";
        logger.error("Required parameter '" + config.name + "' was not passed. Please provide a value by " + runCommand);
      });
    }
    function printUsageGuideMessage(options, helpParam) {
      if (helpParam != null) {
        var helpArg = helpParam.alias != null ? "-" + helpParam.alias : "--" + helpParam.name;
        var command = options.baseCommand != null ? "run '" + options.baseCommand + " " + helpArg + "'" : "pass '" + helpArg + "'";
        options.logger.log("To view the help guide " + command);
      }
    }
    function listMissingArgs(commandLineConfig, parsedArgs) {
      return commandLineConfig.filter(function(config) {
        return config.optional == null && parsedArgs[config.name] == null;
      }).filter(function(config) {
        if (config.type.name === "Boolean") {
          parsedArgs[config.name] = false;
          return false;
        }
        return true;
      });
    }
  }
});

// node_modules/ts-command-line-args/dist/contracts.js
var require_contracts = __commonJS({
  "node_modules/ts-command-line-args/dist/contracts.js"(exports) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
  }
});

// node_modules/ts-command-line-args/dist/index.js
var require_dist7 = __commonJS({
  "node_modules/ts-command-line-args/dist/index.js"(exports) {
    "use strict";
    var __createBinding = exports && exports.__createBinding || (Object.create ? function(o, m, k, k2) {
      if (k2 === void 0)
        k2 = k;
      Object.defineProperty(o, k2, { enumerable: true, get: function() {
        return m[k];
      } });
    } : function(o, m, k, k2) {
      if (k2 === void 0)
        k2 = k;
      o[k2] = m[k];
    });
    var __exportStar = exports && exports.__exportStar || function(m, exports2) {
      for (var p in m)
        if (p !== "default" && !Object.prototype.hasOwnProperty.call(exports2, p))
          __createBinding(exports2, m, p);
    };
    Object.defineProperty(exports, "__esModule", { value: true });
    __exportStar(require_parse(), exports);
    __exportStar(require_contracts(), exports);
    __exportStar(require_helpers(), exports);
  }
});

// src/index.ts
var import_node_path2 = __toESM(require("node:path"));
var import_node_fs = require("node:fs");
var import_ts_command_line_args = __toESM(require_dist7());

// src/converter/converter.ts
var import_node_path = __toESM(require("node:path"));
function convertPosition(position) {
  return {
    line: position.line + 1,
    // pyright count start at 0 while codeclimate start at 1
    column: position.character
  };
}
function convertRange(range) {
  return {
    begin: convertPosition(range.start),
    end: convertPosition(range.end)
  };
}
function convertDiagnosticCategoryToGitlabSeverity(category) {
  switch (category) {
    case "error": {
      return "blocker";
    }
    case "warning": {
      return "major";
    }
    case "information": {
      return "info";
    }
    case "unreachablecode": {
      return "minor";
    }
    case "unusedcode": {
      return "info";
    }
    case "deprecated": {
      return "info";
    }
  }
}
function convertDiagnosticToGitlab(diag, base_path) {
  return {
    description: diag.message,
    fingerprint: diag.rule || "Unknown",
    severity: convertDiagnosticCategoryToGitlabSeverity(diag.severity),
    location: {
      path: import_node_path.default.relative(import_node_path.default.resolve(base_path), diag.file),
      positions: diag.range === void 0 ? { begin: { column: 1, line: 1 }, end: { column: 1, line: 1 } } : convertRange(diag.range)
    }
  };
}
function convertDiagnostics(diag, base_path) {
  const response = [];
  for (const value of diag) {
    response.push(convertDiagnosticToGitlab(value, base_path));
  }
  return response;
}

// src/index.ts
var arguments_ = (0, import_ts_command_line_args.parse)(
  {
    src: String,
    output: String,
    base_path: String,
    help: { type: Boolean, optional: true, alias: "h", description: "Prints this usage guide" }
  },
  {
    helpArg: "help",
    headerContentSections: [{ header: "Overall usage", content: "Get a json output of pyright and output another json. There is no validation for the input json, so be careful to pass in the correct one. All the paths will be converted from absolute ones to relative ones based on the base_path - which should point to the base directory of the repository" }]
  }
);
var report = JSON.parse((0, import_node_fs.readFileSync)(arguments_.src, "utf8").toString());
var parsedData = report.generalDiagnostics;
var convertedData = convertDiagnostics(parsedData, import_node_path2.default.resolve(arguments_.base_path));
(0, import_node_fs.writeFileSync)(arguments_.output, JSON.stringify(convertedData), { encoding: "utf8" });
/*! Bundled license information:

deep-extend/lib/deep-extend.js:
  (*!
   * @description Recursive object extending
   * @author Viacheslav Lotsmanov <lotsmanov89@gmail.com>
   * @license MIT
   *
   * The MIT License (MIT)
   *
   * Copyright (c) 2013-2018 Viacheslav Lotsmanov
   *
   * Permission is hereby granted, free of charge, to any person obtaining a copy of
   * this software and associated documentation files (the "Software"), to deal in
   * the Software without restriction, including without limitation the rights to
   * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
   * the Software, and to permit persons to whom the Software is furnished to do so,
   * subject to the following conditions:
   *
   * The above copyright notice and this permission notice shall be included in all
   * copies or substantial portions of the Software.
   *
   * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
   * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
   * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
   * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
   * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
   * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
   *)
*/
